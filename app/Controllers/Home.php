<?php

namespace App\Controllers;
use App\Models\Mtrack;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;
use CodeIgniter\Model;
class Home extends BaseController
{
	public function initController(
        RequestInterface $request,
        ResponseInterface $response,
        LoggerInterface $logger
    ) {
        parent::initController($request, $response, $logger);
		$this->db = db_connect();
		$this->smarty->assign('base_url',base_url());
		//$this->session = session();
		//$this->smarty->assign('auth',$this->session->get());
        $this->model=new Mtrack();
    }
    public function index()
    {
        return view('welcome_message');
    }
	
	//public function infols($lsnumber,$lsid,$ionumber){
	public function infols($lsnumber){
			$lsnumber=base64_decode($lsnumber);
			$datalsheader 		= $this->model->getrecord('ls_header', $lsnumber);
			//print_r($datalsheader);exit;
			$lsid=$datalsheader[0]["ls_id"];
			$ionumber=$datalsheader[0]["io_number"];
			$datapartial = $this->model->get_data('get_partial',$lsid,$lsnumber,$ionumber);
			
			$partial=$datapartial['partial_number'];
			$permentbaru=$datapartial['permentbaru'];
							
			if($lsnumber){
				$datalspackage 		= $this->model->getrecord('ls_package', $lsid, $ionumber);
				$datalsremarks 		= $this->model->getrecord('ls_remarks', $lsid, $ionumber);
				$datalsremarksdetil = $this->model->getrecord('ls_remarks_detail', $lsid, $ionumber);
				$datalsdetail	 	= $this->model->getrecord('ls_detail', $lsid, $ionumber);
			
				//print_r($datalsdetail);exit();
				$dataverificator	= $this->model->getrecord('ls_verificator', $lsid, $ionumber);
				$dataksocode 		= $this->model->getrecord('ksocode', $lsid, $ionumber);
				
				$getcomodity = substr($ionumber, 2, 2);
				$this->smarty->assign('judulcomodity', $this->model->getrecord('commodity_desc', $getcomodity));
				if($getcomodity == '02'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 85/M-DAG/PER/10/2015';
					}
				}elseif($getcomodity == '03'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 622/MPP/KEP/10/2003';
					}
				}elseif($getcomodity == '05'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						//$perment = 'No. 19/M-DAG/PER/3/2008';
						$perment = 'No. 103/M-DAG/PER/12/2015';
					}
				}elseif($getcomodity == '07'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						//$perment = 'No. 58/M-DAG/PER/9/2012';
						$perment = 'No. 125/M-DAG/PER/12/2015';	
					}
				}elseif($getcomodity == '08'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 647/MPP/KEP/10/2004';
					}
				}elseif($getcomodity == '09'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						//$perment = 'No. 18/M-DAG/PER/4/2007';
						//$perment = 'No. 117/M-DAG/PER/12/2015';
						$perment = 'No. 14 TAHUN 2020';
					}
				}elseif($getcomodity == '10'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 35/M-DAG/PER/5/2012';
					}
				}elseif($getcomodity == '11'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 72/M-DAG/PER/11/2012';
					}
				}elseif($getcomodity == '12'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						//$perment = 'No. 07/M-DAG/PER/2/2012';
						$perment = 'No. 102/M-DAG/PER/12/2015';
					}
				}elseif($getcomodity == '13'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						//$perment = 'No. 39/M-DAG/PER/9/2009';
						//$perment = 'No. 31/M-DAG/PER/5/2016';
						$perment = 'No. 84 TAHUN 2019';
					}
				}elseif($getcomodity == '14'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						//$perment = 'No. 36/M-DAG/PER/7/2014';
						//$perment = 'No. 73/M-DAG/PER/10/2014';
						$perment = 'No. 87/M-DAG/PER/10/2015';
					}
				}elseif($getcomodity == '15'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						//$perment = 'No. 36/M-DAG/PER/7/2014';
						//$perment = 'No. 73/M-DAG/PER/10/2014';
						$perment = 'No. 87/M-DAG/PER/10/2015';
					}
				}elseif($getcomodity == '16'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						//$perment = 'No. 36/M-DAG/PER/7/2014';
						//$perment = 'No. 73/M-DAG/PER/10/2014';
						$perment = 'No. 87/M-DAG/PER/10/2015';
					}
				}elseif($getcomodity == '17'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						//$perment = 'No. 73/M-DAG/PER/10/2014';
						$perment = 'No. 87/M-DAG/PER/10/2015';
					}
				}elseif($getcomodity == '18'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						//$perment = 'No. 08/M-DAG/PER/2/2012';
						$perment = 'No. 113/M-DAG/PER/12/2015';
					}
				}elseif($getcomodity == '19'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 71/M-DAG/PER/11/2012';
					}
				}elseif($getcomodity == '20'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						//$perment = 'No. 36/M-DAG/PER/7/2014';
						//$perment = 'No. 73/M-DAG/PER/10/2014';
						$perment = 'No. 87/M-DAG/PER/10/2015';
					}
				}elseif($getcomodity == '21'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 75/M-DAG/PER/10/2014';
					}
				}elseif($getcomodity == '22'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 40/M-DAG/PER/12/2011';
					}
				}elseif($getcomodity == '23'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						//$perment = 'No. 40/M-DAG/PER/7/2014';
						$perment = 'No. 83/M-DAG/PER/10/2015';
					}
				}elseif($getcomodity == '24'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						//$perment = 'No. 47/M-DAG/PER/8/2013';
						//$perment = 'No. 71/M-DAG/PER/9/2015';
						$perment = 'No. 44 TAHUN 2019';
						//$perment = 'No. 30/M-DAG/PER/5/2017';
					}
				}elseif($getcomodity == '25'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						//$perment = 'No. 38/M-DAG/PER/8/2013';
						$perment = 'No. 41/M-DAG/PER/5/2016';
					}
				}elseif($getcomodity == '26'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						//$perment = 'No. 36/M-DAG/PER/7/2014';
						//$perment = 'No. 73/M-DAG/PER/10/2014';
						$perment = 'No. 87/M-DAG/PER/10/2015';
					}
				}elseif($getcomodity == '29'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						//$perment = 'No. 36/M-DAG/PER/7/2014';
						//$perment = 'No. 73/M-DAG/PER/10/2014';
						$perment = 'No. 87/M-DAG/PER/10/2015';
					}
				}elseif($getcomodity == '30'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 40/M-DAG/PER/8/2013';
					}
				}elseif($getcomodity == '31'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 23/M-DAG/PER/4/2014';
					}
				}elseif($getcomodity == '32'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 28/M-DAG/PER/6/2014';
					}
				}elseif($getcomodity == '33'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						//$perment = 'No. 55/M-DAG/PER/9/2014';
						$perment = 'No. 84/M-DAG/PER/10/2015';
					}
				}elseif($getcomodity == '34'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 86/M-DAG/PER/10/2015';
					}
				}elseif($getcomodity == '35'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 77/M-DAG/PER/11/2016';
					}
				}elseif($getcomodity == '36'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						//$perment = 'No. 82/M-DAG/PER/12/2016';
						$perment = 'No. 110 TAHUN 2018';
					}
				}elseif($getcomodity == '37'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 37/M-DAG/PER/7/2014';					
					}
				}elseif($getcomodity == '38'){
					$perment = 'No. 84/M-DAG/PER/11/2017';
				}elseif($getcomodity == '39'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 29 TAHUN 2018';
					}
				}elseif($getcomodity == '40'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 30 TAHUN 2018';
					}
				}elseif($getcomodity == '41'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 86 TAHUN 2017';
					}
				}elseif($getcomodity == '42'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 66 TAHUN 2018';
					}
				}elseif($getcomodity == '43'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 68 TAHUN 2020';
					}
				}elseif($getcomodity == '44'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 68 TAHUN 2020';
					}
				}elseif($getcomodity == '45'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 68 TAHUN 2020';
					}
				}elseif($getcomodity == '46'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{}
				}elseif($getcomodity == '47'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{}
				}elseif($getcomodity == '48'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{}
				}elseif($getcomodity == '49'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{}
				}elseif($getcomodity == '50'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{}
				}elseif($getcomodity == '51'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{}
				}elseif($getcomodity == '52'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{}
				}elseif($getcomodity == '53'){
					if($permentbaru=='1'){
						$perment = '';
					}else{}
				}
								
				
				
				$this->smarty->assign('commodity', $getcomodity);
				$this->smarty->assign('datalsheader', $datalsheader);
				$this->smarty->assign('datalspackage', $datalspackage);
				$this->smarty->assign('datalsremarks', $datalsremarks);
				$this->smarty->assign('datalsremarksdetil', $datalsremarksdetil);
				//$detilnya = htmlentities($datalsdetail);
				$this->smarty->assign('datalsdetail', $datalsdetail);
				$this->smarty->assign('dataverificator', $dataverificator);
				
				//ls_file_monitoring
				//$this->cek_ls_dist($lsid,$ionumber,$datalsheader[0]['status']);
				//end
				
				
				$sql="SELECT * FROM vpti_ls_detail WHERE ls_id='".$lsid."' AND io_number='".$ionumber."' AND flag='1' ";
				$ceklampiran = $this->db->query($sql)->getResultArray();
				if($ceklampiran){
					$datalampiran = $this->model->getrecord('ls_detail_lampiran', $lsid, $ionumber);
					$this->smarty->assign('datalampiran', $datalampiran);
				} else {
					$this->smarty->assign('datalampiran', '');
				}
				
				$this->smarty->assign('lsnumber', $lsnumber);
				$this->smarty->assign('ionumber', $ionumber);
				$this->smarty->assign('perment', $perment);
				
				//$lsforbarcode = substr($lsnumber, (strlen($lsnumber)-3), 3);
				//$ioforbarcode = substr($ionumber, (strlen($ionumber)-3), 3);
				if(isset($dataverificator["inisial"])){
					//$verificatorbarcode = $this->dapetinhurufpertama($dataverificator->checked_by_user);
					$verificatorbarcode = $dataverificator["inisial"];
				}else{
					$verificatorbarcode = "KSO";
					//$verificatorbarcode = $datalsheader[0]['issuance_place'];
				}
				$lsdatebarcode	 	= date('dmY', strtotime($datalsheader[0]['ls_date']));
				
				$htmlheader = $this->smarty->fetch('Public/ls_pdf_header_new2021.tpl');			
				$htmlcontent = $this->smarty->fetch('Public/ls_pdf_new2021.tpl');	
				echo $htmlheader;
				echo $htmlcontent;
				
				exit;
		}
	}
}

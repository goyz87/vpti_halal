<?php

namespace App\Controllers;
use App\Models\Mumum;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;
class Umum extends BaseController
{
	public function initController(
        RequestInterface $request,
        ResponseInterface $response,
        LoggerInterface $logger
    ) {
        parent::initController($request, $response, $logger);
		$this->smarty->assign('base_url',base_url());
		$this->session = session();
		
        $this->smarty->assign('auth',$this->session->get());
        $this->smarty->assign('info',$this->session->get());
        $this->smarty->assign('base_url',base_url());
		$this->model=new Mumum();
    }
    public function index()
    {
		//echo $this->session->get('USER_ID');exit;
	   if((!$this->session->get('USER_ID'))){
			return redirect()->to(base_url().'/login');
		}
       $this->smarty->display('main.html');
    }
	public function getkonten($p1="",$p2="")
    {
		if((!$this->session->get('USER_ID'))){
			return redirect()->to(base_url().'/login');
		}
		switch($p1){
			case "profile":
				$konten="profile.html";
			break;
		}
		$this->smarty->assign('konten',$konten);
        $this->smarty->display('main.html');
    }

	public function getmodal()
    {
		if((!$this->session->get('USER_ID'))){
			return redirect()->to(base_url().'/login');
		}
		$mod=$this->request->getPost('mod');
		switch($mod){
			case "supplier":
				$konten="VR/data-supplier.html";
			break;
			case "form-supplier":
				$konten="VR/data-supplier.html";
			break;
			
		}
		$this->smarty->assign('konten',$konten);
        $this->smarty->display($konten);
    }
}

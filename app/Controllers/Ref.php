<?php

namespace App\Controllers;
use App\Models\Mref;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;
class Ref extends BaseController
{
	public function initController(
        RequestInterface $request,
        ResponseInterface $response,
        LoggerInterface $logger
    ) {
        parent::initController($request, $response, $logger);
		$this->smarty->assign('base_url',base_url());
		$this->session = session();
		$this->smarty->assign('auth',$this->session->get());
        $this->model=new Mref();
    }
    public function index()
    {
		//echo $this->session->get('USER_ID');exit;
	   if((!$this->session->get('USER_ID'))){
			return redirect()->to(base_url().'/login');
		}
       $this->smarty->display('main.html');
    }
	
	public function getkonten($p1="",$p2="")
    {
		if((!$this->session->get('USER_ID'))){
			return redirect()->to(base_url().'/login');
		}
		//echo $this->request->getPost('aksi');exit;
		switch($p1){
			case "ip":
				
				if($this->request->getPost('aksi')){
					
					if($this->request->getPost('aksi')=='edit'){
						$data=$this->model->get_data('ip','edit');
						$this->smarty->assign('dt',$data);
						$this->smarty->assign('sts','edit');
					}
					$konten="Ref/form_ip.html";
					
					return $this->smarty->display($konten);
				}else{
					$konten="Ref/ip.html";
				}
			break;
			case "supplier":
				
				if($this->request->getPost('aksi')){
					$contri=$this->model->get_data('dt-country');
					if($this->request->getPost('aksi')=='edit'){
						$data=$this->model->get_data('supplier','edit');
						$this->smarty->assign('dt',$data);
						$this->smarty->assign('sts','edit');
					}
					$konten="Ref/form_supplier.html";
					$this->smarty->assign('contri',$contri);
					return $this->smarty->display($konten);
				}else{
					$konten="Ref/supplier.html";
				}
			break;
			case "exporter":
				if($this->request->getPost('aksi')){
					$contri=$this->model->get_data('dt-country');
					if($this->request->getPost('aksi')=='edit'){
						$data=$this->model->get_data('exporter','edit');
						$this->smarty->assign('dt',$data);
						$this->smarty->assign('sts','edit');
					}
					$this->smarty->assign('contri',$contri);
					$konten="Ref/form_exporter.html";
					
					return $this->smarty->display($konten);
				}else{
					$konten="Ref/exporter.html";
				}
				
			break;
			case "hs":
				if($this->request->getPost('aksi')){
					//$hs=$this->db->table('ref_hsdetail')->getWhere(['commcode' => $this->session->get('commodity_code')])->getResultArray();
					$hs=$this->model->get_data('dt-hs');
					$contri=$this->model->get_data('dt-country');
					//echo "<pre>";print_r($hs);exit;
					if($this->request->getPost('aksi')=='edit'){
						$data=$this->model->get_data('hs-module','edit');
						$this->smarty->assign('dt',$data);
						//echo "<pre>";print_r($data);
						$this->smarty->assign('sts','edit');
					}
					$this->smarty->assign('hs',$hs);
					$this->smarty->assign('contri',$contri);
					$konten="Ref/form_hs.html";
					
					return $this->smarty->display($konten);
				}else{
					$konten="Ref/hs_modul.html";
				}
				
			break;
		}
		$this->smarty->assign('konten',$konten);
        $this->smarty->display('main.html');
    }
	public function getdata($p1="",$p2="")
    {
		if((!$this->session->get('USER_ID'))){
			return redirect()->to(base_url().'/login');
		}
		$model=new Mref();
		switch($p1){
			case "supplier":
				echo $dt=$model->get_data('supplier');
				exit;
				
			break;
			case "exporter":
				echo $dt=$model->get_data('exporter');
				exit;
				
			break;
			case "hs":
				echo $dt=$model->get_data('hs');
				exit;
				
			break;
			case "ip":
				echo $dt=$model->get_data('ip');
				exit;
				
			break;
			case "list":
				$konten="VR/list.html";
			break;
		}
		echo json_encode($dt);
    }
	
	public function getmodal()
    {
		if((!$this->session->get('USER_ID'))){
			return redirect()->to(base_url().'/login');
		}
		$mod=$this->request->getPost('mod');
		switch($mod){
			case "supplier":
				$konten="VR/data-supplier.html";
			break;
			
		}
		$this->smarty->assign('konten',$konten);
        $this->smarty->display($konten);
    }
	
	public function crud($p1)
    {
		$post = array();
		//echo $this->session->get('commodity_code');exit;
        foreach($_POST as $k=>$v){
			if($this->request->getPost($k)!=""){
				//$post[$k] = $this->db->escape_str($this->input->post($k));
				$post[$k] = $this->request->getPost($k);
			}
			
		}
		/*if( $this->model->simpan_data($p1,$post)){
			if($_POST['sts']=='delete'){
				echo 1;
			}else{
				return redirect()->to(base_url().'/list-'.$p1);
			}
		}else{
			 return redirect()->to(base_url().'/list-'.$p1);
		}*/
		echo $this->model->simpan_data($p1,$post);
		
	
	}
	
}

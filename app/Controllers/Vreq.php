<?php

namespace App\Controllers;
use App\Models\Mvreq;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;
class Vreq extends BaseController
{
	public function initController(
        RequestInterface $request,
        ResponseInterface $response,
        LoggerInterface $logger
    ) {
        parent::initController($request, $response, $logger);
		$this->smarty->assign('base_url',base_url());
		$this->session = session();
		$this->smarty->assign('auth',$this->session->get());
        $this->smarty->assign('base_url',base_url());
		$this->model=new Mvreq();
    }
    public function index()
    {
		//echo $this->session->get('USER_ID');exit;
	   if((!$this->session->get('USER_ID'))){
			return redirect()->to(base_url().'/login');
		}
       $this->smarty->display('main.html');
    }
	
	public function getkonten($p1="",$p2="")
    {
		if((!$this->session->get('USER_ID'))){
			return redirect()->to(base_url().'/login');
		}
		switch($p1){
			case "vreq":
				$curr=$this->model->get_data('currency');
				$this->smarty->assign('curr',$curr);
				$konten="VR/vreq.html";
			break;
			case "plb":
				$konten="VR/plb.html";
			break;
			case "kbgb":
				$konten="VR/kbgb.html";
			break;
			case "list":
				$konten="VR/list.html";
			break;
			case "upload_dok":
				$this->smarty->assign("vr_submit_no",$this->request->getPost('vr'));	
				$this->smarty->assign("comodity", $this->session->get('comodity'));	
				//$this->smarty->assign("flag_plb", $p3);
				
				$sts='add';
				$data=$this->model->get_data("vr_doc",$this->request->getPost('vr'));
				
				$file=$this->model->get_data("cl_vr_kategori_file",$this->session->get('commodity_code'),$this->request->getPost('vr'));
				//print_r($file);exit;
				
				//$io_number=$this->db->get_where('vpti2_vr_submission_header',array('vr_submit_no'=>$p2))->row('io_number');
				//$this->smarty->assign("io_number", $io_number);
				//print_r($io_number);
				if(count($data["res"])>0){
					$sts='edit';
					$this->smarty->assign("datana", $data["rec"]);	
				}
				$this->smarty->assign("sts", $sts);	
				$this->smarty->assign("file", $file);	
				$this->smarty->assign("com_code", $this->session->get('commodity_code'));	
				
			
				$konten="VR/form_upload_dok.html";
				echo $this->smarty->display($konten);
				exit;
			//	echo "xxx";exit;
			break;
			case "edit_detil":
				$hs=$this->model->get_data('dt-hs');
				$contri=$this->model->get_data('dt-country');
				$data=$this->model->get_data('hs-module',$this->request->getPost('vr'));
				$this->smarty->assign('dt',$data);
				$this->smarty->assign('hs',$hs);
				$this->smarty->assign('contri',$contri);
				//echo "<pre>";print_r($data);exit;
				$this->smarty->assign('sts','edit');
				$konten="VR/edit_detil.html";
				echo $this->smarty->display($konten);
				exit;
				//echo "xxx";exit;
			break;
		}
		$this->smarty->assign('konten',$konten);
        $this->smarty->display('main.html');
    }
	public function getdata($p1="",$p2="")
    {
		if((!$this->session->get('USER_ID'))){
			return redirect()->to(base_url().'/login');
		}
		$model=new Mvreq();
		switch($p1){
			case "vr":
				echo $dt=$model->get_data('list_vr');
				exit;
				
			break;
			case "hs-module-ref":
				echo $dt=$model->get_data('hs-module-ref');
				exit;
				
			break;
			case "supplier":
				echo $dt=$model->get_data('supplier');
				exit;
				
			break;
			case "port_loading":
				echo $dt=$model->get_data('port_loading');
				exit;
				
			break;
			case "port_discharge":
				echo $dt=$model->get_data('port_discharge');
				exit;
				
			break;
			case "ip":
				echo $dt=$model->get_data('ip');
				exit;
				
			break;
			case "exporter":
				echo $dt=$model->get_data('exporter');
				exit;
				
			break;
			case "list":
				$konten="VR/list.html";
			break;
		}
		echo json_encode($dt);
    }
	
	public function getmodal()
    {
		if((!$this->session->get('USER_ID'))){
			return redirect()->to(base_url().'/login');
		}
		$mod=$this->request->getPost('mod');
		switch($mod){
			case "supplier":
				$konten="VR/data-supplier.html";
			break;
			case "hs-module":
				$konten="VR/data-hsmodule.html";
			break;
			case "port_loading":
				$konten="VR/data-portloading.html";
			break;
			case "port_discharge":
				$konten="VR/data-portdischarge.html";
			break;
			case "ip":
				$konten="VR/data-ip.html";
			break;
			case "exporter":
				$konten="VR/data-exporter.html";
			break;
		}
		$this->smarty->assign('konten',$konten);
        $this->smarty->display($konten);
    }
	
	public function submit_vr()
    {
		$post = array();
        foreach($_POST as $k=>$v){
			if($this->request->getPost($k)!=""){
				//$post[$k] = $this->db->escape_str($this->input->post($k));
				$post[$k] = ($this->request->getPost($k)=='null' ? null : $this->request->getPost($k)) ;
			}
			
		}
		echo $this->model->submit_vr($post);
		
	
	}
	public function tes_pdf()
    {
		$mpdf = new \Mpdf\Mpdf();
		$html = "<p>TESTING.....</p>";
		$mpdf->WriteHTML($html);
		$this->response->setHeader('Content-Type', 'application/pdf');
		$mpdf->Output('arjun.pdf','I'); // opens in browser
	}
	public function viewpdf($vr)
    {
		
		$mpdfConfig = array(
				'mode' => 'utf-8', 
				'format' => 'A4',    // format - A4, for example, default ''
				'default_font_size' => 0,     // font size - default 0
				'default_font' => '',    // default font family
				//'mgl'=>12.7,
				//'mgr'=>12.7,
				//'mgt'=>36,
				//'mgb'=>20,
				//'mgh'=>30,
				//'mgf'=>10,
				'margin_left' => 15,
				'margin_top' => 35,
				'margin_header' => 8,     // 9 margin header
				'margin_footer' => 5,     // 9 margin footer
				'margin-bottom' => 10,     // 9 margin footer
				'orientation' => 'P'  	// L - landscape, P - portrait
			);
		$mpdf = new \Mpdf\Mpdf($mpdfConfig);
		$model=new Mvreq();
		//$dt_vr=$model->get_data('list_vr','get',$vr);		
		
		$userData = $this->session->get();
		$this->smarty->assign("dataCompany", $userData);
		
        $this->smarty->assign("pic", strtoupper($this->session->get('PIC')));
		$this->smarty->assign('vr_submission_no',$vr);
        //$this->smarty->assign('vr_submission_date',$dt_vr["vr_submit_date"]);
		$no=$this->get_no($this->session->get("commodity_code"));
		$this->smarty->assign("no", $no);
		//echo $no;exit;
		//$this->smarty->assign('com_code',$subDate);
        $dataHeader = $model->getDataDetail($vr);
		$this->smarty->assign('dataDetail',$dataHeader);
        $this->smarty->assign('detailSubmission',$model->getDetailSubmission($vr));
		
		$htmlcontent = $this->smarty->fetch("VR/vr_pdf_2022.tpl");
		$htmlheader = $this->smarty->fetch("VR/vr_pdf_header_2022.tpl");
		
		//echo $htmlcontent;exit;
		//print_r($dataHeader);exit;
		
		//$spdf = new mPDF('', 'A4', 0, '', 12.7, 12.7, 36, 20, 10, 10, 'P');
		$mpdf->ignore_invalid_utf8 = true;
		// bukan sulap bukan sihir sim salabim jadi apa prok prok prok
		$mpdf->allow_charset_conversion = true;     // which is already true by default
		$mpdf->charset_in = 'iso-8859-1';  // set content encoding to iso
		$mpdf->SetDisplayMode('fullpage');		
		$mpdf->SetHTMLHeader($htmlheader);
		//$spdf->keep_table_proportions = true;
		$mpdf->useSubstitutions=false;
		$mpdf->simpleTables=true;
		
		$mpdf->SetHTMLFooter('
			<div style="font-family:arial; font-size:8px; text-align:center; font-weight:bold;">
				<table width="100%" style="font-family:arial; font-size:8px;">
					<tr>
						<td width="30%" align="left">
						</td>
						<td width="40%" align="center">
						</td>
						<!--td width="30%" align="right">
							Hal. {PAGENO} dari {nbpg}
						</td-->
						<td width="30%" align="right">
							Halaman {PAGENO} / Page {PAGENO}
						</td>
					</tr>
				</table>
			</div>
		');				
		$filename = $vr;
		
		$mpdf->WriteHTML($htmlcontent);
		$this->response->setHeader('Content-Type', 'application/pdf');
		$mpdf->Output($filename.'.pdf','I'); // opens in browser
	}
	function get_no($com_id){
		$no='-';
		switch($com_id){
			case "01":
				$no="No. 0".$com_id."/KSO-VPTI/I/2013";
			break;
			case "02":
				$no="No. 0".$com_id."/KSO-VPTI/IV/2003";
			break;
			case "03":
				$no="No. 0".$com_id."/KSO-VPTI/VII/2003";
			break;
			case "04":
				$no="No. 0".$com_id."/KSO-VPTI/I/2013";
			break;
			case "05":
				$no="No. 0".$com_id."/KSO-VPTI/IV/2008";
			break;
			case "06":
				$no="No. 0".$com_id."/KSO-VPTI/I/2013";
			break;
			case "07":
				$no="No. 0".$com_id."/KSO-VPTI/XI/2005";
			break;
			case "08":
				$no="No. 0".$com_id."/KSO-VPTI/X/2004";
			break;
			case "09":
				$no="No. 0".$com_id."/KSO-VPTI/IX/2003";
			break;
			case "10":
				$no="No. ".$com_id."/KSO-VPTI/V/2004";
			break;
			case "11":
				$no="No. ".$com_id."/KSO-VPTI/I/2006";
			break;
			case "12":
				$no="No. ".$com_id."/KSO-VPTI/V/2006";
			break;
			case "13":
				$no="No. ".$com_id."/KSO-VPTI/IX/2009";
			break;
			case "14":
				$no="No. 14/KSO-VPTI/II/2013";
			break;
			case "15":
				$no="No. 15/KSO-VPTI/II/2013";
			break;
			case "16":
				$no="No. 16/KSO-VPTI/II/2013";
			break;
			case "17":
				$no="No. 17/KSO-VPTI/II/2013";
			break;
			case "18":
				$no="No. ".$com_id."/KSO-VPTI/XII/2010";
			break;
			case "19":
				$no="No. ".$com_id."/KSO-VPTI/X/2009";
			break;
			case "20":
				$no="No. 20/KSO-VPTI/II/2013";
			break;
			case "21":
				$no="No. ".$com_id."/KSO-VPTI/X/2011";
			break;
			case "22":
				$no="No. ".$com_id."/KSO-VPTI/I/2012";
			break;
			case "23":
				$no="No. ".$com_id."/KSO-VPTI/III/2012";
			break;
			case "24":
				$no="No. ".$com_id."/KSO-VPTI/VI/2013";
			break;
			case "25":
				$no="No. ".$com_id."/KSO-VPTI/II/2012";
			break;
			case "26":
				$no="No. 26/KSO-VPTI/II/2013";
			break;
			case "27":
				$no="No. ".$com_id."/KSO-VPTI/VI/2013";
			break;
			case "28":
				$no="No. ".$com_id."/KSO-VPTI/VI/2013";
			break;
			case "29":
				$no="No. 29/KSO-VPTI/II/2013";
			break;
			case "30":
				$no="No. ".$com_id."/KSO-VPTI/XII/2013";
			break;
			case "31":
				$no="No. ".$com_id."/KSO-VPTI/VI/2014";
			break;
			case "32":
				$no="No. ".$com_id."/KSO-VPTI/VI/2014";
			break;
			case "33":
				$no="No. ".$com_id."/KSO-VPTI/I/2015";
			break;
			case "34":
				$no="No. ".$com_id."/KSO-VPTI/X/2015";
			break;
			case "35":
				$no="No. ".$com_id."/KSO-VPTI/XI/2016";
			break;
			case "36":
				$no="No. ".$com_id."/KSO-VPTI/XII/2016";
			break;
			case "37":
				$no="No. ".$com_id."/KSO-VPTI/XII/2016";
			break;
			case "38":
				$no="No. ".$com_id."/KSO-VPTI/XII/2017";
			break;
			case "39":
				$no="No. ".$com_id."/KSO-VPTI/II/2018";
			break;
			case "40":
				$no="No. ".$com_id."/KSO-VPTI/IV/2018";
			break;
			case "41":
				$no="No. ".$com_id."/KSO-VPTI/V/2018";
			break;
			case "42":
				$no="No. ".$com_id."/KSO-VPTI/VI/2018";
			break;
			case "43":
				$no="No. ".$com_id."/KSO-VPTI/IX/2020";
			break;
			case "44":
				$no="No. ".$com_id."/KSO-VPTI/IX/2020";
			break;
			case "45":
				$no="No. ".$com_id."/KSO-VPTI/IX/2020";
			break;
		}
		
		return $no;
	}
	
	function upload_dok(){
		//echo "<pre>";print_r($_FILES);exit;
		$data=array();
		$file_namena=array();
		$data["vr_submit_no"]=$_POST['vr_submit_no'];
		$data["com_code"]=$_POST['com_code'];
		
		
		/*if($flag_plb == '1'){
			$upload_dir_ftp="/Y.".$_POST['com_code']."/dev_".$_POST['vr_submit_no']."/";
		}elseif($flag_kbgb == '1'){
			$upload_dir_ftp="/Z.".$_POST['com_code']."/dev_".$_POST['vr_submit_no']."/";
		}else{
			$upload_dir_ftp="/X.".$_POST['com_code']."/dev_".$_POST['vr_submit_no']."/"; 
		}
		*/
		$upload_dir_ftp="/X.".$_POST['com_code']."/dev_".$_POST['vr_submit_no']."/"; 	
		//$upload_dir_ftp="/X.255/".$_POST['vr_submit_no']."/";
		
		foreach($_FILES as $v=>$x){
			//echo $v;exit;
			$fileElementName=$v;
			$id=explode("_",$v);
			if($x["name"] != ''){
				$fName = $_FILES[$fileElementName]['tmp_name'];
				$type = $_FILES[$fileElementName]["type"];
				$fSize = @filesize($_FILES[$fileElementName]['tmp_name']);
				@unlink($_FILES[$fileElementName]);		
				$filename = $_FILES[$fileElementName]['name'];
					
				$name = explode(".", $filename);
				$accepted_types = array('application/zip', 'application/x-zip-compressed', 'multipart/x-zip', 'application/x-compressed');
				foreach($accepted_types as $mime_type) {
					if($mime_type == $type) {
						$okay = true;
						break;
					} 
				}	
					
				$upload_dir_doc="__repo/vr_doc/";
				$upload_dir_submit=$upload_dir_doc.$_POST['vr_submit_no']."/";
				$uploadfile = $upload_dir_submit . $filename;
				
				if(!file_exists($upload_dir_doc))mkdir($upload_dir_doc, 0777, true);
				if(!file_exists($upload_dir_submit))mkdir($upload_dir_submit, 0777, true);				
				
				$name_filein=array();
				
				if(move_uploaded_file($fName, $uploadfile)) {					
					if($id[0]!="LL"){
						$zip = new \ZipArchive();
						$x = $zip->open($uploadfile);
						if ($x === true) {
							$zip->extractTo($upload_dir_submit); // change this to the correct site path
							for ($i = 0; $i < $zip->numFiles; $i++) {
								 $name_filein[]=$zip->getNameIndex($i);
							 }
							//$name_filein=$zip->getNameIndex(0);
							$zip->close();
					
							unlink($uploadfile);
						}
					}else{
						$name_filein[]=$x["name"];
					}
					
					if(count($name_filein)>0){
						$waktu_kirim = date('YmdHis');
						//$idx=1;
						$nama_filena="";
						
						$get_doc=$this->model->get_data("vpti2_vr_doc",$_POST["vr_submit_no"],$id[1]);
						 if(isset($get_doc["id"])){
							$data['sts']='edit';
							$data['id']=$get_doc["id"];
							$file_na=$get_doc["file_name"];
							$this->model->hapus_doc($get_doc["id"]);
							//$this->db->where('vpti2_vr_document_id',$get_doc["id"]);
							//$this->db->delete('vpti2_vr_ceklist');
							if($get_doc["file_name"]!=""){
								$file_nya=explode("|",$get_doc["file_name"]);
								//echo count($file_nya);exit;
								if(count($file_nya)>0){
									$idx=count($file_nya)-1;
									$nama_filena=$file_na;
									//echo $idx;exit;
								}
								else{$idx=0;}								
							}
						}else{
							$data['sts']='add';$idx=0;
						}
						foreach($name_filein as $x){
							//~ $pecah=explode(".",$x);
							$idx++;
							$pecah = pathinfo($x, PATHINFO_EXTENSION);
							//	$newFilename = $_POST['vr_submit_no']."_X_".$this->auth["commodity_code"]."_".$id[0]."_".$idx;
							$newFilename = $_POST['vr_submit_no']."_".$id[0]."_".$idx;
							$newFilename = $this->buang_karakter_spesial($newFilename).'.'.$pecah;
							
							$file = $upload_dir_submit . $newFilename;
							
							
							$nama_filena .=$newFilename."|";
							if(file_exists($upload_dir_submit.$newFilename)){
								chmod($upload_dir_submit.$newFilename,0777);
								unlink($upload_dir_submit.$newFilename);
							}
							
							rename($upload_dir_submit.$x, $upload_dir_submit.$newFilename);
							 
							//FTP TRANSFER MANGGGGG 
							/*
							$ftp_server=$this->config->item('FTP_host');
							$ftp_user_name=$this->config->item('FTP_user');
							$ftp_user_pass=$this->config->item('FTP_pwd');
							$conn_id = ftp_connect($ftp_server)or die("Couldn't connect to $ftp_server");
							if (@ftp_login($conn_id, $ftp_user_name, $ftp_user_pass)) {
								ftp_pasv($conn_id, true);
								$mkdir=get_direktorina_ftp($conn_id,$upload_dir_ftp);
								if($mkdir==1){
									if(ftp_put($conn_id, $newFilename, $file, FTP_BINARY)){
										$msg=1;
									}
									else{
										$msg=4;
										break;
										echo $msg;exit;
									}
								}else{
									$msg=3;
									break;
									echo $msg;exit;
								}
								
							}
							else{$msg=2;
								break;
								echo $msg;exit;
							}
							
							ftp_close($conn_id);
							chmod($upload_dir_submit.$newFilename,0777);
							unlink($upload_dir_submit.$newFilename);
							*/
						}
					}
						
					$data['cl_vr_kategori_file_id']=$id[1];
					$data['file_name']=$nama_filena;
					$data["upload_date"]=date('Y-m-d H:i:s');
					//print_r($data);exit;
					if($this->model->simpan_filedoc('vpti2_vr_document',$data)){
						$msg=1;
					}
				}
					
				else{
					$msg=2;
					break;
					echo $msg;exit;
				}
			}
		}

		echo $msg;
		
		
		
	}
	
	function buang_karakter_spesial($s) {
		$old_pattern = array("/[^a-zA-Z0-9]/", "/_+/", "/_$/");
        $new_pattern = array("_", "_", "");
		$result = preg_replace($old_pattern,$new_pattern, $s);
		return $result;
		//echo $result;
	}
	function download_doc(){
		$vr=$_POST['vr_submit_no'];
		$com_code=$_POST['com_code'];
		//$flag=$_POST['flag'];
		$file=$_POST['file'];
		//print_r($_POST);exit; 
		//echo $flag_plb;exit();
		//if($flag_plb == '1'){
			//$upload_dir_ftp="/".$flag.".".$com_code."/".$vr."/".$file;
		//}else{
			//$upload_dir_ftp="/X.".$com_code."/".$vr."/".$file;
		//}
		
		//echo $upload_dir_ftp;exit;
		
		
		$upload_dir_ftp="__repo/vr_doc/".$vr."/".$file;
		$name=$file;
		//$data=file_get_contents("ftp://fd_kso2:fd_kso2@192.168.1.213".$upload_dir_ftp);
		$data=file_get_contents($upload_dir_ftp);
		return  $this->response->download($name, $data);
		//force_download($name,$data);
			
	}
	
	function edit_detil(){
		$post = array();
        foreach($_POST as $k=>$v){
			if($this->request->getPost($k)!=""){
				//$post[$k] = $this->db->escape_str($this->input->post($k));
				$post[$k] = ($this->request->getPost($k)=='null' ? null : $this->request->getPost($k)) ;
			}
			
		}
		echo $this->model->edit_detil($post);
	}
}

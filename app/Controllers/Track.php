<?php

namespace App\Controllers;
use App\Models\Mtrack;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;
use CodeIgniter\Model;
use Endroid\QrCode\Color\Color;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelLow;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\Label\Label;
use Endroid\QrCode\Logo\Logo;
use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use Endroid\QrCode\Writer\PngWriter;
class Track extends BaseController
{
	public function initController(
        RequestInterface $request,
        ResponseInterface $response,
        LoggerInterface $logger
    ) {
        parent::initController($request, $response, $logger);
		$this->db = db_connect();
		$this->smarty->assign('base_url',base_url());
		$this->session = session();
		$this->smarty->assign('auth',$this->session->get());
        $this->model=new Mtrack();
    }
    public function index()
    {
		//echo $this->session->get('USER_ID');exit;
	   if((!$this->session->get('USER_ID'))){
			return redirect()->to(base_url().'/login');
		}
       $this->smarty->display('main.html');
    }
	
	public function getkonten($p1="",$p2="")
    {
		if((!$this->session->get('USER_ID'))){
			return redirect()->to(base_url().'/login');
		}
		//echo $this->request->getPost('aksi');exit;
		switch($p1){
			case "list_upload":
			
				$io=$this->request->getPost('io_number');		
				$file_cat=$this->request->getPost('kat');
				$partial=$this->request->getPost('partial');
				$ls=$this->request->getPost('ls_number');
				$info=$this->model->get_data('list_upload',$this->request->getPost("id"));
				$ex=explode('|',$info["file_doc_1"]);
				//echo "<pre>";print_r($ex);exit;
				$this->smarty->assign('info',$info);
				$this->smarty->assign('ex',$ex);
				$this->smarty->assign('io',$io);
				$this->smarty->assign('file_cat',$file_cat);
				$this->smarty->assign('partial',$partial);
				$this->smarty->assign('ls',$ls);
				$konten="Track/list_upload.html";
				echo $this->smarty->display($konten);
				exit;
			break;
			case "monitor_status":
				if($p2!=""){
					$this->smarty->assign('io_number',$p2);
				}
				$konten="Track/status.html";
			break;
			case "view_status":
				$info=$this->model->get_data('info_status');
				//echo "<pre>";print_r($info);exit;
				$this->smarty->assign('info',$info);
				$konten="Track/view_status.html";
				echo $this->smarty->display($konten);
				exit;
			break;
			case "form_fd":
				$vo=$this->request->getPost("vo");
				$partial=$this->request->getPost("partial");
				$ls=$this->request->getPost("ls");
				$kat=$this->model->get_data('cl_category_file');
				//print_r($kat);exit;
				$icon=$this->model->get_data("get_icon",$vo,$partial,$ls);
				$this->smarty->assign('icon',$icon);
				$this->smarty->assign('vo',$vo);
				$this->smarty->assign('partial',$partial);
				$this->smarty->assign('ls',$ls);
				$this->smarty->assign('kat',$kat);
				$this->smarty->assign('commodity',$this->session->get('commodity_code'));
				$konten="Track/form_upload_fd.html";
				echo $this->smarty->display($konten);
				exit;
			break;
			
		}
		$this->smarty->assign('konten',$konten);
        $this->smarty->display('main.html');
    }
	public function getdata($p1="",$p2="")
    {
		if((!$this->session->get('USER_ID'))){
			return redirect()->to(base_url().'/login');
		}
		$model=new Mtrack();
		switch($p1){
			case "get_vo":
				echo $dt=$model->get_data('get_vo');
				exit;
				
			break;
			
		}
		echo json_encode($dt);
    }
	
	public function getmodal()
    {
		if((!$this->session->get('USER_ID'))){
			return redirect()->to(base_url().'/login');
		}
		$mod=$this->request->getPost('mod');
		switch($mod){
			case "supplier":
				$konten="VR/data-supplier.html";
			break;
			
		}
		$this->smarty->assign('konten',$konten);
        $this->smarty->display($konten);
    }
	
	public function crud($p1)
    {
		$post = array();
		//echo $this->session->get('commodity_code');exit;
        foreach($_POST as $k=>$v){
			if($this->request->getPost($k)!=""){
				//$post[$k] = $this->db->escape_str($this->input->post($k));
				$post[$k] = $this->request->getPost($k);
			}
			
		}
		/*if( $this->model->simpan_data($p1,$post)){
			if($_POST['sts']=='delete'){
				echo 1;
			}else{
				return redirect()->to(base_url().'/list-'.$p1);
			}
		}else{
			 return redirect()->to(base_url().'/list-'.$p1);
		}*/
		echo $this->model->simpan_data($p1,$post);
		
	
	}
	function download_vo(){
		//echo $this->request->getPost('vo');exit;
		//set_time_limit(0);
		//ini_set('display_errors', 1);
		
		set_time_limit(200000000);
		$mpdfConfig = array(
				'mode' => 'utf-8', 
				'format' => 'A4',    // format - A4, for example, default ''
				'default_font_size' => 0,     // font size - default 0
				'default_font' => '',    // default font family
				//'mgl'=>12.7,
				//'mgr'=>12.7,
				//'mgt'=>36,
				//'mgb'=>20,
				//'mgh'=>30,
				//'mgf'=>10,
				'margin_left' => 15,
				'margin_top' => 40,
				'margin_header' => 8,     // 9 margin header
				'margin_footer' => 5,     // 9 margin footer
				'margin-bottom' => 10,     // 9 margin footer
				'orientation' => 'P'  	// L - landscape, P - portrait
			);
		$mpdf = new \Mpdf\Mpdf($mpdfConfig);
		//$model=new Mvreq();
		
		$io_number =$this->request->getPost('vo');//'X.16.017346';
		$cekIO = 'SELECT pdf_creator,email_date, registration_number FROM vpti_io_header WHERE io_number = ' .$this->db->escape($io_number);
		$res = $this->db->query($cekIO)->getRowArray();
		
		//print_r($res);exit;
		if (empty($res['pdf_creator']) || empty($res['email_date'])) {
			$this->db->query("UPDATE vpti_io_header SET email_date=GETDATE(),pdf_creator=".$this->db->escape($this->auth['COMPANY_NAME'])." WHERE io_number='".$io_number."'");
		}
		
		$data_io_number = $this->model->get_data('io_header', $io_number);
		$data_io_notify = $this->model->get_data('io_notify_parties', $io_number);
		$data_io_detail = $this->model->get_data('io_detail', $io_number);
		$data_io_remarks = $this->model->get_data('io_remarks', $io_number);
		$data_vr_no = $this->model->get_data('vr_no',$io_number);
		
		$data_importer = $this->model->get_data('importer', $data_io_number['importer_id'], $data_io_number['commodity_code']);
		$data_exporter = $this->model->get_data('exporter', $data_io_number['exporter_id'], $data_io_number['commodity_code']);
		
		$data_ip_header = $this->model->get_data('ip_header', $data_io_number['importer_id'], $data_io_number['commodity_code']);
		
		//~ $data_npik_header = $this->mvreq->get_data('npik_header', $data_io_number[0]['importer_id'], $data_io_number[0]['commodity_code']);
		$data_npik_header = $this->model->get_data('npik_io_license', $io_number);
		
		$data_commodity_desc = $this->model->get_data('comm_desc', $data_io_number['commodity_code']);
		//print_r($data_commodity_desc);exit;
		
		//$expire_vo = date('d/m/Y', strtotime('+6 months', strtotime(str_replace('/', '-', $data_io_number[0]['vo_date']))));
		$expire_vo = date("d/m/Y", strtotime($data_io_number['lc_expiry_date']));
		
		$this->smarty->assign('expire_vo',$expire_vo);
		$this->smarty->assign('data_io_number',$data_io_number);
		$this->smarty->assign('data_io_notify',$data_io_notify);
		$this->smarty->assign('data_io_detail',$data_io_detail);
		$this->smarty->assign('data_io_remarks',$data_io_remarks);
		$this->smarty->assign('data_vr_no',$data_vr_no);
		$this->smarty->assign('comm_code',$data_io_number['commodity_code']);
		
		$this->smarty->assign('data_importer',$data_importer);
		$this->smarty->assign('data_exporter',$data_exporter);
		$this->smarty->assign('data_ip_header',$data_ip_header);
		$this->smarty->assign('data_npik_header',$data_npik_header);
		$this->smarty->assign('data_commodity_desc',$data_commodity_desc);
		
		//$htmlheader = $this->smarty->fetch('Track/vo_header.tpl');
		$htmlheader = $this->smarty->fetch('Track/vo_pdf_header_2022.tpl');
		
		$kodevo = substr($io_number, 0, 1); 
		//$htmlcontent = $this->smarty->fetch('Track/vo_pdf.tpl');
		$htmlcontent = $this->smarty->fetch('Track/vo_pdf_2022.tpl');
		
		//echo $htmlcontent;exit;
		
		$mpdf->ignore_invalid_utf8 = true;
		$mpdf->allow_charset_conversion = true;     // which is already true by default
		$mpdf->charset_in = 'iso-8859-1';  // set content encoding to iso
		$mpdf->SetDisplayMode('fullpage');		
		$mpdf->SetHTMLHeader($htmlheader);
		//$spdf->keep_table_proportions = true;
		$mpdf->useSubstitutions=false;
		$mpdf->simpleTables=true;
		
		//$footAktif = 'This VO may also serve as valid invoice for payment of either minimum fee or full amount of actual fee';
		$footAktif = '';
		
		$mpdf->SetHTMLFooter('
			<div style="font-family:arial; font-size:8px; text-align:center; font-weight:bold;">
				'.$footAktif.'
				<br />
				<table width="100%" style="font-family:arial; font-size:8px;">
					<tr>
						<td width="30%" align="left">
							
						</td>
						<td width="40%" align="center">
							
						</td>
						<td width="30%" align="right">
							Hal. {PAGENO} dari {nbpg}
						</td>
						<!--td width="30%" align="right">
							Halaman {PAGENO} / Page {PAGENO}
						</td-->
					</tr>
				</table>
			</div>
		');				
		
		$filename = $io_number;
		
		$mpdf->WriteHTML($htmlcontent);
		$this->response->setHeader('Content-Type', 'application/pdf');
		$mpdf->Output($filename.'.pdf','I'); // opens in browser
	}
	
	function download_ls(){
			
			//error_reporting(-1);
			//ini_set('display_errors', '1');
			//$amend=$this->input->post('amend');
			//echo $this->input->post('amend');exit;
			set_time_limit(1000000000);
			ini_set('memory_limit', '4095M');
			
			$lsnumber = $this->request->getPost('ls');//$this->input->post('ls_number');
			$lsid	  = $this->request->getPost('ls_id');//$this->input->post('ls_id');
			$ionumber = $this->request->getPost('vo');//$this->input->post('io_number');
			//echo $lsnumber.' -> '.$lsid.'->'.$ionumber;exit;
			//$this->cek_ls_dist($lsid,$ionumber);
			//echo $lsid;exit;
			$datapartial = $this->model->get_data('get_partial',$lsid,$lsnumber,$ionumber);
			//echo "<pre>";print_r($datapartial);exit;
			$partial=$datapartial['partial_number'];
			$permentbaru=$datapartial['permentbaru'];
							
			if($lsnumber){
				//ob_clean();
			
				$datalsheader 		= $this->model->getrecord('ls_header', $lsnumber);
				$datalspackage 		= $this->model->getrecord('ls_package', $lsid, $ionumber);
				$datalsremarks 		= $this->model->getrecord('ls_remarks', $lsid, $ionumber);
				$datalsremarksdetil = $this->model->getrecord('ls_remarks_detail', $lsid, $ionumber);
				$datalsdetail	 	= $this->model->getrecord('ls_detail', $lsid, $ionumber);
			
				//print_r($datalsdetail);exit();
				$dataverificator	= $this->model->getrecord('ls_verificator', $lsid, $ionumber);
				$dataksocode 		= $this->model->getrecord('ksocode', $lsid, $ionumber);
				
				$getcomodity = substr($ionumber, 2, 2);
				$this->smarty->assign('judulcomodity', $this->model->getrecord('commodity_desc', $getcomodity));
				if($getcomodity == '02'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 85/M-DAG/PER/10/2015';
					}
				}elseif($getcomodity == '03'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 622/MPP/KEP/10/2003';
					}
				}elseif($getcomodity == '05'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						//$perment = 'No. 19/M-DAG/PER/3/2008';
						$perment = 'No. 103/M-DAG/PER/12/2015';
					}
				}elseif($getcomodity == '07'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						//$perment = 'No. 58/M-DAG/PER/9/2012';
						$perment = 'No. 125/M-DAG/PER/12/2015';	
					}
				}elseif($getcomodity == '08'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 647/MPP/KEP/10/2004';
					}
				}elseif($getcomodity == '09'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						//$perment = 'No. 18/M-DAG/PER/4/2007';
						//$perment = 'No. 117/M-DAG/PER/12/2015';
						$perment = 'No. 14 TAHUN 2020';
					}
				}elseif($getcomodity == '10'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 35/M-DAG/PER/5/2012';
					}
				}elseif($getcomodity == '11'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 72/M-DAG/PER/11/2012';
					}
				}elseif($getcomodity == '12'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						//$perment = 'No. 07/M-DAG/PER/2/2012';
						$perment = 'No. 102/M-DAG/PER/12/2015';
					}
				}elseif($getcomodity == '13'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						//$perment = 'No. 39/M-DAG/PER/9/2009';
						//$perment = 'No. 31/M-DAG/PER/5/2016';
						$perment = 'No. 84 TAHUN 2019';
					}
				}elseif($getcomodity == '14'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						//$perment = 'No. 36/M-DAG/PER/7/2014';
						//$perment = 'No. 73/M-DAG/PER/10/2014';
						$perment = 'No. 87/M-DAG/PER/10/2015';
					}
				}elseif($getcomodity == '15'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						//$perment = 'No. 36/M-DAG/PER/7/2014';
						//$perment = 'No. 73/M-DAG/PER/10/2014';
						$perment = 'No. 87/M-DAG/PER/10/2015';
					}
				}elseif($getcomodity == '16'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						//$perment = 'No. 36/M-DAG/PER/7/2014';
						//$perment = 'No. 73/M-DAG/PER/10/2014';
						$perment = 'No. 87/M-DAG/PER/10/2015';
					}
				}elseif($getcomodity == '17'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						//$perment = 'No. 73/M-DAG/PER/10/2014';
						$perment = 'No. 87/M-DAG/PER/10/2015';
					}
				}elseif($getcomodity == '18'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						//$perment = 'No. 08/M-DAG/PER/2/2012';
						$perment = 'No. 113/M-DAG/PER/12/2015';
					}
				}elseif($getcomodity == '19'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 71/M-DAG/PER/11/2012';
					}
				}elseif($getcomodity == '20'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						//$perment = 'No. 36/M-DAG/PER/7/2014';
						//$perment = 'No. 73/M-DAG/PER/10/2014';
						$perment = 'No. 87/M-DAG/PER/10/2015';
					}
				}elseif($getcomodity == '21'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 75/M-DAG/PER/10/2014';
					}
				}elseif($getcomodity == '22'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 40/M-DAG/PER/12/2011';
					}
				}elseif($getcomodity == '23'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						//$perment = 'No. 40/M-DAG/PER/7/2014';
						$perment = 'No. 83/M-DAG/PER/10/2015';
					}
				}elseif($getcomodity == '24'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						//$perment = 'No. 47/M-DAG/PER/8/2013';
						//$perment = 'No. 71/M-DAG/PER/9/2015';
						$perment = 'No. 44 TAHUN 2019';
						//$perment = 'No. 30/M-DAG/PER/5/2017';
					}
				}elseif($getcomodity == '25'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						//$perment = 'No. 38/M-DAG/PER/8/2013';
						$perment = 'No. 41/M-DAG/PER/5/2016';
					}
				}elseif($getcomodity == '26'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						//$perment = 'No. 36/M-DAG/PER/7/2014';
						//$perment = 'No. 73/M-DAG/PER/10/2014';
						$perment = 'No. 87/M-DAG/PER/10/2015';
					}
				}elseif($getcomodity == '29'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						//$perment = 'No. 36/M-DAG/PER/7/2014';
						//$perment = 'No. 73/M-DAG/PER/10/2014';
						$perment = 'No. 87/M-DAG/PER/10/2015';
					}
				}elseif($getcomodity == '30'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 40/M-DAG/PER/8/2013';
					}
				}elseif($getcomodity == '31'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 23/M-DAG/PER/4/2014';
					}
				}elseif($getcomodity == '32'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 28/M-DAG/PER/6/2014';
					}
				}elseif($getcomodity == '33'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						//$perment = 'No. 55/M-DAG/PER/9/2014';
						$perment = 'No. 84/M-DAG/PER/10/2015';
					}
				}elseif($getcomodity == '34'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 86/M-DAG/PER/10/2015';
					}
				}elseif($getcomodity == '35'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 77/M-DAG/PER/11/2016';
					}
				}elseif($getcomodity == '36'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						//$perment = 'No. 82/M-DAG/PER/12/2016';
						$perment = 'No. 110 TAHUN 2018';
					}
				}elseif($getcomodity == '37'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 37/M-DAG/PER/7/2014';					
					}
				}elseif($getcomodity == '38'){
					$perment = 'No. 84/M-DAG/PER/11/2017';
				}elseif($getcomodity == '39'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 29 TAHUN 2018';
					}
				}elseif($getcomodity == '40'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 30 TAHUN 2018';
					}
				}elseif($getcomodity == '41'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 86 TAHUN 2017';
					}
				}elseif($getcomodity == '42'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 66 TAHUN 2018';
					}
				}elseif($getcomodity == '43'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 68 TAHUN 2020';
					}
				}elseif($getcomodity == '44'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 68 TAHUN 2020';
					}
				}elseif($getcomodity == '45'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{
						$perment = 'No. 68 TAHUN 2020';
					}
				}elseif($getcomodity == '46'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{}
				}elseif($getcomodity == '47'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{}
				}elseif($getcomodity == '48'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{}
				}elseif($getcomodity == '49'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{}
				}elseif($getcomodity == '50'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{}
				}elseif($getcomodity == '51'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{}
				}elseif($getcomodity == '52'){
					if($permentbaru=='1'){
						$perment = 'No. 20 TAHUN 2021';
					}else{}
				}elseif($getcomodity == '53'){
					if($permentbaru=='1'){
						$perment = '';
					}else{}
				}
								
				//log_download_els 31/08/2016
				$imp_id_newtable = $this->session->get('COMPANY_ID');
				$ceklog = $this->model->logdownload_els($getcomodity,$imp_id_newtable,$lsnumber);
				if(count($ceklog)==0){
					$dataget = $partial;
					
					$data = array(
						'commodity_code'=>$getcomodity,
						'importer_id_new_table'=>$imp_id_newtable,
						'ls_number'=>$lsnumber,
						'io_number'=>$ionumber,
						'partial_number'=>$dataget['partial_number']
					);
					//$this->model->simpan_log_els('vpti2_download_els',$data);
				}
				//end log_download_els
				
				$this->smarty->assign('commodity', $getcomodity);
				$this->smarty->assign('datalsheader', $datalsheader);
				$this->smarty->assign('datalspackage', $datalspackage);
				$this->smarty->assign('datalsremarks', $datalsremarks);
				$this->smarty->assign('datalsremarksdetil', $datalsremarksdetil);
				//$detilnya = htmlentities($datalsdetail);
				$this->smarty->assign('datalsdetail', $datalsdetail);
				$this->smarty->assign('dataverificator', $dataverificator);
				
				//ls_file_monitoring
				//$this->cek_ls_dist($lsid,$ionumber,$datalsheader[0]['status']);
				//end
				
				
				$sql="SELECT * FROM vpti_ls_detail WHERE ls_id='".$lsid."' AND io_number='".$ionumber."' AND flag='1' ";
				$ceklampiran = $this->db->query($sql)->getResultArray();
				if($ceklampiran){
					$datalampiran = $this->model->getrecord('ls_detail_lampiran', $lsid, $ionumber);
					$this->smarty->assign('datalampiran', $datalampiran);
				} else {
					$this->smarty->assign('datalampiran', '');
				}
				
				$this->smarty->assign('lsnumber', $lsnumber);
				$this->smarty->assign('ionumber', $ionumber);
				$this->smarty->assign('perment', $perment);
				
				//$lsforbarcode = substr($lsnumber, (strlen($lsnumber)-3), 3);
				//$ioforbarcode = substr($ionumber, (strlen($ionumber)-3), 3);
				if(isset($dataverificator["inisial"])){
					//$verificatorbarcode = $this->dapetinhurufpertama($dataverificator->checked_by_user);
					$verificatorbarcode = $dataverificator["inisial"];
				}else{
					$verificatorbarcode = "KSO";
					//$verificatorbarcode = $datalsheader[0]['issuance_place'];
				}
				$lsdatebarcode	 	= date('dmY', strtotime($datalsheader[0]['ls_date']));
				
				/*generatebarcode($verificatorbarcode.$lsdatebarcode.$lsnumber, '40', 'horizontal', 'code128', $verificatorbarcode.$lsdatebarcode.$lsnumber);
				$this->smarty->assign('barcode_file', $verificatorbarcode.$lsdatebarcode.$lsnumber);	
				*/
				
				 //QR
				//$en=$this->encrypt->encode($lsnumber);
				//$bse=base64_encode($en);
				//echo $bse;exit;
				//$encript=$bse;
				//$text_qr='https://app-vpti.com/imp';
				
				$text_qr=base_url().'/info-lvhi/'.base64_encode($lsnumber);
				//echo $lsnumber;exit;
				$nm_qr=$this->create_qr($text_qr,$lsnumber);
				//$this->smarty->assign('barcode_file', $verificatorbarcode.$lsdatebarcode.$lsnumber);	
				$this->smarty->assign('barcode_file', $lsnumber);	
				
				
				
				//$filename = $lsnumber;
				
				$filename = $ionumber.'_'.$lsnumber;
				
				/*$htmlheader = $this->smarty->fetch('ls_pdf_header.tpl');
				//$htmlheader = $this->smarty->fetch('ls_pdf_header_new.tpl');
				
				$plb = substr($ionumber, 0, 1); 
				//if($plb=='Y'){
				if($datalsheader[0]['flag_plb']==1){	
					$htmlcontent = $this->smarty->fetch('ls_plb_pdf.tpl');
				}else if($datalsheader[0]['flag_kbgb']==1){	
					$htmlcontent = $this->smarty->fetch('ls_kbgb_pdf.tpl');
					//$htmlcontent = $this->smarty->fetch('ls_kbgb_new.tpl');
				}else{
					$htmlcontent = $this->smarty->fetch('ls_pdf.tpl');
					//$htmlcontent = $this->smarty->fetch('ls_pdf_new.tpl');
				}
				*/
				
				$htmlheader = $this->smarty->fetch('Track/ls_pdf_header_new2021.tpl');			
				//echo $htmlheader;exit;
				//periode < 2022-10-19
				//$htmlheader = $this->smarty->fetch('ls_header.tpl');
				//periode > 2022-10-19
				//$htmlheader = $this->smarty->fetch('ls_header2.tpl'); 
				
				/*
				$plb = substr($ionumber, 0, 1);  
				//if($plb=='Y'){
				if($datalsheader[0]['flag_plb']==1){
					if($getcomodity == '53'){
						if($datalsheader[0]['status'] == 'N'){
							$htmlcontent = $this->smarty->fetch('ls_bmtb_2022_batam.tpl');
						}else{
							$htmlcontent = $this->smarty->fetch('ls_bmtb_2022.tpl');
						}
					}else{
						//$htmlcontent = $this->smarty->fetch('ls_plb_pdf.tpl');
						$htmlcontent = $this->smarty->fetch('ls_plb_new2021.tpl');
					}
				}else if($datalsheader[0]['flag_kbgb']==1){
					if($getcomodity == '53'){
						if($datalsheader[0]['status'] == 'N'){
							$htmlcontent = $this->smarty->fetch('ls_bmtb_2022_batam.tpl');
						}else{
							$htmlcontent = $this->smarty->fetch('ls_bmtb_2022.tpl');
						}
					}else{
						//$htmlcontent = $this->smarty->fetch('ls_kbgb_pdf.tpl');
						$htmlcontent = $this->smarty->fetch('ls_kbgb_new.tpl');
					}
				}else{
					if($getcomodity == '53'){
						if($datalsheader[0]['status'] == 'N'){
							$htmlcontent = $this->smarty->fetch('ls_bmtb_2022_batam.tpl');
						}else{
							$htmlcontent = $this->smarty->fetch('ls_bmtb_2022.tpl');
						}
					}else{
						$htmlcontent = $this->smarty->fetch('ls_pdf_new2021.tpl');	
					}
				}
				*/
				$htmlcontent = $this->smarty->fetch('Track/ls_pdf_new2021.tpl');	
				//echo $htmlcontent;exit;
				/*if($getcomodity == '53'){
					$htmlfooter = $this->smarty->fetch('ls_bmtb_footer_2022.tpl');
				}else{
					$htmlfooter = $this->smarty->fetch('ls_pdf_footer_new2021.tpl');
				}
				*/
				$htmlfooter = $this->smarty->fetch('Track/ls_pdf_footer_new2021.tpl');
				//echo $htmlfooter;exit;
				//$htmlfooter = $this->smarty->fetch('ls_pdf_footer_new.tpl');
				//echo $htmlcontent;exit;
				
				$mpdfConfig = array(
						'mode' => 'utf-8', 
						'format' => 'A4',    // format - A4, for example, default ''
						'default_font_size' => 0,     // font size - default 0
						'default_font' => '',    // default font family
						//'mgl'=>12.7,
						//'mgr'=>12.7,
						//'mgt'=>36,
						//'mgb'=>20,
						//'mgh'=>30,
						//'mgf'=>10,
						'margin_left' => 15,
						'margin_top' => 40,
						'margin_header' => 8,     // 9 margin header
						'margin_footer' => 5,     // 9 margin footer
						'margin-bottom' => 10,     // 9 margin footer
						'orientation' => 'P'  	// L - landscape, P - portrait
					);
				$mpdf = new \Mpdf\Mpdf($mpdfConfig);
				
				$mpdf->ignore_invalid_utf8 = true;
				$mpdf->allow_charset_conversion = true;     // which is already true by default
				$mpdf->charset_in = 'iso-8859-1';  // set content encoding to iso
				$mpdf->SetDisplayMode('fullpage');		
				$mpdf->SetHTMLHeader($htmlheader);
				//$spdf->keep_table_proportions = true;
				$mpdf->useSubstitutions=false;
				$mpdf->simpleTables=true;
				
				$this->smarty->assign('ksokodenya', $dataksocode);
				$mpdf->SetHTMLFooter($htmlfooter);	
				$mpdf->SetProtection(array('copy','print'));				
				$mpdf->WriteHTML($htmlcontent);
				$this->response->setHeader('Content-Type', 'application/pdf');
				$mpdf->Output($filename.'.pdf','I'); // opens in browser
				$mpdf->Output('repositories/ls/'.$filename.'.pdf', 'I'); // save to file because we can
			//~ $spdf->Output('repositories/Dokumen_LS/LS_PDF/'.$filename.'.pdf', 'I'); // view file
			//ob_end_flush();
		}
		else{
			echo '<center><div class="box-info">
				<h1 style="font-size:30px;font-weight:bold;">WARNING !!!</h1>
				<h1 style="font-size:20px;font-weight:bold;">Ooopssss... Sorry</h1>
				<br />
				<img height="150px" align="middle" src="'.base_url().'assets/images/no_access.png">
			</div></center>';
		}
	}
	
	function cek_ls_dist($lsid,$ionumber,$status){
		$seq_baru=$this->model->get_data('get_seq_no',$ionumber,$lsid);
		if($status=='I'){
			$sql="SELECT * FROM ls_file_monitoring WHERE io_number='".$ionumber."' AND ls_id='".$lsid."' AND status_code='5' AND note='e-LS' ";
			$jml=$this->db->query($sql)->getResultArray();
			if(count($jml)!=0){
				$sql="SELECT * FROM ls_file_monitoring WHERE io_number='".$ionumber."' AND ls_id='".$lsid."' AND status_code='6' ";
				$ex=$this->db->query($sql)->getResultArray();
				
				if(count($ex)==0){
					
					//echo 'Tidak Ada 6';
					$data=array('seq_no'=>$seq_baru["seq_baru"],
								'io_number'=>$ionumber,
								'ls_id'=>$lsid,
								'checked_by_user'=>'VPTI-Online',
								'checked_date'=>date('Y-m-d H:i:s'),
								'status_code'=>'6',
								'flag_ls_reprint'=>'F',
								'flag_NNLS'=>'F',
								'flag_amend'=>'F',
								'partial_number'=>$seq_baru["partial_number"],
								'note'=>$this->session->get('COMPANY_NAME'),								
					);
					$this->model->simpan_monitoring('ls_file_monitoring',$data);
					//$this->db->insert('ls_file_monitoring',$data);
				}
				$sql="SELECT * FROM ls_file_monitoring WHERE io_number='".$ionumber."' AND ls_id='".$lsid."' AND status_code='7' ";
				$ex=$this->db->query($sql)->getResultArray();
				if(count($ex)==0){
					
					$data=array('seq_no'=>$seq_baru["seq_baru"],
								'io_number'=>$ionumber,
								'ls_id'=>$lsid,
								'checked_by_user'=>'VPTI-Online',
								'checked_date'=>date('Y-m-d H:i:s'),
								'status_code'=>'7',
								'flag_ls_reprint'=>'F',
								'flag_NNLS'=>'F',
								'flag_amend'=>'F',
								'partial_number'=>$seq_baru["partial_number"],
								'note'=>'Downloaded by '.$this->session->get('COMPANY_NAME'),								
					);
					return $this->model->simpan_monitoring('ls_file_monitoring',$data);
					//return $this->db->insert('ls_file_monitoring',$data);
				}
				
				//cek AMEND untuk insert di Kolom AMEND
				$sql="SELECT * FROM ls_file_monitoring WHERE io_number='".$ionumber."' AND ls_id='".$lsid."' AND status_code='5' AND note='E-LS' AND flag_amend='T' ";
				//$ex=$this->db->get_where('ls_file_monitoring',array('io_number'=>$ionumber,'ls_id'=>$lsid,'status_code'=>'5','note'=>'E-LS','flag_amend'=>'T'))->result_array();
				$ex=$this->db->query($sql)->getResultArray();
				if(count($ex)!=0){
					$sql="SELECT * FROM ls_file_monitoring WHERE io_number='".$ionumber."' AND ls_id='".$lsid."' AND status_code='6' AND flag_amend='T' ";
					//$row6els=$this->db->get_where('ls_file_monitoring',array('io_number'=>$ionumber,'ls_id'=>$lsid,'status_code'=>'6','flag_amend'=>'T'))->result_array();
					$row6els=$this->db->query($sql)->getResultArray();
					if(count($row6els)==0){
						//$seq_baru=$this->mtrack->get_data('get_seq_no',$ionumber,$lsid);
							
						$data=array('seq_no'=>$seq_baru["seq_baru"],
									'io_number'=>$ionumber,
									'ls_id'=>$lsid,
									'checked_by_user'=>'VPTI-Online',
									'checked_date'=>date('Y-m-d H:i:s'),
									'status_code'=>'6',
									'flag_ls_reprint'=>'F',
									'flag_NNLS'=>'F',
									'flag_amend'=>'T',
									'partial_number'=>$seq_baru["partial_number"],
									'note'=>$this->session->get('COMPANY_NAME'),								
						);
						return $this->model->simpan_monitoring('ls_file_monitoring',$data);	
						//return $this->db->insert('ls_file_monitoring',$data);
					}
				}
				//end AMEND
				
				//cek REPRINT untuk insert di Kolom REPRINT
				$sql="SELECT * FROM ls_file_monitoring WHERE io_number='".$ionumber."' AND ls_id='".$lsid."' AND status_code='5' AND note='E-LS' AND flag_ls_reprint='T' ";
				//$ex=$this->db->get_where('ls_file_monitoring',array('io_number'=>$ionumber,'ls_id'=>$lsid,'status_code'=>'5','note'=>'E-LS','flag_ls_reprint'=>'T'))->result_array();
				$ex=$this->db->query($sql)->getResultArray();
				if(count($ex)!=0){
					$sql="SELECT * FROM ls_file_monitoring WHERE io_number='".$ionumber."' AND ls_id='".$lsid."' AND status_code='6' AND flag_ls_reprint='T' ";
					//$row6els=$this->db->get_where('ls_file_monitoring',array('io_number'=>$ionumber,'ls_id'=>$lsid,'status_code'=>'6','flag_ls_reprint'=>'T'))->result_array();
					$row6els=$this->db->query($sql)->getResultArray();
					if(count($row6els)==0){
						//$seq_baru=$this->mtrack->get_data('get_seq_no',$ionumber,$lsid);
						//echo 'Tidak Ada 6';
						$data=array('seq_no'=>$seq_baru["seq_baru"],
									'io_number'=>$ionumber,
									'ls_id'=>$lsid,
									'checked_by_user'=>'VPTI-Online',
									'checked_date'=>date('Y-m-d H:i:s'),
									'status_code'=>'6',
									'flag_ls_reprint'=>'T',
									'flag_NNLS'=>'F',
									'flag_amend'=>'F',
									'partial_number'=>$seq_baru["partial_number"],
									'note'=>$this->session->get('COMPANY_NAME'),								
						);
						return $this->model->simpan_monitoring('ls_file_monitoring',$data);		
						//return $this->db->insert('ls_file_monitoring',$data);
					}
				}
				//end REPRINT
				
				else{
					return true;
				}
			}else{
				return true;
			}
			
		}elseif($status=='N'){
			//cek NNLS untuk insert di Kolom NNLS
			$sql="SELECT * FROM ls_file_monitoring WHERE io_number='".$ionumber."' AND ls_id='".$lsid."' AND status_code='6' AND flag_NNLS='T' ";
			//$ex=$this->db->get_where('ls_file_monitoring',array('io_number'=>$ionumber,'ls_id'=>$lsid,'status_code'=>'6','flag_NNLS'=>'T'))->result_array();
			$ex=$this->db->query($sql)->getResultArray();
			if(count($ex)!=0){
				$sql="SELECT * FROM ls_file_monitoring WHERE io_number='".$ionumber."' AND ls_id='".$lsid."' AND status_code='7' AND flag_NNLS='T' ";
				//$row7els=$this->db->get_where('ls_file_monitoring',array('io_number'=>$ionumber,'ls_id'=>$lsid,'status_code'=>'7','flag_NNLS'=>'T'))->result_array();
				$row7els=$this->db->query($sql)->getResultArray();
				if(count($row7els)==0){
					
						
					$data=array('seq_no'=>$seq_baru["seq_baru"],
								'io_number'=>$ionumber,
								'ls_id'=>$lsid,
								'checked_by_user'=>'VPTI-Online',
								'checked_date'=>date('Y-m-d H:i:s'),
								'status_code'=>'7',
								'flag_ls_reprint'=>'F',
								'flag_NNLS'=>'T',
								'flag_amend'=>'F',
								'partial_number'=>$seq_baru["partial_number"],
								'note'=>$this->session->get('COMPANY_NAME'),								
					);
					return $this->model->simpan_monitoring('ls_file_monitoring',$data);	
					//return $this->db->insert('ls_file_monitoring',$data);
				}
			}
			else{
				return true;
			}
		}else{
			return true;
		}
	}
		
	function create_qr($code,$name){
		$writer = new PngWriter();
		
        // Create QR code
        $qrCode = QrCode::create($code)
            ->setEncoding(new Encoding('UTF-8'))
            ->setErrorCorrectionLevel(new ErrorCorrectionLevelLow())
            ->setSize(300)
            ->setMargin(10)
            ->setRoundBlockSizeMode(new RoundBlockSizeModeMargin())
            ->setForegroundColor(new Color(0, 0, 0))
            ->setBackgroundColor(new Color(255, 255, 255));

        // Create generic logo
        $logo = Logo::create(FCPATH.'assets/img/BPJPHLogo.png')
            ->setResizeToWidth(50);

        $result = $writer->write($qrCode,$logo);
        $result->saveToFile(FCPATH.'__repo/qr/'.$name.'.png');
        //$dataUri = $result->getDataUri();
        //echo '<img src="'.$code.'" alt="'.$code.'">';
	}
	
	function upload_fd_dok(){
		if($this->session->get('COMPANY_NAME')){
			$data=array();
			$file_namena=array();
			//print_r($_FILES);exit; 
			$msg=0;
			$newFilename="";
			$com=substr($_POST['io_number'],0,4);
			$sql="SELECT vr_submit_no FROM vpti2_vr_submission_header WHERE io_number='".$_POST['io_number']."' ";
			$vr=$this->db->query($sql)->getRowArray();
			$vr=$vr["vr_submit_no"];
			//$vr=$this->db->get_where('vpti2_vr_submission_header',array('io_number'=>$_POST['io_number']))->row('vr_submit_no');
			//$upload_dir_ftp="/".$com."/".$vr."/".$_POST['io_number']."/partial_".$_POST['partial_no']."/".$_POST['ls_number']."/";
			$upload_dir_ftp="/DEV/".$com."/".$vr."/".$_POST['io_number']."/partial_".$_POST['partial_no']."/".$_POST['ls_number']."/";
			//echo "<pre>";print_r($_FILES);exit;
			
			foreach($_FILES as $v=>$x){
				//echo 'a';exit;
				$fileElementName=$v;
				if($x["name"] !=''){
					$fName = $_FILES[$fileElementName]['tmp_name'];
					$type = $_FILES[$fileElementName]["type"];
					$fSize = @filesize($_FILES[$fileElementName]['tmp_name']);
					@unlink($_FILES[$fileElementName]);		
					$filename = $_FILES[$fileElementName]['name'];
					
					
					$name = explode(".", $filename);
					$accepted_types = array('application/zip', 'application/x-zip-compressed', 'multipart/x-zip', 'application/x-compressed');
					foreach($accepted_types as $mime_type) {
						if($mime_type == $type) {
							$okay = true;
							break;
						} 
					}
					
					
					$upload_dir_io="__repo/final_document/".$_POST['io_number']."/";
					$upload_dir_part=$upload_dir_io."partial_".$_POST['partial_no']."/";
					$upload_dir_ls=$upload_dir_part.$_POST['ls_number']."/";
					
						
					$uploadfile = $upload_dir_ls . $filename;
					
					 if(!file_exists($upload_dir_io))mkdir($upload_dir_io, 0777, true);
					 if(!file_exists($upload_dir_part))mkdir($upload_dir_part, 0777, true);			
					 if(!file_exists($upload_dir_ls))mkdir($upload_dir_ls, 0777, true);				 
					 //exit;
					// move_uploaded_file($fName, $uploadfile);
					// echo 4;exit; 
					
					$name_filein=array();
					if(move_uploaded_file($fName, $uploadfile)) {
	
						
						if($this->session->get('COMPANY_ID')=='14983' && $this->session->get('commodity_code')=='36'){							
							if($_POST['cl_category_file_id']!=4){
								$zip = new \ZipArchive();
								$x = $zip->open($uploadfile);
								if ($x === true) {
									
									$zip->extractTo($upload_dir_ls); // change this to the correct site path
									for ($i = 0; $i < $zip->numFiles; $i++) {
										 $name_filein[]=$zip->getNameIndex($i);
									 }
									
									$zip->close();
							
									unlink($uploadfile);
								}
							}else{
								$name_filein[]=$x["name"];
							}
						}else{
							$zip = new \ZipArchive();
							$x = $zip->open($uploadfile);
							if ($x === true) {
								
								$zip->extractTo($upload_dir_ls); // change this to the correct site path
								for ($i = 0; $i < $zip->numFiles; $i++) {
									 $name_filein[]=$zip->getNameIndex($i);
								 }
								//$name_filein=$zip->getNameIndex(0);
								$zip->close();
						
								unlink($uploadfile);
							}
						}
						//echo "<pre>";print_r($name_filein);exit;
						if(count($name_filein)>0){
							$waktu_kirim = date('YmdHis');
							
							$nama_filena="";
							$sts_file="";
							$sql="SELECT * FROM vpti2_final_document WHERE io_number='".$_POST['io_number']."' AND partial_no='".$_POST['partial_no']."' AND ls_number='".$_POST['ls_number']."' AND cl_category_file_id=".$_POST['cl_category_file_id'];
							//$get_doc=$this->db->get_where('vpti2_final_document',array('io_number'=>$_POST['io_number'],'partial_no'=>$_POST['partial_no'],'ls_number'=>$_POST['ls_number'],'cl_category_file_id'=>$_POST['cl_category_file_id']))->row();
							$get_doc=$this->db->query($sql)->getRowArray();
								
								 if(isset($get_doc["id"])){
									$data['sts']='edit';
									$data['id']=$get_doc["id"];
									$file_na=$get_doc["file_doc_1"];
									$jml_file_na=explode('|',$file_na);
									//print_r($file_na);exit;
									//echo count($jml_file_na);exit;
									if(count($jml_file_na)>0){
										$idx=count($jml_file_na)-1;
										$sts_file="ada";
										$nama_filena=$file_na;
										//echo $idx;exit;
									}else{$idx=0;}
									
									
								 }
								 else{$data['sts']='add';$idx=0;}
								// echo "XXXX";exit;
							foreach($name_filein as $x){			
								 $a=pathinfo($x, PATHINFO_EXTENSION);
								 $sql="SELECT * FROM cl_category_file WHERE id=".$_POST['cl_category_file_id'];
								 
								 $cat_na=$this->db->query($sql)->getRowArray();
								 //$cat_na=$this->mtrack->get_data("cl_categorina",$_POST['cl_category_file_id']);
								 $namana=$this->buang_karakter_spesial($cat_na["id"]);
								 $waktu_kirim = date('YmdHis');
								 $newFilename = $_POST['ls_number']."_".$namana."_".$idx.".".$a;
								 $nama_filena .=$newFilename."|";
								
								 $file = $upload_dir_ls.$newFilename;
								 if(file_exists($upload_dir_ls.$newFilename)){
									//chmod($upload_dir_ls.$newFilename,0777);
									//unlink($upload_dir_ls.$newFilename);
								 }

								 
								//$get_doc=$this->mtrack->getFinalDoc($_POST['io_number'],$_POST['partial_no'],$_POST['ls_number']);
								
								 
								//echo $file;exit;
								rename($upload_dir_ls.$x, $upload_dir_ls.$newFilename);
								
								// GET UPLOAD TO FTP SERVER
								
								//$ftp_server=$this->config->item('FTP_host');
								$ftp_server='192.168.1.213';
								//$ftp_user_name=$this->config->item('FTP_user');
								$ftp_user_name='fd_kso2';
								//$ftp_user_name='fd';
								//$ftp_user_pass='123';
								//$ftp_user_pass=$this->config->item('FTP_pwd');
								$ftp_user_pass='fd_kso2';
								$conn_id = ftp_connect($ftp_server)or die("Couldn't connect to $ftp_server");
								
								//$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
								if (@ftp_login($conn_id, $ftp_user_name, $ftp_user_pass)) {
									ftp_pasv($conn_id, true);
									$mkdir=$this->get_direktorina_ftp($conn_id,$upload_dir_ftp);
									if($mkdir==1){
										if(ftp_put($conn_id, $newFilename, $file, FTP_BINARY)){
											$msg=1;
											 //echo "XXXX";exit;
											//echo 'ok';exit;
										}
										else{
											$msg=4;
											break;
											echo $msg;exit;
										}
									}else{
										$msg=3;
										break;
										echo $msg;exit;
									}
									
								}
								else{$msg=2;
									break;
									echo $msg;exit;
								}
								
								
								ftp_close($conn_id);
						
								//if($idx==8){echo $newFilename;break;exit; }
								  
								 chmod($upload_dir_ls.$newFilename,0777);
								 unlink($upload_dir_ls.$newFilename);
								
								 
								//$data["file_".$v]=$newFilename;
								
								$msg=1;
								
								
								$idx++;
								
							}
							
							$data["file_".$v]=$nama_filena;
							
							$this->rrmdir($upload_dir_io);
							// echo "XXXX";exit;
							$cc='';
							/*
							
							if(($this->auth['commodity_code']=='13' || $this->auth['commodity_code']=='47')){
								$to='fd_limbah@scisi.com';
							}else if(($this->auth['commodity_code']=='02') || ($this->auth['commodity_code']=='26') || ($this->auth['commodity_code']=='16')
								|| ($this->auth['commodity_code']=='17') || ($this->auth['commodity_code']=='34') || ($this->auth['commodity_code']=='43')
								|| ($this->auth['commodity_code']=='48')) {
								$to='fd_disc@scisi.com';
							}else if(($this->auth['commodity_code']=='10') || ($this->auth['commodity_code']=='11') || ($this->auth['commodity_code']=='22')
								|| ($this->auth['commodity_code']=='29') || ($this->auth['commodity_code']=='30')){
								$to='fd_ceramic@scisi.com';
							}else if($this->auth['commodity_code']=='24'){
								//$to='fd_kso@scisi.com';
								$to='fd_agri@scisi.com';
								$cc='audiya@scisi.com';
							}else if(($this->auth['commodity_code']=='05') || ($this->auth['commodity_code']=='09') || ($this->auth['commodity_code']=='15') 
								|| ($this->auth['commodity_code']=='31') || ($this->auth['commodity_code']=='37') || ($this->auth['commodity_code']=='38') 
								|| ($this->auth['commodity_code']=='42') || ($this->auth['commodity_code']=='49') || ($this->auth['commodity_code']=='52')){
								$to='fd_agri@scisi.com';
							}else if(($this->auth['commodity_code']=='12') || ($this->auth['commodity_code']=='14') || ($this->auth['commodity_code']=='25') 
								|| ($this->auth['commodity_code']=='33') || ($this->auth['commodity_code']=='44')){
								$to='fd_elektronik@scisi.com';
							}else if(($this->auth['commodity_code']=='36') || ($this->auth['commodity_code']=='40') || ($this->auth['commodity_code']=='45') 
								|| ($this->auth['commodity_code']=='51') || ($this->auth['commodity_code']=='35') || ($this->auth['commodity_code']=='19')){
								$to='fd_baja@scisi.com';
							}else if(($this->auth['commodity_code']=='03') || ($this->auth['commodity_code']=='07') || ($this->auth['commodity_code']=='08') 
								|| ($this->auth['commodity_code']=='19') || ($this->auth['commodity_code']=='20') || ($this->auth['commodity_code']=='21')
								|| ($this->auth['commodity_code']=='23') || ($this->auth['commodity_code']=='35') || ($this->auth['commodity_code']=='39')
								|| ($this->auth['commodity_code']=='41') || ($this->auth['commodity_code']=='46') || ($this->auth['commodity_code']=='50')){
								$to='fd_kimia@scisi.com';
							}else{
							 	$to='fd_kso@scisi.com';	
							}
							
							$cc='notif@scisi.com';
							
						
							 //$to='yogi_p4try4@yahoo.com';
							//$to='heri@scisi.com';
							//$vrSubId='XXXXX';
							$com_na=substr($_POST['io_number'],0,4);
							$subject  = "Upload/Re-Upload Final Document VO: ".$_POST['io_number'].", Partial: ".$_POST['partial_no'].", LS: ".$_POST['ls_number'];
							$body  = "<h2>Upload/Re-Upload Final Document (FD)</h2><hr>";
							$body .='Dear FD team,<br>For Your Information, Final Document (FD) for VO: '.$_POST['io_number'].', Partial:'.$_POST['partial_no'].', LS: '.$_POST['ls_number'].', has been uploaded/re-uploaded. Please proccess immediately.<br><br><br><br> Thank You, <br><br><br><br><br>'.$this->auth['COMPANY_NAME'].'';
							
							if($msg==1){
								$this->send_mail($_POST['io_number'],$to,$body,$subject,$cc);
							}else{
								echo 4;exit;
							}
							
							*/
							
							
							
						}
					}
					
					else{
						$msg=2;
						break;
						echo $msg;exit;
					}
				
			
			 }
			 
			 }
			 
		if($msg==1){
			$data["io_number"]=$_POST['io_number'];
			
			$data["partial_no"]=$_POST['partial_no'];
			$data["ls_number"]=$_POST['ls_number'];
			$data["remark"]=$_POST['remark'];
			$data["create_date"]=date('Y-m-d H:i:s');
			$data["update_date"]=date('Y-m-d H:i:s');
			$data["cl_category_file_id"]=$_POST['cl_category_file_id'];
			//echo 'Your Session His Expired Please Refresh Your Browser ';exit;
			echo $this->model->simpan_filedoc('vpti2_final_document',$data);
			
		}
	
		}
		else{
			//$msg=2;
			//break;
			echo 'Your Session His Expired Please Refresh Your Browser ';exit;
		}
	}
	
	function buang_karakter_spesial($s) {
		$old_pattern = array("/[^a-zA-Z0-9]/", "/_+/", "/_$/");
        $new_pattern = array("_", "_", "");
		$result = preg_replace($old_pattern,$new_pattern, $s);
		return $result;
		//echo $result;
	}	
	
	function rrmdir($dir) {
		foreach(glob($dir . '/*') as $file) {
			if(is_dir($file))
				$this->rrmdir($file);
			else
				unlink($file);
		}
		rmdir($dir);
	}
	
	function get_direktorina_ftp($con,$dir){
		$pecah=explode("/",$dir);
		foreach ($pecah as $v){
			if($v != ""){			
				if(@ftp_chdir($con,$v)){
					$msg=1;
				}else{					
					if(@ftp_mkdir($con, $v)){
						ftp_chdir($con,$v);
						$msg=1;
					}else{
						$msg=2;
					}
				}			
			}
		}		
		return $msg;
	}
	function download_fd(){
			$io=$this->request->getPost('io_number');
			$com=substr($io,0,4);
			//$vr=$this->db->get_where('vpti2_vr_submission_header',array('io_number'=>$io))->row('vr_submit_no');
			$sql="SELECT vr_submit_no FROM vpti2_vr_submission_header WHERE io_number='".$io."'";
			$vr=$this->db->query($sql)->getRowArray()["vr_submit_no"];
		//	echo $vr;exit;
			$partial=$this->request->getPost('partial_no');
			$ls=$this->request->getPost('ls_number');
			$file=$this->request->getPost('file_name');
		if($io){	
			ini_set('memory_limit', '4000M');
			
			//$remote_file = $com.'/'.$vr.'/'.$io.'/partial_'.$partial.'/'.$ls.'/'.$file;
			$remote_file="/DEV/".$com."/".$vr."/".$io."/partial_".$partial."/".$ls."/".$file;
			$local_file = '__repo/final_document/'.$file;
			//$handle = fopen($local_file, 'w+');
			$ftp_server='192.168.1.213';
			//$ftp_user_name=$this->config->item('FTP_user');
			$ftp_user_name='fd_kso2';
			//$ftp_user_name='fd';
			//$ftp_user_pass='123';
			//$ftp_user_pass=$this->config->item('FTP_pwd');
			$ftp_user_pass='fd_kso2';
			//$ftp_server=$this->config->item('FTP_host');
			//$ftp_user_name=$this->config->item('FTP_user');;
			//$ftp_user_pass=$this->config->item('FTP_pwd');;
			$conn_id = ftp_connect($ftp_server)or die("Couldn't connect to $ftp_server");
			
			//$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
			if (@ftp_login($conn_id, $ftp_user_name, $ftp_user_pass)) {
				ftp_pasv($conn_id, true);
				if (ftp_get($conn_id, $local_file, $remote_file, FTP_BINARY)) {
						//echo "successfully written to $local_file\n";
						header('Location: '.base_url().'/__repo/final_document/'.$file);
				} else {
					 echo "There was a problem while downloading $remote_file to $local_file\n";
				}
				ftp_close($conn_id);
				//fclose($handle);
				
				//echo "connect ";
			} else {
				echo "Couldn't connect as $ftp_user_name\n";
			}
			exit;
			
			// try to download $remote_file and save it to $handle
			if (ftp_get($conn_id, $local_file, $remote_file, FTP_BINARY)) {
				echo "Successfully written to $local_file\n";
			} else {
				echo "There was a problem\n";
			}

			// close the connection and the file handler
			ftp_close($conn_id);
			//fclose($handle);
		}
		else{
			echo '<center><div class="box-info">
				<h1 style="font-size:30px;font-weight:bold;">WARNING !!!</h1>
				<h1 style="font-size:20px;font-weight:bold;">Ooopssss... Sorry</h1>
				<br />
				<img height="150px" align="middle" src="'.base_url().'assets/images/no_access.png">
			</div></center>';
		}
	}
}

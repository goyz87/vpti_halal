<?php

namespace App\Controllers;
use App\Models\Mrevisi;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;
class Revisi extends BaseController
{
	public function initController(
        RequestInterface $request,
        ResponseInterface $response,
        LoggerInterface $logger
    ) {
        parent::initController($request, $response, $logger);
		$this->smarty->assign('base_url',base_url());
		$this->session = session();
		$this->smarty->assign('auth',$this->session->get());
        $this->smarty->assign('info',$this->session->get());
        $this->smarty->assign('base_url',base_url());
		$this->model=new Mrevisi();
    }
    public function index()
    {
		//echo $this->session->get('USER_ID');exit;
	   if((!$this->session->get('USER_ID'))){
			return redirect()->to(base_url().'/login');
		}
       $this->smarty->display('main.html');
    }
	public function getkonten($p1="",$p2="")
    {
		if((!$this->session->get('USER_ID'))){
			return redirect()->to(base_url().'/login');
		}
		switch($p1){
			case "revisi_ls":
				$konten="Revisi/revls.html";
			break;
			case "revisi_vo":
				$konten="Revisi/revvo.html";
			break;
			case "form_revisi_vo":
				$vo=$this->model->get_data('dt_vo');
				$this->smarty->assign('vo',$vo);
				if($this->request->getPost('aksi')=='edit'){
					$data=$this->model->get_data('list_revisi_vo','edit');
					$this->smarty->assign('dt',$data);
					$this->smarty->assign('sts','edit');
				}
				$konten="Revisi/form_revvo.html";
				
				return $this->smarty->display($konten);
			break;
			case "form_revisi_ls":
				$vo=$this->model->get_data('dt_vo');
				//$ls=$this->model->get_data('dt_ls');
				$this->smarty->assign('vo',$vo);
				//$this->smarty->assign('ls',$ls);
				if($this->request->getPost('aksi')=='edit'){
					$data=$this->model->get_data('list_revisi_ls','edit');
					$this->smarty->assign('dt',$data);
					$this->smarty->assign('sts','edit');
				}
				$konten="Revisi/form_revls.html";
				
				return $this->smarty->display($konten);
			break;
		}
		$this->smarty->assign('konten',$konten);
        $this->smarty->display('main.html');
    }

	public function getmodal()
    {
		if((!$this->session->get('USER_ID'))){
			return redirect()->to(base_url().'/login');
		}
		$mod=$this->request->getPost('mod');
		switch($mod){
			case "supplier":
				$konten="VR/data-supplier.html";
			break;
			case "form-supplier":
				$konten="VR/data-supplier.html";
			break;
			
		}
		$this->smarty->assign('konten',$konten);
        $this->smarty->display($konten);
    }
	public function getdata($p1="",$p2="")
    {
		if((!$this->session->get('USER_ID'))){
			return redirect()->to(base_url().'/login');
		}
		switch($p1){
			case "vo":
				$dt=$this->model->get_data('list_revisi_vo');
			break;
			case "ls":
				$dt=$this->model->get_data('list_revisi_ls');
			break;
			case "js_vo":
				$dt=$this->model->get_data('dt_vo');
			break;
			case "js_ls":
				$dt=$this->model->get_data('dt_ls');
			break;
			case "js_partial":
				$dt=$this->model->get_data('dt_partial');
			break;
		}
		echo $dt;
	}
	public function crud($p1)
    {
		$post = array();
		//echo $this->session->get('commodity_code');exit;
        foreach($_POST as $k=>$v){
			if($this->request->getPost($k)!=""){
				//$post[$k] = $this->db->escape_str($this->input->post($k));
				$post[$k] = $this->request->getPost($k);
			}
			
		}
		echo $this->model->simpan_data($p1,$post);
		
	
	}
	
}

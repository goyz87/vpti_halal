<?php

namespace App\Controllers;
use App\Models\Mdashboard;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;
class Dashboard extends BaseController
{
	 public function initController(
        RequestInterface $request,
        ResponseInterface $response,
        LoggerInterface $logger
    ) {
        parent::initController($request, $response, $logger);
		$this->smarty->assign('base_url',base_url());
		$this->session = session();
		$this->smarty->assign('auth',$this->session->get());
        $this->smarty->assign('base_url',base_url());
		$this->model=new Mdashboard();
    }
    public function index()
    {
		if((!$this->session->get('USER_ID'))){
			return redirect()->to(base_url().'/login');
		}
		//$dt=$this->getdata('line_chart');
		$this->smarty->display('main.html');
	   
    }
	
	public function getkonten($p1="",$p2="")
    {
		//echo ;exit;
		
		$this->smarty->assign('base_url',base_url());
        $this->smarty->display('main.html');
		
    }
	public function getdata($p1="",$p2="")
    {
		if((!$this->session->get('USER_ID'))){
			return redirect()->to(base_url().'/login');
		}
		switch($p1){
			case "line_chart":
				
				echo $dt=$this->model->get_data('linechart');
				exit;
				
			break;
			case "stack_dash":
				
				echo $dt=$this->model->get_data('stack_dash');
				exit;
				
			break;
			case "donat_dash":
				
				echo $dt=$this->model->get_data('donat_dash');
				exit;
				
			break;
		}
	}
}

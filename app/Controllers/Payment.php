<?php

namespace App\Controllers;
use App\Models\Mpayment;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;
class Payment extends BaseController
{
	public function initController(
        RequestInterface $request,
        ResponseInterface $response,
        LoggerInterface $logger
    ) {
        parent::initController($request, $response, $logger);
		$this->smarty->assign('base_url',base_url());
		$this->session = session();
		$this->smarty->assign('auth',$this->session->get());
        $this->model=new Mpayment();
    }
    public function index()
    {
		//echo $this->session->get('USER_ID');exit;
	   if((!$this->session->get('USER_ID'))){
			return redirect()->to(base_url().'/login');
		}
       $this->smarty->display('main.html');
    }
	
	public function getkonten($p1="",$p2="")
    {
		if((!$this->session->get('USER_ID'))){
			return redirect()->to(base_url().'/login');
		}
		//echo $this->request->getPost('aksi');exit;
		switch($p1){
			
			case "bill":
				$inv_no=$this->request->getPost('inv_no');
				$dt=$this->model->get_data('get_bill',$inv_no);
				//$x=array('x','Y');
				//$a=join("','",$x);
				//echo $a;exit;
				//echo "<pre>";print_r($dt);exit;
				$this->smarty->assign('dt',$dt);
				$konten="Payment/bill.html";
				echo $this->smarty->display($konten);
				exit;
			break;
			case "list-payment":
				$konten="Payment/list-payment.html";
			break;
			case "histori":
				$konten="Payment/histori.html";
			break;
			case "isi_list_pay":
				$dt=$this->model->get_data('get_list_pay');
				$this->smarty->assign('dt',$dt);
				$konten="Payment/isi_list.html";
				echo $this->smarty->display($konten);
				exit;
			break;
			case "view_status":
				$info=$this->model->get_data('info_status');
				$this->smarty->assign('info',$info);
				$konten="Track/view_status.html";
				echo $this->smarty->display($konten);
				exit;
			break;
			
		}
		$this->smarty->assign('konten',$konten);
        $this->smarty->display('main.html');
    }
	public function getdata($p1="",$p2="")
    {
		if((!$this->session->get('USER_ID'))){
			return redirect()->to(base_url().'/login');
		}
		$model=new Mpayment();
		switch($p1){
			case "get_list_pay":
				echo $dt=$model->get_data('get_list_pay');
				exit;
				
			break;
			case "his_bill":
				echo $dt=$model->get_data('his_bill');
				exit;
				
			break;
		}
		echo json_encode($dt);
    }
	
	public function getmodal()
    {
		if((!$this->session->get('USER_ID'))){
			return redirect()->to(base_url().'/login');
		}
		$mod=$this->request->getPost('mod');
		switch($mod){
			case "supplier":
				$konten="VR/data-supplier.html";
			break;
			
		}
		$this->smarty->assign('konten',$konten);
        $this->smarty->display($konten);
    }
	
	public function crud($p1)
    {
		$post = array();
		//echo $this->session->get('commodity_code');exit;
        foreach($_POST as $k=>$v){
			if($this->request->getPost($k)!=""){
				//$post[$k] = $this->db->escape_str($this->input->post($k));
				$post[$k] = $this->request->getPost($k);
			}
			
		}
		/*if( $this->model->simpan_data($p1,$post)){
			if($_POST['sts']=='delete'){
				echo 1;
			}else{
				return redirect()->to(base_url().'/list-'.$p1);
			}
		}else{
			 return redirect()->to(base_url().'/list-'.$p1);
		}*/
		echo $this->model->simpan_data($p1,$post);
		
	
	}
	
	function genCaptcha($rand){
		//echo "<img src='".base_url()."/assets/img/back_captcha.jpg'>";exit;
		
		header("Content-type: image/jpeg");// out out the image
		
		$RandomStr = md5(microtime());// md5 to generate the random string
		$ResultStr = substr($RandomStr,0,5);//trim 5 digit
		//echo $ResultStr;exit;
		$NewImage = imagecreatefromjpeg("assets/img/back_captcha.jpg");//image create by existing image and as back ground
		$font=realpath('assets/font/ROCCB.ttf');

		$TextColor = imagecolorallocate($NewImage, 1, 34, 128);//text color-white

		imagettftext($NewImage, 22, 4, 25, 25, $TextColor, $font, $ResultStr);
		
		$this->session->set("captcha", $ResultStr);
		imagejpeg($NewImage);//Output image to browser/**/
		//ImageDestroy($image); //Free up resources
		exit();
	}
	function checkCaptcha($txt){
		$captcha = $this->session->get('captcha');
		if($txt==$captcha){
			echo "sama";
		} else {
			echo "tidak sama";
		}
	}
	function set_payment($p1)
	{
		$post = array();
		foreach ($_POST as $k => $v) $post[$k] = $this->request->getPost($k);
		echo $this->model->set_payment($p1, $post);
	}
	
	function cetak_bill($p1 = "", $p2 = "")
	{
		$mpdfConfig = array(
				'mode' => 'utf-8', 
				'format' => 'A4',    // format - A4, for example, default ''
				'default_font_size' => 0,     // font size - default 0
				'default_font' => '',    // default font family
				'margin_left' => 20,
				'margin_top' => 35,
				'margin_right' => 20,
				'margin_bottom' => 20,
				'orientation' => 'P'  	// L - landscape, P - portrait
			);
		if (!$this->request->getPost('mva')) {
			$this->smarty->assign('msg', "Sorry No ACCESS URL");
			$this->smarty->assign('_module', 'invalid_tiket.tpl');
			return $this->smarty->display("index.tpl");
		}
		$p1 = $this->request->getPost('mod');
		//echo $p1;exit;
		set_time_limit(0);
		//$this->load->library('mlpdf');
		//$this->load->helper('terbilang');
		//$pdf = $this->mlpdf->load();
		//echo ucwords($this->number_to_words("8348887.5"));exit;
		//if($p1=='inv'){
		$data = $this->model->get_data('get_bill_his', $p1, $this->request->getPost('mva'));
		//}else{

		//}
		//echo "<pre>";print_r($data);exit;
		$total = $data['data_header']['Amount'];
		$terbilang = ucwords($this->number_to_words($total));
		//$terbilang ='x';
		//$mva=base64_decode($p2);
		$mva = $this->request->getPost('mva');
		//echo "xx";exit;
		//$mva = $this->encrypt->decode($this->input->post('mva'));
		$this->smarty->assign('data', $data);
		$this->smarty->assign('mod', $p1);
		$this->smarty->assign('terbilang', $terbilang);

		$htmlcontent = $this->smarty->fetch("Payment/bill_pdf.tpl");
		//$htmlcontent = '<p>XXX</p>';
		$htmlheader = $this->smarty->fetch("Payment/header_bill.tpl");

		//echo $htmlcontent;exit;
		$mpdf = new \Mpdf\Mpdf($mpdfConfig);
		$mpdf->ignore_invalid_utf8 = true;
		// bukan sulap bukan sihir sim salabim jadi apa prok prok prok
		$mpdf->allow_charset_conversion = true;     // which is already true by default
		$mpdf->charset_in = 'iso-8859-1';  // set content encoding to iso
		$mpdf->SetDisplayMode('fullpage');		
		$mpdf->SetHTMLHeader($htmlheader);
		//$spdf->keep_table_proportions = true;
		$mpdf->useSubstitutions=false;
		$mpdf->simpleTables=true;
		
		$mpdf->SetHTMLFooter('
			<div style="font-family:arial; font-size:8px; text-align:center; font-weight:bold;">
				<table width="100%" style="font-family:arial; font-size:8px;">
					<tr>
						<td width="30%" align="left">
						</td>
						<td width="40%" align="center">
						</td>
						<!--td width="30%" align="right">
							Hal. {PAGENO} dari {nbpg}
						</td-->
						<td width="30%" align="right">
							Halaman {PAGENO} / Page {PAGENO}
						</td>
					</tr>
				</table>
			</div>
		');		
		$filename = $mva;
		$filename = str_replace('/', '', $filename);
		//echo $filename;exit;
		//$spdf->SetProtection(array('print'));
		//$spdf->WriteHTML($htmlcontent); // write the HTML into the PDF
		//$spdf->Output('repositories/Dokumen_LS/LS_PDF/'.$filename.'.pdf', 'F'); // save to file because we can
		$mpdf->WriteHTML($htmlcontent);
		$this->response->setHeader('Content-Type', 'application/pdf');
		$mpdf->Output($filename.'.pdf','I'); // opens in browser
		
		
	//	$spdf->Output('repositories/Billing/' . $filename . '.pdf', 'F');
	//	$spdf->Output($filename . '.pdf', 'I'); // view file	
		//if ($p1 == 'inv') {
			//$this->send_mail($mva);
	//	} else {
			//$this->send_mail_konfirmasipayment($mva);
		//}
	}
	
	function number_to_words($number)
	{
		$before_comma = trim($this->to_word($number));
		$after_comma = trim($this->comma($number));
		$angka_koma = stristr($number,'.');
		$get_angka_koma = substr($angka_koma,1,2);
		//echo (int)$get;exit;
		if((int)$get_angka_koma > 0){
			return ucwords($results = $before_comma.' koma '.$after_comma);
		}else{
			return ucwords($results = $before_comma);
		}
		
	}

	function to_word($number)
	{
		$words = "";
		$arr_number = array(
		"",
		"satu",
		"dua",
		"tiga",
		"empat",
		"lima",
		"enam",
		"tujuh",
		"delapan",
		"sembilan",
		"sepuluh",
		"sebelas");

		if($number<12)
		{
			$words = " ".$arr_number[$number];
		}
		else if($number<20)
		{
			$words = $this->to_word($number-10)." belas";
		}
		else if($number<100)
		{
			$words = $this->to_word($number/10)." puluh ".$this->to_word($number%10);
		}
		else if($number<200)
		{
			$words = "seratus ".$this->to_word($number-100);
		}
		else if($number<1000)
		{
			$words = $this->to_word($number/100)." ratus ".$this->to_word($number%100);
		}
		else if($number<2000)
		{
			$words = "seribu ".$this->to_word($number-1000);
		}
		else if($number<1000000)
		{
			$words =$this-> to_word($number/1000)." ribu ".$this->to_word($number%1000);
		}
		else if($number<1000000000)
		{
			$words = $this->to_word($number/1000000)." juta ".$this->to_word($number%1000000);
		}
		else
		{
			$words = "undefined";
		}
		return $words;
	}

	function comma($number)
	{
		$after_comma = stristr($number,'.');
		$arr_number = array(
		"nol",
		"satu",
		"dua",
		"tiga",
		"empat",
		"lima",
		"enam",
		"tujuh",
		"delapan",
		"sembilan");

		$results = "";
		$length = strlen($after_comma);
		$i = 1;
		while($i<$length)
		{
			$get = substr($after_comma,$i,1);
			$results .= " ".$arr_number[$get];
			$i++;
		}
		return $results;
	}
	
}

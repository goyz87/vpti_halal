<?php

namespace App\Controllers;
use App\Models\Mlogin;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;
class Login extends BaseController
{
    public function initController(
        RequestInterface $request,
        ResponseInterface $response,
        LoggerInterface $logger
    ) {
        parent::initController($request, $response, $logger);

        $this->smarty->assign('base_url',base_url());
		$this->session = \Config\Services::session();
        $this->session->start();
    }
    
    public function index()
    {
        
        $this->smarty->display('login.html');
    }
    public function getdata($p1="")
    {
        echo $p1;
    }
	public function cek_login($p1=""){
		//echo "<pre>";print_r($_POST);exit;
		//echo $user;exit;
		$encrypter = \Config\Services::encrypter();
		$user=$this->request->getPost('username');
		$pwd=$this->request->getPost('pwd');
		
		
		if(!$user or !$pwd){
			$this->smarty->assign("pesan","Please assign username or password!");
			$this->smarty->assign("user","");
			$this->smarty->assign("pass","");
			$this->smarty->display("login.html");
			return;
		};
		
		
		
		$model=new Mlogin();
		$dt=$model->get_data('login',$user);
        if(isset($dt["USER_ID"])){
			$verif=password_verify($pwd, $dt["USER_PASS"]);
			/*if($verif){
				$session = session();
                $session->set($dt);
              
				return redirect()->to(base_url().'/vreq');

				
			}else{
				$this->smarty->assign("pesan","Password Invalid.");
				$this->smarty->assign("user","");
				$this->smarty->assign("pass","");
				$this->smarty->display("login.html");
			}
			*/
			$session = session();
            $session->set($dt);
			if($dt["role"]==1)
				return redirect()->to(base_url().'/main-pusat');
			else
				return redirect()->to(base_url().'/main');
				

			
		}else{
			$this->smarty->assign("pesan","User Not Found.");
			$this->smarty->assign("user","");
			$this->smarty->assign("pass","");
			$this->smarty->display("login.html");
		}
       
	}
	public function logout()
    {
        $session = session();
        $session->destroy();
        return redirect()->to(base_url().'/login');
    }
}

<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (is_file(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Login');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(false);
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
// $routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');
$routes->get('login', 'Login::index');
$routes->get('tes', 'Tes::index');
$routes->get('dat', 'Tes::dat');
$routes->get('log_dt', 'Login::dat');

$login = [
    'login'      => 'Login::index',
    'logout'      => 'Login::logout',
    'masuk'      => 'Login::cek_login',
    'datalogin/(:any)' => 'Login::getdata/$1',
    'konten/(:any)/(:any)' => 'Main_app::getkonten/$1/$2',
];
$main = [
    //'main'      => 'Main_app::index',
    'konten/(:any)' => 'Main_app::getkonten/$1',
];
$dash = [
    'main'      => 'Dashboard::index',
    'konten/(:any)' => 'Dashboard::getkonten/$1',
    'line_dash' => 'Dashboard::getdata/line_chart',
    'stack_dash' => 'Dashboard::getdata/stack_dash',
    'donat_dash' => 'Dashboard::getdata/donat_dash',
];
$vr = [
  //  'main'      => 'Vreq::index',
    'dt/(:any)'      => 'Vreq::getdata/$1',
    'konten_vr/(:any)' => 'Vreq::getkonten/$1',
    'vreq' => 'Vreq::getkonten/vreq',
    'plb' => 'Vreq::getkonten/plb',
    'kbgb' => 'Vreq::getkonten/kbgb',
    'list-vreq' => 'Vreq::getkonten/list',
    'list_modal' => 'Vreq::getmodal',
    'submit_vr' => 'Vreq::submit_vr',
    'tespdf' => 'Vreq::tes_pdf',
    'viewpdf/(:any)' => 'Vreq::viewpdf/$1',
    'upload_dok' => 'Vreq::upload_dok',
    'dwn_dok' => 'Vreq::download_doc',
    'edit_detil' => 'Vreq::edit_detil',
];
$ref = [
    'ref'      => 'Ref::index',
    'konten_ref/(:any)' => 'Ref::getkonten/$1',
    'dt-ref/(:any)' => 'Ref::getdata/$1',
    'list-supplier' => 'Ref::getkonten/supplier',
    'list-exporter' => 'Ref::getkonten/exporter',
    'list-hs' => 'Ref::getkonten/hs',
    'ip' => 'Ref::getkonten/ip',
    'simpan_ref/(:any)' => 'Ref::crud/$1',
];
$track = [
    'monitor-status'      	=> 'Track::getkonten/monitor_status',
    'status/(:any)'      	=> 'Track::getkonten/monitor_status/$1',
	'konten_track/(:any)' 	=> 'Track::getkonten/$1',
    'dt-track/(:any)'      	=> 'Track::getdata/$1',
    'dwn_vo'      			=> 'Track::download_vo',
	'dwn_lvhi'      		=> 'Track::download_ls',
	'tes_qr'      			=> 'Track::tes_qr',
	'upload_fd'      			=> 'Track::getkonten/form_fd',
	'upload_fd_dok'      			=> 'Track::upload_fd_dok',
	'list_upload'      			=> 'Track::getkonten/list_upload',
	'download_fd'      			=> 'Track::download_fd',
];
$umum = [
    'profile'      => 'Umum::getkonten/profile',
	
];
$revisi = [
    'revisi-vo'      => 'Revisi::getkonten/revisi_vo',
    'revisi-ls'      => 'Revisi::getkonten/revisi_ls',
    'dt-revisi/(:any)'      => 'Revisi::getdata/$1',
	'konten_revisi/(:any)' => 'Revisi::getkonten/$1',
	'simpan_rev/(:any)' => 'Revisi::crud/$1',
];
$payment = [
    'CheckCaptcha/(:any)'      => 'Payment::CheckCaptcha/$1',
    'genCaptcha/(:any)'      => 'Payment::genCaptcha/$1',
    'payment'      => 'Payment::getkonten/list-payment',
    'dt-pay/(:any)'      => 'Payment::getdata/$1',
	'konten_pay/(:any)' => 'Payment::getkonten/$1',
	'billing' => 'Payment::getkonten/bill',
	'isi_list_pay/' => 'Payment::getkonten/isi_list_pay',
	'simpan_pay/(:any)' => 'Payment::crud/$1',
	'set_pay/(:any)' => 'Payment::set_payment/$1',
	'histori' => 'Payment::getkonten/histori',
	'cetak_bill' => 'Payment::cetak_bill',
];
$pusat = [
    'main-pusat'      => 'Pusat::index',
    'total_ls'      => 'Pusat::get_chart/total_ls',
    'total_usd_ls'      => 'Pusat::get_chart/total_usd_ls',
    'total_country'      => 'Pusat::get_chart/total_country',
    'total_country_usd'      => 'Pusat::get_chart/total_country_usd',
    'total_port'      => 'Pusat::get_chart/total_port',
    'total_hs'      => 'Pusat::get_chart/total_hs',
    'total_hs_usd'      => 'Pusat::get_chart/total_hs_usd',
    
];
$public = [
    'info-lvhi/(:any)'      => 'Home::infols/$1',
	
];
$routes->map($public);
$routes->map($pusat);
$routes->map($payment);
$routes->map($revisi);
$routes->map($umum);
$routes->map($track);
$routes->map($vr);
$routes->map($login);
$routes->map($dash);
$routes->map($main);
$routes->map($ref);
//$routes->post('simpan_ref', 'Ref::crud/$1');
$routes->post('/simpan_ref/(:any)', 'Ref::crud/$1');

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}

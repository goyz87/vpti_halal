{literal}
<style type="text/css">
table.gridtable {
	font-family: verdana,arial,sans-serif;
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #666666;
	border-collapse: collapse;
}
table.gridtable th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #dedede;
}
table.gridtable td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
}
</style>
{/literal}
{if $mod eq 'inv'}
<div style="float:left;width:45%;margin-top:150px">
<table width="100%" style="font-family:helvetica,arial,verdana,sans; font-size:11px;">
	<tr>
		{if $data.data_header.Method eq '-'}
			<td>MVA/Kode Bayar</td>
		{else}
			<td>No. Invoice (DOKU)</td>
		{/if}
		<td>:</td>
		<td>{$data.data_header.MvaNoId|default:'-'}</td>
	</tr>
	<tr>
		<td>Tanggal</td>
		<td>:</td>
		<td>{$data.data_header.tgl|default:'-'}</td>
	</tr>
</table>
</div>
<div style="float:left;width:50%;font-family:helvetica,arial,verdana,sans; font-size:11px;margin-top:150px ">
	<table width="100%" style="font-family:helvetica,arial,verdana,sans; font-size:11px;">
	<tr>
		<td valign="top">Importer</td>
		<td valign="top">:</td>
		<td>{$auth.COMPANY_NAME|default:'-'}</td>
	</tr>
	<tr>
		<td valign="top">Alamat</td>
		<td valign="top">:</td>
		<td>{$auth.address1|default:'-'}</td>
	</tr>
	<tr>
		<td>Phone/Fax</td>
		<td>:</td>
		<td>{$auth.phone|default:'-'} / {$auth.fax|default:'-'}</td>
	</tr>
	<tr>
		<td>Attn</td>
		<td>:</td>
		<td>{$auth.contact_name|default:'-'}</td>
	</tr>
</table>
</div>
<br>
<br>
<div style="float:left;width:100%;margin-top:20px;">
	<table class="gridtable">
		<tr style="border:1px solid black;">
		  <th style="border-bottom:1px solid black;" width="4%">No.</th>
		  <th style="border-bottom:1px solid black;" width="20%">Invoice</th>
		  <th style="border-bottom:1px solid black;" width="15%">VO</th>
		  <th style="border-bottom:1px solid black;" width="15%">LVHI No.</th>
		  <th style="border-bottom:1px solid black;" width="21%">Keterangan</th>
		  <th style="border-bottom:1px solid black;" width="25%">Jumlah</th>
		</tr>
		{assign var=no_na value=0} 
		{foreach from=$data.data_detil item=i}
		<tr>
			<td align="center">{assign var=no_na value=$no_na+1}{$no_na}.</td>
			<td align="left">{$i.Inv_No|default:'-'}</td>
			<td align="center">{$i.Vo_No|default:'-'}</td>
			<td align="center">{$i.Ls_No|default:'-'}</td>
			<td align="left">{$i.Descs|default:'-'}</td>
			<td align="right">{if $data.data_header.Curr eq 'USD'}$&nbsp;{else}Rp. &nbsp;{/if}{$i.Amount|number_format:2:'.':','|default:'-'}</td>
		</tr>
		{/foreach}
		{if $data.data_header.Method neq '-'}
		<tr>
			<td  align="right" colspan="5">Total:</td>
			<td align="right">{if $data.data_header.Curr eq 'USD'}$&nbsp;{else}Rp. &nbsp;{/if}{$data.data_header.TotalAsli|number_format:2:'.':','|default:'-'}</td>
		</tr>
		<tr>
			<td  align="right" colspan="5">Biaya Layanan:</td>
			<td align="right">{if $data.data_header.Curr eq 'USD'}$&nbsp;{else}Rp. &nbsp;{/if}{$data.data_header.Fee|number_format:2:'.':','|default:'-'}</td>
		</tr>
		<tr>
			<td  align="right" colspan="5">Grand Total:</td>
			<td align="right">{if $data.data_header.Curr eq 'USD'}$&nbsp;{else}Rp. &nbsp;{/if}{$data.data_header.Amount|number_format:2:'.':','|default:'-'}</td>
		</tr>
		{else}
		<tr>
			<td  align="right" colspan="5">Total</td>
			<td align="right">{if $data.data_header.Curr eq 'USD'}$&nbsp;{else}Rp. &nbsp;{/if}{$data.data_header.Amount|number_format:2:'.':','|default:'-'}</td>
		</tr>
		{/if}
	</table>

</div>

{else}


<div style="float:left;width:45%;">
<table width="100%" style="font-family:helvetica,arial,verdana,sans; font-size:12px;">
	<tr>
		<td width="42%">Dokumen Pembayaran</td>
		<td>:</td>
		<td>{$data.data_header.Doc_No|default:'-'}</td>
	</tr>
	<tr>
		<td>Tanggal</td>
		<td>:</td>
		<td>{$data.data_header.tgl|default:'-'}</td>
	</tr>
</table>
</div>
<div style="float:left;width:50%;font-family:helvetica,arial,verdana,sans; font-size:11px; ">
	<table width="100%" style="font-family:helvetica,arial,verdana,sans; font-size:11px;">
	<tr>
		<td>Importer</td>
		<td>:</td>
		<td>{$auth.COMPANY_NAME|default:'-'}</td>
	</tr>
	<tr>
		<td valign="top">Alamat</td>
		<td valign="top">:</td>
		<td>{$auth.address1|default:'-'}</td>
	</tr>
	<tr>
		<td>Phone/Fax</td>
		<td>:</td>
		<td>{$auth.phone|default:'-'} / {$auth.fax|default:'-'}</td>
	</tr>
	<tr>
		<td>Attn</td>
		<td>:</td>
		<td>{$auth.contact_name|default:'-'}</td>
	</tr>
</table>
</div>
<br>
<br>
<div style="float:left;width:100%;margin-top:20px;">
	<table class="gridtable">
		<tr style="border:1px solid black;">
		  <th style="border-bottom:1px solid black;" width="4%">No.</th>
		  <th style="border-bottom:1px solid black;" width="20%">Invoice</th>
		  <th style="border-bottom:1px solid black;" width="15%">VO</th>
		  <th style="border-bottom:1px solid black;" width="15%">LS</th>
		  <th style="border-bottom:1px solid black;" width="21%">Keterangan</th>
		  <th style="border-bottom:1px solid black;" width="25%">Jumlah</th>
		</tr>
		{assign var=no_na value=0} 
		{foreach from=$data.data_detil item=i}
		<tr>
			<td  align="center">{assign var=no_na value=$no_na+1}{$no_na}.</td>
			<td align="left">{$i.Inv_No|default:'-'}</td>
			<td align="center">{$i.Vo_No|default:'-'}</td>
			<td align="center">{$i.Ls_No|default:'-'}</td>
			<td align="left">{$i.Descs|default:'-'}</td>
			<td align="right">{if $data.data_header.Curr eq 'USD'}$&nbsp;{else}Rp. &nbsp;{/if}{$i.Amount|number_format:2:'.':','|default:'-'}</td>
		</tr>
		{/foreach}
		<tr>
			<td  align="right" colspan="5">Total:</td>
			<td align="right">{if $data.data_header.Curr eq 'USD'}$&nbsp;{else}Rp. &nbsp;{/if}{$data.data_header.Amount|number_format:2:'.':','|default:'-'}</td>
		</tr>
	</table>

</div>
{/if}
<div style="float:left;width:100%;margin-top:20px;">
<p style="font-size:10px;font-weight:bold;"><i>Terbilang : {$terbilang|default:'-'}&nbsp;{if $data.data_header.Curr eq 'USD'}Dollar{else}Rupiah{/if}</i></p>

{if $mod eq 'inv'}
<ol style="font-size:10px;">
	<li>
		Billing Payment Ini dicetak langsung melalui sistem terkomputerisasi.<br>
		Tidak dibubuhi tanda tangan asli dan stempel perusahaan tetapi memiliki kekuatan hukum sama dan diakui sah.
	</li>
	{if $data.data_header.Method eq '-'}
		<li>
			Billing Payment ini adalah invoice gabungan dari tagihan yang sudah ditagihkan, untuk mempermudah dalam prosess pembayaran.<br>
			Pembayaran melalui Bank Mandiri (MVA host to host)<br>
			Pembayaran Invoice (kode invoice 3)<br>
			A/C No. {$data.data_header.MvaNoId|default:'-'}
		</li>
	{else}
		<li>
			Billing Payment ini adalah invoice gabungan dari tagihan yang sudah ditagihkan, untuk mempermudah dalam prosess pembayaran.<br>
			<b>Pembayaran melalui {$data.data_header.Method|replace:'_':' '}</b> <br>
			Silahkan akses URL berikut:<br>{$data.data_header.Link}
		</li>
	{/if}
	<li>
		Billing Payment ini bukan bentuk tagihan atau Invoice yang kami terbitkan dan kami kirimkan.
	</li>
	<li>
		Bukti potong PPH 23 harap dikirim ke alamat:<br>
		L'avenue Office Tower Lt.8, Jl. Raya Pasar Minggu Kav 16, Pancoran, Jakarta Selatan 12870, Indonesia<br>
		dengan mencantumkan nomor invoice ini.
	</li>
</ol>
{/if}
</div>

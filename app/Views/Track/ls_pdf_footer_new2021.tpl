<div>
	<table width="100%">
		<tr>
			<td style='text-align:center;font-size:10px;' colspan="2">
				<b>{if $commodity eq '03'}
					Keputusan Menteri Perindustrian Dan Perdagangan Republik Indonesia {$perment}
				{else}
					Peraturan Menteri Perdagangan Republik Indonesia {$perment}
				{/if}</b>
			</td>
		</tr>
		<tr>
			<td style='text-align:center;font-size:9px;' colspan="2">
				<i>{if $commodity eq '03'}
					Decision of the Minister of the Industry and Trade of The Republic of Indonesia {$perment}
				{else}
					Regulation of the Minister of Trade of The Republic of Indonesia {$perment}
				{/if}</i>
			</td>
		</tr>
		<tr>
			<td style='text-align:center;font-size:8px;' colspan="2"> Laporan ini merupakan hasil cetak dokumen elektronik. Laporan ini diterbitkan dalam rangka memenuhi persyaratan yang ditetapkan oleh Pemerintah Republik Indonesia dan tidak membebaskan pihak-pihak terkait dari tanggung jawabnya terhadap ketentuan perundang-undangan Republik Indonesia</td>
		</tr>
		<tr>
			<td style='text-align:center;font-size:8px;' colspan="2"><i>(The report is printed electronic document. This report is published in order to comply with the requirements implemented by the Government of the Republic of Indonesia and not to release the related parties from their responsibility for the provisions of the laws of the Republic of Indonesia)</i></td>
		</tr>
		<tr>
			<td style="text-align:center;font-size:10px;"><!--b>KSO CODE: {$datalsheader.0.sc_code}</b--></td>
			<td style="width:10%; text-align:right;font-size:8px;">Page. {literal}{PAGENO}{/literal}</td>
		</tr>
	</table>
</div> 

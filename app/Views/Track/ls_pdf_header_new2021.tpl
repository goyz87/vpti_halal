	<div id='header'>
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td style="width:30%">
					<img src='assets/img/LogoBPJPH.gif' width='270px'>
				</td>				
				<td valign="top" style="color: #00000; text-align: right;" valign="top" width="70%">
					<div style="font-family:arial; font-weight:bold;">
					<h5>
						<br/>
						LAPORAN VERIFIKASI HALAL IMPOR
						<div style="font-family:bell; font-size:10px;"><i>(IMPORT HALAL VERIFICATION REPORT)</i></div>
						
					</h5>
					</div>
					<br>
					<div style="font-family:arial; font-size:11px; text-align:right; font-weight:bold;">		
						Impor {$judulcomodity.description}<br />
						<div style="font-family:bell; font-size:10px;"><i>(Import of {$judulcomodity.description2})</i><br>
						LVHI No. {$lsnumber|default:'-'}&nbsp;&nbsp;&nbsp;&nbsp; VO No. {$ionumber} P{$datalsheader.0.partial_number|default:'-'}
						</div>
					</div>
				</td>
				<td align="right">&nbsp;
				</td>
			</tr>
			<tr>
				<td colspan='3'><hr /></td>
			</tr>
		</table>
	</div> 

<div id='header'>
    <table width="100%" >
        <tr>
            <td style="width:30%">
				<img src='assets/img/LogoBPJPH.gif' width='270px'>
			</td>
            <td style="color: #00000; text-align: right;" valign="top" width="70%">
                <div style="font-family:arial; font-weight:bold;">		
                    <h4>ORDER VERIFIKASI</h4>
					<h5><i>VERIFICATION ORDER</i></h5>
					
					<div style="font-size:9.5px">
						VO No. {$data_io_number.io_number|default:'-'}<br>
						{$data_commodity_desc.deskripsi|default:'PRODUK TERTENTU'}<br>
						<i>{$data_commodity_desc.deskripsi2|default:'PRODUK TERTENTU'}</i><br>
						VR No. {$data_vr_no.vr_submit_no|default:'-'}
					</div>

                </div>
            </td>
            <td align="right">&nbsp;
                <!--img src='assets/images/kso_scisi.png' width='170px' /-->
            </td>
        </tr>
		<tr>
			<td colspan='3'><hr /></td>
		</tr>
    </table>
</div> 

<!DOCTYPE html>
<html>
<head>
	<title>Document Verification Order</title>
	<style>
		table { width:100%; }
	</style>
</head>
<body>
<div id='content'>
<table width="100%" style="font-family:arial; font-size:10px;">	
	<tr>
		<td colspan="3" style="color: navy;"><b>Importir / 
			<font style="font-size:8px;"><i>Importer</i></font></b></td>
		<td>&nbsp;</td>
		<td colspan="3"  style="color: navy;"><b>Eksportir / <font style="font-size:8px;"><i>Exporter</i></font></b></td>
	</tr>
	<tr>
		<td width="18%" valign='top'>Nama / <font style="font-size:8px;"><i>Name</i></font></td>
		<td width="3%" valign='top'>:</td>
		<td width="27%"  valign='top'>{$data_importer.importer_type|default:''}&nbsp;{$data_importer.importer_name|default:''}</td>
		<td width="5%">&nbsp;</td>
		<td width="18%" valign='top'>Nama / <font style="font-size:8px;"><i>Name</i></font></td>
		<td width="3%" valign='top'>:</td>
		<td width="27%"   valign='top'>{$data_exporter.name|default:'-'}</td>
	</tr>
	<tr>
		<td>Kontak / <font style="font-size:8px;"><i>Contact</i></font></td>
		<td>:</td>
		<td>{$data_importer.contact_name|upper}</td>
		<td>&nbsp;</td>
		<td>Kontak / <font style="font-size:8px;"><i>Contact</i></font></td>
		<td>:</td>
		<td>{$data_exporter.contact_name|upper}</td>
	</tr>		  
	<tr>
		<td valign='top'>Alamat / <font style="font-size:8px;"><i>Address</i></font></td>
		<td valign='top'>:</td>
		<td valign='top'>{$data_importer.address1|upper:'-'},<br/>{$data_importer.address2|upper:'-'}<br/>
		{$data_importer.city|upper:'-'},&nbsp;{$data_importer.propinsi|upper}&nbsp;{$data_importer.zip_code|default:'-'}</td>
		<td>&nbsp;</td>
		<td valign='top'>Alamat / <font style="font-size:8px;"><i>Address</i></font></td>
		<td valign='top'>:</td>
		<td  valign='top'>{$data_exporter.address|default:'-'}<br/>{$data_exporter.city|default:'-'}<br/>{$data_exporter.country_name}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$data_exporter.country_code}</td>
	</tr>
	<tr>
		<td>Telepon / <font style="font-size:8px;"><i>Phone</i></font></td>
		<td>:</td>
		<td>{$data_importer.phone|default:'-'}</td>
		<td>&nbsp;</td>
		<td>Telepon / <font style="font-size:8px;"><i>Phone</i></font></td>
		<td>:</td>
		<td>{$data_exporter.phone|default:'-'}</td>
	</tr>
	<tr>
		<td valign='top'>Surel / <font style="font-size:8px;"><i>Email</i></font></td>
		<td valign="top">:</td>
		<td valign='top'>{$data_importer.email_address|lower}</td>
		<td>&nbsp;</td>
		<td valign='top'>Surel / <font style="font-size:8px;"><i>Email</i></font></td>
		<td valign="top">:</td>
		<td valign="top" rowspan="4" >{$data_exporter.email_address|lower}</td>
	</tr>
	<tr>
		<td>NPWP</td>
		<td>:</td>
		<td>{$data_importer.npwp|default:'-'}</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>Status</td>
		<td>:</td>
		<td>{$data_importer.jenis_api|default:'-'}</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>NIB</td>
		<td>:</td>
		<td>{$data_importer.api_no|default:'-'}</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	{if $comm_code neq '11' && $comm_code neq '14' && $comm_code neq '15' && $comm_code neq '16' && $comm_code neq '17' && $comm_code neq '19' && $comm_code neq '20' && $comm_code neq '26' && $comm_code neq '33' && $comm_code neq '50'}
	<tr>
		<td valign="top">PI/IP/IT</td>
		<td valign="top">:</td>
		<td colspan="5" valign="top">{$data_npik_header.ip_no|default:'-'}</td>
	</tr>
	<tr>
		<td valign="top">Berakhir PI/IP/IT / <br><font style="font-size:8px;"><i>PI/IP/IT Expiry</i></font></td>
		<td valign="top">:</td>
		<td colspan="5" valign="top" >
			{if $data_npik_header.ip_expiry_date|date_format:"%d %b %Y" eq '30/12/1899' || $data_npik_header.ip_expiry_date eq ''} 
				- 
			{else}
				{$data_npik_header.ip_expiry_date|date_format:"%d %b %Y"|default:'-'}
			{/if}</td>
	</tr>
	{/if}
</table>
<br>			
<table width="100%" style="font-family:arial; font-size:10px;">	
	<tr>
	  <td colspan="7" style="color: navy;"><b>Data Pengapalan / <font style="font-size:8px;"><i>Shipment Data</i></font></b></td>
  </tr>
	<tr>
		<td width="18%" valign="top">Moda Transportasi / <br><font style="font-size:8px;"><i>Mode of Transportation</i></font></td>
		<td width="3%" valign="top">:</td>
		<td width="27%" valign="top">{$data_io_number.transportation_method} 
			{if $data_io_number.transportation_method<>'AIR'}
			({$data_io_number.package_type|default:'-'})
			{/if}
		</td>
		<td width="5%">&nbsp;</td>
		<td width="18%" valign="top">Invoice</td>
		<td width="3%" valign="top">:</td>
		<td width="27%" valign="top">{$data_io_number.pro_inv_no|default:'-'}</td>
	</tr>
	<tr>
		<td valign="top">Pelabuhan Muat / <br><font style="font-size:8px;"><i>Loading Port</i></font></td>
		<td valign="top">:</td>
		<td valign="top">{$data_io_number.loading_port} ({$data_io_number.code_loading_port})</td>
		<td>&nbsp;</td>
		<td valign="top">Total Nilai / <font style="font-size:8px;"><i>Total Value</i></font></td>
		<td valign="top">:</td>
		<td valign="top">{$data_io_number.currency|default:'-'}&nbsp;{$data_io_number.total_value|number_format:2:".":","|default:'-'}</td>
	</tr>
	<tr>
		<td valign="top">Pelabuhan Tujuan / <br><font style="font-size:8px;"><i>Port of Destination</i></font></td>
		<td valign="top">:</td>
		<td valign="top">{$data_io_number.discharge_port} ({$data_io_number.code_discharge_port})</td>
		<td>&nbsp;</td>
		<td valign="top">Incoterm</td>
		<td valign="top">:</td>
		<td valign="top">{$data_io_number.inco_term|default:'-'}</td>
	</tr>
	<tr>
		<td valign="top">Perkiraan Keberangkatan / <br><font style="font-size:8px;"><i>ETD</i></font></td>
		<td valign="top">:</td>
		<td valign="top">{$data_io_number.load_date|date_format:"%d %b %Y"}</td>
		<td >&nbsp;</td>
		<td valign="top">&nbsp;</td>
		<td valign="top">&nbsp;</td>
		<td valign="top">&nbsp;</td>
	</tr>
	<tr>
		<td valign="top">Perkiraan Ketibaan / <br><font style="font-size:8px;"><i>ETA</i></font></td>
		<td valign="top">:</td>
		<td valign="top">{$data_io_number.disch_date|date_format:"%d %b %Y"}</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>
<br>
<table style="font-family:arial; font-size:10px;width:100%">
	<tr>
		<th colspan="7" style="color: navy;" align="left"><b>Detail Verifikasi Barang / <font style="font-size:8px;"><i>Details of Goods Verified</i></font></b></th>
	</tr>
	<tr>
		<th width="5%" valign="top">No.</th>
		<th width="10%" align="center">Kode HS <br><font style="font-size:8px;"><i>HS Code</i></font></th>
		<th align="center">Uraian Barang <br><font style="font-size:8px;"><i>Goods Description</i></font></th>
		{if $comm_code eq '25'}
			<th width="40%" align="center">Kominfo Cert. No.</th>
		{/if}
		<th width="15%" align="center">Jumlah / Satuan<br><font style="font-size:8px;"><i>Quantity / UOM</i></font></th>
		{if $comm_code eq '11' || $comm_code eq '36' || $comm_code eq '40'}
			<th width="15%" align="center">Berat Bersih<br><font style="font-size:8px;"><i>Net Weight</i></font></th>
        {/if}
		<th width="20%" align="center">Negara Asal<br><font style="font-size:8px;"><i>Origin</i></font></th>
	</tr>
	{assign var=no_na value=0}
	{foreach from=$data_io_detail item=rows}        
	<tr>
		<td align="center" valign="top">{assign var=no_na value=$no_na+1}{$no_na}.</td>
		<td align="center" valign="top">{$rows.hs_code}</td>
		{if $comm_code eq '25'}
			<td align="justify">{$rows.description|default:'-'|escape}<br/>{$rows.quota_number|default:'-'}</td>
			<td align="justify">{$rows.no_certificate|default:'-'}</td>
		{else}
			<td align="justify">{$rows.description|default:'-'|escape}</td>
		{/if}
		<!--QUANTITY -->
		{if $comm_code eq '11' || $comm_code eq '36' || $comm_code eq '40'}
			<td align="right" valign="top">{$rows.quantity|number_format:3:".":","|default:'-'}&nbsp;{$rows.unit|default:'-'}</td>
			<td align="right" valign="top">{$rows.weight_kgs|number_format:3:".":","|default:'-'}&nbsp;{$rows.unit_net|default:'-'}</td>
		{else}
			<td align="right" valign="top">{$rows.quantity|number_format:3:".":","|default:'-'}&nbsp;{$rows.unit|default:'-'}</td>
		{/if}
		<td align="center" valign="top">{$rows.country_name|default:'-'}</td>
	</tr>
	{/foreach}
</table>
<br>
<table width="100%" style="margin-bottom:10px; font-family:arial; font-size:10px;">
	<tr>
		<td align="right">Tanggal VO / <font style="font-size:8px;"><i>VO Date</i></font></td>
	    <td width="2%">:</td>
	    <td align="left" width="27%">{$data_io_number.vo_date|date_format:"%d %b %Y"}</td>
	</tr>
	<tr>
		<td align="right">Berakhir VO / <font style="font-size:8px;"><i>VO Expiry</i></font></b></td>
	    <td >:</td>
	    <td align="left">{$expire_vo|date_format:"%d %b %Y"}&nbsp;{if $comm_code neq '24'}(Latest Arrival Date){else}(Latest Departure Date){/if}</td>

	</tr>
</table>
<br>
<table width="100%" style="margin-bottom:10px; font-family:arial; font-size:10px;">
	<tr>
		<td style="color: navy;"><b>Catatan / <font style="font-size:8px;"><i>Remarks</i></font></b></td>
	</tr>
	<tr>
		<td>{foreach from=$data_io_remarks item=rowss}
		{$rowss.remarks|escape:'html'} <br/>
	{/foreach}
		</td>
	</tr>
</table>
</div>
    </body>
</html>

<!DOCTYPE html>
<html>
<head>
    <title>VO Document</title>
    <style>
		table { width:100%; }
	</style>
</head>
<body>
<div id='content'>
	<table width="100%" style="margin-bottom:10px; font-family:arial; font-weight:none; font-size:10px;">
		  <tr>
			<td width="10%">VR No.</td>
			<td width="1%">:</td>
			<td width='89%' style="">{$data_io_number.registration_number|default:'-'}</td>
		  </tr>
		  <tr>
			<td>VO No.</td>
			<td>:</td>
			<td style="">{$data_io_number.io_number|default:'-'}</td>
		  </tr>
		  <tr>
			<td>VO Date</td>
			<td>:</td>
			<td style="">{$data_io_number.vo_date|default:'-'}&nbsp;&nbsp;&nbsp;VO Expiry : {$expire_vo|default:'-'}&nbsp;{if $comm_code neq '24'}(Latest Arrival Date){else}(Latest Departure Date){/if}

			</td>
		  </tr>
	</table>
	
	<table width="100%" style="margin-bottom:10px; font-family:arial; font-size:10px;">	
		  <tr>
			<td colspan="3" style="color: navy;"><b>IMPORTER</b></td>
			<td width="5%">&nbsp;</td>
			<td colspan="3"  style="color: navy;"><b>EXPORTER</b></td>
		  </tr>
		   <tr>
			<td width="20%" valign='top'>Name</td>
			<td width="1%" valign='top'>:</td>
			<td width="25%" style="" valign='top'>{$data_importer.importer_type|default:''}&nbsp;{$data_importer.importer_name|default:''}</td>
			<td >&nbsp;</td>
			<td width="20%" valign='top'>Name</td>
			<td width="1%" valign='top'>:</td>
			<td width="25%"  style="" valign='top'>{$data_exporter.name|default:'-'}</td>
		  </tr>
		  <tr>
			<td width="20%" valign='top'>Contact</td>
			<td width="1%" valign='top'>:</td>
			<td width="25%" style="" valign='top'>{$data_importer.contact_name|default:''}</td>
			<td >&nbsp;</td>
			<td width="20%" valign='top'>Contact</td>
			<td width="1%" valign='top'>:</td>
			<td width="25%"  style="" valign='top'>{$data_exporter.contact_name|default:'-'}</td>
		  </tr>
		   <tr>
			<td valign='top'>Address</td>
			<td valign='top'>:</td>
			<td width="27%" style="" valign='top'>{$data_importer.address1|default:'-'},<br/>{$data_importer.address2|default:'-'}<br/>{$data_importer.city|default:'-'},&nbsp;{$data_importer.propinsi|upper}&nbsp;{$data_importer.zip_code|default:'-'}</td>
			<td >&nbsp;</td>
			<td valign='top'>Address</td>
			<td valign='top'>:</td>
			<td style="" valign='top'>{$data_exporter.address|default:'-'}<br/>{$data_exporter.city|default:'-'}<br/>{$data_exporter.country_name}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$data_exporter.country_code}</td>
		  </tr>
		  <tr>
			<td width="20%" valign='top'>Phone</td>
			<td width="1%" valign='top'>:</td>
			<td width="25%" style="" valign='top'>{$data_importer.phone|default:'-'}</td>
			<td >&nbsp;</td>
			<td width="20%" valign='top'>Phone</td>
			<td width="1%" valign='top'>:</td>
			<td width="25%"  style="" valign='top'>{$data_exporter.phone|default:'-'}</td>
		  </tr>
		  <tr>
			<td width="20%" valign='top'>Fax.</td>
			<td width="1%" valign='top'>:</td>
			<td width="25%" style="" valign='top'>{$data_importer.fax|default:'-'}</td>
			<td >&nbsp;</td>
			<td width="20%" valign='top'>Fax.</td>
			<td width="1%" valign='top'>:</td>
			<td width="25%"  style="" valign='top'>{$data_exporter.fax|default:'-'}</td>
		  </tr>
		  <tr>
			<td width="20%" valign='top'>E-Mail</td>
			<td width="1%" valign='top'>:</td>
			<td width="25%" style="" valign='top'>{$data_importer.email_address|default:'-'}</td>
			<td >&nbsp;</td>
			<td width="20%" valign='top'>E-Mail</td>
			<td width="1%" valign='top'>:</td>
			<td width="25%" rowspan="4" style="" valign='top'>{$data_exporter.email_address|default:'-'}</td>
		  </tr>
		  <tr>
			<td width="20%" valign='top'>NPWP No.</td>
			<td width="1%" valign='top'>:</td>
			<td width="25%" style="" valign='top'>{$data_importer.npwp|default:'-'}</td>
			<td >&nbsp;</td>
			<td width="20%" valign='top'>&nbsp;</td>
			<td width="1%" valign='top'>&nbsp;</td>
		  </tr>
		  <tr>
			<td width="20%" valign='top'>Status</td>
			<td width="1%" valign='top'>:</td>
			<td width="25%" style="" valign='top'>{$data_importer.jenis_api|default:'-'}</td>
			<td >&nbsp;</td>
			<td width="20%" valign='top'>&nbsp;</td>
			<td width="1%" valign='top'>&nbsp;</td>
		  </tr>
		  <tr>
			<td width="20%" valign='top'>API/NIB No.</td>
			<td width="1%" valign='top'>:</td>
			<td width="25%" style="" valign='top'>{if $data_importer.api_no neq ''} 
					{$data_importer.api_no}
				{else}
					{$data_importer.nib|default:'-'}
				{/if}</td>
			<td >&nbsp;</td>
			<td width="20%" valign='top'>&nbsp;</td>
			<td width="1%" valign='top'>&nbsp;</td>
		  </tr>
		  {assign var='disableIPIT' value=','|explode:"11,22"}
		  {if !in_array($comm_code,$disableIPIT)}
		  <tr>
			<td width="20%" valign='top'>Importer License No.</td>
			<td width="1%" valign='top'>:</td>
			<td width="25%" style="" valign='top'>{$data_npik_header.ip_no|default:'-'}</td>
			<td >&nbsp;</td>
			<td width="20%" valign='top'>Expiry</td>
			<td width="1%" valign='top'>:</td>
			<td width="25%"  style="" valign='top'>
			{if $data_npik_header.ip_expiry_date|date_format:"%d/%m/%Y" eq '30/12/1899' || $data_npik_header.ip_expiry_date eq ''} 
				- 
			{else}
				{$data_npik_header.ip_expiry_date|date_format:"%d/%m/%Y"|default:'-'}
			{/if}
			</td>
		  </tr>
		  {/if}
		  {assign var='disableNpik' value=','|explode:"03,07,08,10,11,13,15,18,19,20,21,22,23,24,29,30,32,33"}
		  {if !in_array($comm_code,$disableNpik)}
		  <!--<tr> sesuai Permendag 50/M-DAG/PER/7/2015
			<td width="20%" valign='top'>NPIK</td>
			<td width="1%" valign='top'>:</td>
			<td width="25%" style="" valign='top'>{$data_npik_header.0.npik|default:''}</td>
			<td >&nbsp;</td>
			<td width="20%" valign='top'>NPIK Expiry</td>
			<td width="1%" valign='top'>:</td>
			<td width="25%"  style="" valign='top'>{$data_npik_header.0.npik_expiry_date|default:'-'}</td>
		  </tr>-->
		  {/if}
	</table>
	
	<table width="100%" style="margin-bottom:0px; font-family:arial; font-size:10px;">	
		  <tr>
			<td width="20%" valign='top'>Total Value</td>
			<td width="1%" valign='top'></td>
			<td width="25%" style="" valign='top'>: {$data_io_number.currency|default:'-'}&nbsp;{$data_io_number.total_value|number_format:2:".":","|default:'-'}</td>
			<td >&nbsp;</td>
			<td width="20%" valign='top'>&nbsp;</td>
			<td width="1%" valign='top'>&nbsp;</td>
			<td width="25%"  style="" valign='top'>&nbsp;</td>
		  </tr>
		  <tr>
			<td width="20%" valign='top'>Inco Term</td>
			<td width="1%" valign='top'></td>
			<td width="25%" style="" valign='top'>: {$data_io_number.inco_term|default:'-'}</td>
			<td >&nbsp;</td>
			<td width="20%" valign='top'>&nbsp;</td>
			<td width="1%" valign='top'>&nbsp;</td>
			<td width="25%"  style="" valign='top'>&nbsp;</td>
		  </tr>
		  
			{if $comm_code eq '18' || $comm_code eq '32' || $comm_code eq '36' || $comm_code eq '40'}
		  <tr>
			<td width="20%" valign='top'>Total Net Weight</td>
			<td width="1%" valign='top'></td>
			<td width="25%" style="" valign='top'>: {$data_io_number.net_weight|number_format:4:".":","|default:'-'}&nbsp;{$data_io_number.unit}</td>
			<td >&nbsp;</td>
			<td width="20%" valign='top'>&nbsp;</td>
			<td width="1%" valign='top'>&nbsp;</td>
			<td width="25%"  style="" valign='top'>&nbsp;</td>
		  </tr>
			{/if}
		  <tr>
			<td width="20%" valign='top'>Method Of Transport</td>
			<td width="1%" valign='top'></td>
			<td width="25%" style="" valign='top'>: {$data_io_number.transportation_method|default:'-'}-{$data_io_number.package_type|default:'-'}</td>
			<td >&nbsp;</td>
			<td width="20%" valign='top'>&nbsp;</td>
			<td width="1%" valign='top'>&nbsp;</td>
			<td width="25%"  style="" valign='top'>&nbsp;</td>
		  </tr>
		  <tr>
			<td width="20%" valign='top'>Loading Port</td>
			<td width="1%" valign='top'></td>
			<td width="25%" style="" valign='top'>: {$data_io_number.loading_port|default:'-'}</td>
			<td >&nbsp;</td>
			<td width="20%" valign='top'>Date</td>
			<td width="1%" valign='top'></td>
			<td width="25%"  style="" valign='top'>: {$data_io_number.load_date|default:'-'}</td>
		  </tr>
		  <tr>
			<td width="20%" valign='top'>Discharge Port</td>
			<td width="1%" valign='top'></td>
			<td width="25%" style="" valign='top'>: {$data_io_number.discharge_port|default:'-'}</td>
			<td >&nbsp;</td>
			<td width="20%" valign='top'>Date</td>
			<td width="1%" valign='top'></td>
			<td width="25%"  style="" valign='top'>: {$data_io_number.disch_date|default:'-'}</td>
		  </tr>
		  <tr>
			<td width="20%" valign='top'>Proforma Invoice No.</td>
			<td width="1%" valign='top'></td>
			<td width="25%" style="" valign='top'>: {$data_io_number.pro_inv_no|default:'-'}</td>
			<td >&nbsp;</td>
			<td width="20%" valign='top'>Date</td>
			<td width="1%" valign='top'></td>
			<td width="25%"  style="" valign='top'>: {$data_io_number.inv_date|default:'-'}</td>
		  </tr>
	</table>
	<br />
     <table style="font-family:helvetica,arial,verdana,sans; font-size:10px; margin-top: 10px; width:100%">
         <tr>
           <th width="5.28%">No.</th>
           <th width="14.28%">HS Code</th>
           {if $comm_code eq '25'}
           <th width="20%">Goods Description<br/>License No.</th>
           <th width="20%">KOMINFO Cert. No</th>
           {else}
           <th width="20%">Goods Description</th>
           {/if}
           <th width="14.28%">Origin</th>
           <th width="14.28%">Quantity/UOM</th>
           {if $comm_code eq '11'}
				<th width="14.28%">Net Weight(KG)</th>
			{elseif $comm_code eq '36' || $comm_code eq '40'}	
				<th width="14.28%">Net Weight</th>
           {/if}
		   {if $comm_code neq '11' || $comm_code neq '12' || $comm_code neq '15' || $comm_code neq '20' ||	$comm_code neq '24'}
				<th width="14.28%">RG</th>
		   {/if}
           <th width="14.28%">SKA</th>
         </tr>
        {assign var=no_na value=0} 
        {foreach from=$data_io_detail item=rows}
            <tr>
              <td style="" align="center" width="5.28%" valign="top">{assign var=no_na value=$no_na+1}{$no_na}</td>
                <td style="" align="center" width="8%" valign="top">{$rows.hs_code}</td>
                {if $comm_code eq '25'}
                <td style="" align="justify" width="20%">{$rows.description|default:'-'|escape}<br/>{$rows.quota_number|default:'-'}</td>
                <td style="" align="justify" width="10%">{$rows.no_certificate|default:'-'}</td>
                {else}
                <td style="" align="justify" width="30%">{$rows.description|default:'-'|escape}</td>
                {/if}
                <td style="" align="center" width="8%" valign="top">{$rows.origin|default:'-'}</td>
                {if $comm_code eq '18' || $comm_code eq '05' || $comm_code eq '07' || $comm_code eq '09' || $comm_code eq '24'|| $comm_code eq '32'}
					<td style="" align="center" width="15%" valign="top">{$rows.weight_kgs|number_format:4:".":","|default:'-'}&nbsp;{$rows.unit|default:'-'}</td>
                {elseif $comm_code eq '11'}
					<td style="" align="center" width="15%" valign="top">{$rows.quantity|number_format:4:".":","|default:'-'}&nbsp;{$rows.unit|default:'-'}</td>
					<td style="" align="center" width="15%" valign="top">{$rows.weight_kgs|number_format:4:".":","|default:'-'}</td>
				{elseif $comm_code eq '36' || $comm_code eq '40'}
					<td style="" align="center" width="15%" valign="top">{$rows.quantity|number_format:4:".":","|default:'-'}&nbsp;{$rows.unit|default:'-'}</td>
					<td style="" align="center" width="15%" valign="top">{$rows.weight_kgs|number_format:4:".":","|default:'-'}&nbsp;{$rows.unit_net|default:'-'}</td>
                {else}
					<td style="" align="center" width="15%" valign="top">{$rows.quantity|number_format:4:".":","|default:'-'}&nbsp;{$rows.unit|default:'-'}</td>
                {/if}
				{if $comm_code neq '11' || $comm_code neq '12' || $comm_code neq '15' || $comm_code neq '20' ||	$comm_code neq '24'}
					<td style="" align="center" width="8%" valign="top">{$rows.flag_rg|default:'-'}</td>
				{/if}
				<td style="" align="center" width="8%" valign="top">{$rows.skaform|default:'-'}</td>
            </tr>
        {/foreach}
		 
	</table>
	<br />
	<br />
	
	<table width="100%" style="margin-bottom:10px; font-family:arial; font-size:10px;">	
		<tr>
			<td style="color: navy;"><b>REMARKS</b></td>
		</tr>		
		<tr>
			<td style='padding-left:10px;'>
				{foreach from=$data_io_remarks item=rowss}
					{$rowss.remarks|escape:'html'} <br/>
				{/foreach}
			</td>
		</tr>
	</table>
	
</div>
</body>
</html>

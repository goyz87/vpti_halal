<!DOCTYPE html>
<html>
<head>
    <title>Laporan Surveyor</title>
    <style>
		select { text-transform:capitalize };
	</style>
</head>
<body>
<div id='content'>
	<div><b>
			<font style="font-family:arial; font-size:10px;"> I. Parties Concerned /</font> 
			<font style="font-family:bell; font-size:8px;"><i>Pihak - Pihak Terkait</i></font>
		</b>
	</div>
	<fieldset style="border:1px solid #878787;border-radius:5px;margin-bottom:5px;padding:10px;">
		<!--legend >Importer / Importir</legend-->
		<div>
			<b><font style="font-family:arial; font-size:10px;">Importer /</font>  
				<font style="font-family:bell; font-size:8px;"><i>Importir</i></font>
			</b>
		</div>
		<hr/>
		<table>
			<tr>
				<td valign='top' style="width: 190px;">
					<font style="font-family:arial; font-size:10px;"> Name /</font>
					<font style="font-family:bell; font-size:8px;"><i>Nama</i></font>
				</td>
				<td valign='top' style="width:20px; font-family:arial; font-size:10px;">:</td>
				<td style="font-family:arial; font-size:10px;" valign='top'>{$datalsheader.0.importer_type|default:''} {$datalsheader.0.importer_name|default:'-'}</td>
			</tr>
			<tr>
				<td valign='top'>
					<font style="font-family:arial; font-size:10px;">Address /</font> 
					<font style="font-family:bell; font-size:8px;"><i>Alamat</i></font>
				</td>
				<td valign='top' style="width:20px; font-family:arial; font-size:10px;">:</td>
				<td style="font-family:arial; font-size:10px;" valign='top'>{$datalsheader.0.importer_address|default:'-'}<br />{$datalsheader.0.importer_address1|default:'-'}</td>
			</tr>
			<tr>
				<td valign='top'>
					<font style="font-family:arial; font-size:10px;">City /</font>  
					<font style="font-family:bell; font-size:8px;"><i>Kota</i></font>
				</td>
				<td style="width:20px; font-family:arial; font-size:10px;">:</td>
				<td style="font-family:arial; font-size:10px;">{$datalsheader.0.importer_city|default:'-'}, &nbsp;{$datalsheader.0.propinsi|upper|default:'-'} &nbsp;{$datalsheader.0.importer_zipcode|default:''}</td>
			</tr>
			<tr>
				<td valign='top'>
					<font style="font-family:arial; font-size:10px;">NPWP</font>
				</td>
				<td style="font-family:arial; font-size:10px;">:</td>
				<td style="font-family:arial; font-size:10px;">{$datalsheader.0.npwp|default:'-'}</td>
			</tr>
			<tr>
			<td valign='top'>
				<font style="font-family:arial; font-size:10px;">API/NIB</font>
			</td>
			<td style="font-family:arial; font-size:10px;">:</td>
			<td style="font-family:arial; font-size:10px;">{$datalsheader.0.importer_api|default:'-'}</td>
			</tr>
			{if $commodity eq '11' || $commodity eq '14' || $commodity eq '15' || $commodity eq '16' || $commodity eq '17' || $commodity eq '19' || $commodity eq '20' || $commodity eq '33' || $commodity eq '47' || $commodity eq '50'}
			&nbsp;
			{elseif $commodity eq '26'}
				{if $datalsheader.0.ip_no neq ''}
					<tr>
					<td valign='top'>
						<font style="font-family:arial; font-size:10px;">PI/IP/IT</font></td>
					<td style="font-family:arial; font-size:10px;">:</td>
					<td style="font-family:arial; font-size:10px;">{if $datalsheader.0.ip_no eq ''}-{else}{$datalsheader.0.ip_no|default:'-'}{/if}</td>
				</tr>
				<tr>
					<td valign='top'>
						<font style="font-family:arial; font-size:10px;">Expiry PI/IP/IT /</font>  
						<font style="font-family:bell; font-size:8px;"><i>Tanggal Berakhir PI/IP/IT</i></font> 
					</td>
					<td style="font-family:arial; font-size:10px;">:</td>
					<td style="font-family:arial; font-size:10px;">
					{if $datalsheader.0.ip_expiry_date!='1899-12-30'}
						{$datalsheader.0.ip_expiry_date|date_format:'%d %b %Y'|default:'-'}
					{/if}
					</td>
				</tr>
				{/if}
			{else}
			<tr>
				<td valign='top'>
					<font style="font-family:arial; font-size:10px;">PI/IP/IT</font></td>
				<td style="font-family:arial; font-size:10px;">:</td>
				<td style="font-family:arial; font-size:10px;">{if $datalsheader.0.ip_no eq ''}-{else}{$datalsheader.0.ip_no|default:'-'}{/if}</td>
			</tr>
			<tr>
				<td valign='top'>
					<font style="font-family:arial; font-size:10px;">Expiry PI/IP/IT /</font>  
					<font style="font-family:bell; font-size:8px;"><i>Tanggal Berakhir PI/IP/IT</i></font> 
				</td>
				<td style="font-family:arial; font-size:10px;">:</td>
				<td style="font-family:arial; font-size:10px;">{$datalsheader.0.ip_expiry_date|date_format:'%d %b %Y'|default:'-'}</td>
			</tr>
			{/if}
		</table>
		
	</fieldset>
	
	<fieldset style="border:1px solid #878787;border-radius:5px;margin-bottom:10px;padding:10px;">
		<!--legend >Exporter / Eksportir</legend-->
		<div style="font-family:arial; font-size:10px;">
			<b>
				<font style="font-family:arial; font-size:10px;">Exporter /</font>   
				<font style="font-family:bell; font-size:8px;"><i>Eksportir</i></font>
			</b>
		</div>
		<hr/>
		<table>
			<tr>
				<td valign='top' style="width: 190px;">
					<font style="font-family:arial; font-size:10px;">Name /</font> 
					<font style="font-family:bell; font-size:8px;"><i>Nama</i></font>
				</td>
				<td valign='top' style="width:20px; font-family:arial; font-size:10px;">:</td>
				<td style="font-family:arial; font-size:10px;" valign='top'>{$datalsheader.0.exporter_name|default:'-'}</td>
			</tr>
			<tr>
				<td valign='top'>
					<font style="font-family:arial; font-size:10px;">Address /</font> 
					<font style="font-family:bell; font-size:8px;"><i>Alamat</i></font>
				</td>
				<td valign='top' style="font-family:arial; font-size:10px;">:</td>
				<td style="font-family:arial; font-size:10px;" valign='top' >{$datalsheader.0.exporter_address|default:'-'}, {$datalsheader.0.exporter_city|default:'-'}</td>
			</tr>
			<tr>
				<td>
					<font style="font-family:arial; font-size:10px;">Country /</font>  
					<font style="font-family:bell; font-size:8px;"><i>Negara</i></font>
				</td>
				<td style="font-family:arial; font-size:10px;">:</td>
				<td style="font-family:arial; font-size:10px;">{$datalsheader.0.country_name|default:'-'}</td>
			</tr>
			
		</table>
	</fieldset>
	
	<div>
		<b>
			<font style="font-family:arial; font-size:10px;">II. Shipment Data /</font>   
			<font style="font-family:bell; font-size:8px;"><i>Data Pengapalan</i></font>
		</b>
	</div>
	<fieldset style="border:1px solid #878787;border-radius:5px;margin-bottom:10px;padding:10px;">
		<table style="font-family:arial; font-size:10px;">
			<tr>
				<td valign="top" style="width: 190px;">
					<font style="font-family:arial; font-size:10px;">Mode of Transportation /</font> 
					<font style="font-family:bell; font-size:8px;"><i>Moda Transportasi</i></font>
				</td>
				<td valign="top" style="width:20px; font-family:arial; font-size:10px;">:</td>
				<td valign="top" style="font-family:arial; font-size:10px;">{$datalsheader.0.transportation_method|default:'-'}</td>
				<td valign="top" width="50px">&nbsp;</td>
				<td valign="top">
					<font style="font-family:arial; font-size:10px;">Invoice /</font> 
					<font style="font-family:bell; font-size:8px;"><i>Invoice</i></font>
				</td>
				<td valign="top" style="width:20px; font-family:arial; font-size:10px;">:</td>
				<td valign="top" style="font-family:arial; font-size:10px;">{$datalsheader.0.invoice_no|default:'-'}</td>
			</tr>
			<tr>
			  	<td valign="top">
			  		<font style="font-family:arial; font-size:10px;">Port of Loading /</font> 
			  		<font style="font-family:bell; font-size:8px;"><i>Pelabuhan Muat</i></font>
			  	</td>
				<td valign="top" style="font-family:arial; font-size:10px;">:</td>
				<td valign="top" style="font-family:arial; font-size:10px;">{$datalsheader.0.loading_port|default:'-'}</td>
				<td valign="top" >&nbsp;</td>
				<td valign="top">
					<font style="font-family:arial; font-size:10px;">Place of Verification /</font> 
					<font style="font-family:bell; font-size:8px;"><i>Tempat Verifikasi</i></font>
				</td>
				<td valign="top" style="font-family:arial; font-size:10px;">:</td>
				<td valign="top" style="font-family:arial; font-size:10px;">{$datalsheader.0.city_name|default:'-'}</td>
			</tr>
			<tr>
				<td valign="top">
					<font style="font-family:arial; font-size:10px;">Port of Discharge /</font>  
					<font style="font-family:bell; font-size:8px;"><i>Pelabuhan Bongkar</i></font>
				</td>
				<td valign="top" style="font-family:arial; font-size:10px;">:</td>
				<td valign="top" style="font-family:arial; font-size:10px;">{$datalsheader.0.discharge_port|default:'-'}</td>
				<td valign="top" >&nbsp;</td>
				<td valign="top">
					<font style="font-family:arial; font-size:10px;">Date of Verification /</font> 
					<font style="font-family:bell; font-size:8px;"><i>Tanggal Verifikasi</i></font>
				</td>
				<td valign="top" style="font-family:arial; font-size:10px;">:</td>
				<td valign="top" style="font-family:arial; font-size:10px;">{$datalsheader.0.inspection_date|date_format:'%d %b %Y'|default:'-'}</td>
			
			</tr>
			<tr>
				<td>
					<font style="font-family:arial; font-size:10px;">Total Net Weight /</font> 
					<font style="font-family:bell; font-size:8px;"><i>Total Berat Bersih</i></font>
				</td>
				<td style="font-family:arial; font-size:10px;">:</td>
				{if $commodity eq '36' || $commodity eq '24' || $commodity eq '15' || $commodity eq '13'}
					<td style="font-family:arial; font-size:10px;">{$datalsheader.0.net_weight|number_format:3:".":","|default:''} &nbsp; {$datalsheader.0.unit|default:'-'}</td>
				{else}
					<td style="font-family:arial; font-size:10px;">{$datalsheader.0.net_weight|number_format:4:".":","|default:''} &nbsp; {$datalsheader.0.unit|default:'-'}</td>
				{/if}
			</tr>
		</table>
	</fieldset>
	
	<div>
		<b>
		<font style="font-family:arial; font-size:10px;">III. Details of Goods Verified /</font> 
		<font style="font-family:bell; font-size:8px;"><i>Detail Verifikasi Barang</i></font>
		</b>
	</div>
	<fieldset style="border:1px solid #878787;border-radius:5px;margin-bottom:10px;padding:10px;">
		<table style="font-family:arial; font-size:10px;">	
			<tr>
				<td colspan="3" >
					<b>
					<font style="font-family:arial; font-size:10px;">Marks and Numbers of Packing /</font>  
					<font style="font-family:bell; font-size:8px;"><i>Keterangan dan Jumlah Kemasan</i></font>
					</b>
				</td>
				<td width="70px">&nbsp;</td>
				<td colspan="3" >
					<b>
					<font style="font-family:arial; font-size:10px;">Quantity and Type of Packing /</font> 
					<font style="font-family:bell; font-size:8px;"><i>Jumlah dan Jenis Kemasan</i></font>
					</b>
				</td>
			</tr>
	        {foreach from=$datalspackage item=rowws}
			<tr>
				<td colspan="3" style="font-family:arial; font-size:10px;"><b>{$rowws.packing_info|default:'-'}</b></td>
				<td >&nbsp;</td>
				<td colspan="3" style='padding-right:10px; font-family:arial; font-size:10px;'><b>{if $rowws.qty neq ''}{$rowws.qty|number_format:0:".":","|default:''}{else} {/if} {$rowws.unit|default:''}</b></td>
			</tr>
			{/foreach}
			
		</table>
	
		
	{if $datalsremarksdetil}
		<hr />
		<table width="50%">
			<tr>
				<td colspan="4" width="20%">
					<b>
					<font style="font-family:arial; font-size:10px;">Container Detail /</font>  
					<font style="font-family:bell; font-size:8px;"><i>Detail Kontainer</i></font>
					</b>
				</td>
			</tr>
			<tr>
				<td width="20px" align="left">
					<b>
					<font style="font-family:arial; font-size:10px;">No.</font> 
					</b>
				</td>
				<td width="10px">&nbsp;</td>
				<td width="350px">
					<b>
					<font style="font-family:arial; font-size:10px;">Size/Container No./Seal No. /</font> 
					<font style="font-family:bell; font-size:8px;"><i>Ukuran/No. Kontainer/No. Segel</i></font>
					</b>
				</td>
				<td width="150px">&nbsp;</td>								
			</tr>
			{assign var=no_nyya value=0} 
			{foreach from=$datalsremarksdetil item=detil}					  
			  <tr>
				<td style="font-family:arial; font-size:10px;" align="left">{assign var=no_nyya value=$no_nyya+1}{$no_nyya}.</td>
				<td>&nbsp;</td>
				<td style="font-family:arial; font-size:10px;">{$detil.container_size|default:''}FT / {$detil.container_number|default:''} / {$detil.seal_number|default:'-'}</td>
				<td>&nbsp;</td>
			  </tr>
			{/foreach}
		</table>
	{/if}
	
	<hr />
	<table width="100%">
        <tr>
          <th width="20px" valign="top" align="left" style='padding-right:10px;'>
          	<font style="font-family:arial; font-size:10px;">No.</font>
          </th>
          <th width="70px" align='left' valign="top" >
       		<font style="font-family:arial; font-size:10px; ">HS Code</font><br/>
          	<font style="font-family:bell; font-size:8px;"><i>Kode HS</i></font>
          </th>
          <th width="280px" align='left' valign="top" >
          	<font style="font-family:arial; font-size:10px;">Description of Goods</font><br/>
          	<font style="font-family:bell; font-size:8px;"><i>Uraian Barang</i></font>
          </th>		  
		  {assign var=colsepan value=3}
		  {if $commodity eq '09'}
		  {assign var=colsepan value=4}
		  <th width="8%" valign="top" >
		  	<font style="font-family:arial; font-size:10px;">ICUMSA</font>
		  </th>
		  {/if}
		  {if $commodity eq '08' || $commodity eq '21'}
		  {assign var=colsepan value=4}
		  <th width="8%" valign="top" >
		  	<font style="font-family:arial; font-size:10px;">CAS No.</font>
		  </th>
		  {/if}
		  <th width="100px" valign="top" >
       		<font style="font-family:arial; font-size:10px; ">Quantity / UoM</font><br/>
          	<font style="font-family:bell; font-size:8px;"><i>Jumlah / Satuan</i></font>
          </th>
		  <th width="100px" valign="top" >
       		<font style="font-family:arial; font-size:10px; ">Origin</font><br/>
          	<font style="font-family:bell; font-size:8px;"><i>Negara Asal</i></font>
          </th>
		</tr>
        {assign var=no_na value=0} 
        {foreach from=$datalsdetail item=rows}
            <tr>
				<td style='padding-right:10px; font-family:arial; font-size:10px;' align="left" valign="top" width="2px">
					{assign var=no_na value=$no_na+1}{$no_na}.
				</td>
				{if $rows.hs_code|count_characters:true > 8}
					<td style="font-family:arial; font-size:10px;" align="left" valign="top" width="10px">
						{$rows.hscode1}.{$rows.hscode2}.{$rows.hscode3}.{$rows.hscode4}						
					</td>
                {else}
					<td style="font-family:arial; font-size:10px;" align="left" valign="top" width="10px">
						{$rows.hscode1}.{$rows.hscode2}.{$rows.hscode3}						
					</td>
				{/if}
                                
				<td style='font-family:arial; font-size:10px;' valign="top" width="200px" align="left">
					{$rows.description|replace:'<':'&lt;'}
				</td>				
				{if $commodity eq '09'}
					<td style="font-family:arial; font-size:10px;" align="center" valign="top" width="10px">
						{$rows.icumsa_no|default:'-'}
					</td>
				{/if}
				{if $commodity eq '08' || $commodity eq '21'}
					<td style="font-family:arial; font-size:10px;" align="center" valign="top" width="10px">
						{$rows.cas_no|default:'-'}
					</td>
				{/if}
				
				<td style="font-family:arial; font-size:10px;" align="center" valign="top" width="10px">
					{if $commodity eq '36' || $commodity eq '24' || $commodity eq '15' || $commodity eq '13'}
						{$rows.qty|number_format:3:".":","|default:'-'} {$rows.unit|default:'-'}
					{else}
						{$rows.qty|number_format:4:".":","|default:'-'} {$rows.unit|default:'-'}
					{/if}
				</td>
			
				<td style="font-family:arial; font-size:10px;" align="center" valign="top" width="10px">
					{$rows.country_name|default:'-'}
				</td>
            </tr>
        {/foreach}
	</table>
	</fieldset>
	
	{if $datalsremarks}	
	<table style="margin-bottom:5px; margin-top:5px; font-family:arial; font-size:10px;">
		  <tr>
			<td width="100%"><b>Remarks</b></td>
		  </tr>
		  <tr>
			<td width="100%">
				{foreach from=$datalsremarks item=rowsss}
					{$rowsss.seg_no} &nbsp;&nbsp; {$rowsss.remarks} <br />
				{/foreach}		
			</td>
		  </tr>
	</table>
	{/if}
	<br />
	<div style='width:100%; float:right; text-align:right; font-family:arial; font-size:10px;'>
		<table width='100%' style="font-family:arial; font-size:10px;">
			<tr>
				<td width='100%' align='right'>
					<div>
					<font style="font-family:arial; font-size:10px;">Place and Date of Issuance /</font>
					<font style="font-family:bell; font-size:8px;"><i>Tempat dan Tanggal Terbit</i></font><br/>
					<font style="font-family:arial; font-size:10px;">JAKARTA, {$datalsheader.0.ls_date|date_format:'%d %b %Y'}</font> <br /><br />
					</div>
					<div>
						<img src='__repo/qr/{$barcode_file}.png' style='margin-right:-11px;' width='120px'>&nbsp; <br /><br />
					</div>
				</td>
			</tr>
		</table>
	</div>
	
	{if $datalampiran}
		<pagebreak />
		<fieldset style="border:1px solid #878787;border-radius:5px;margin-bottom:10px;padding:10px;">
		<div style="font-family:arial; font-size:10px;">
			<b>
				<font style="font-family:arial; font-size:10px;">Attachment /</font>   
				<font style="font-family:bell; font-size:8px;"><i>Lampiran</i></font>
			</b>
		</div>
		<hr/>		
		{if $datalsheader.0.flag_pdf eq '0'} <b><font style="font-family:arial; font-size:10px;">(*) FOUND ITEMS AS SUBJECT NON VPTI :</font></b>
		{elseif $datalsheader.0.flag_pdf eq '1'} <b><font style="font-family:arial; font-size:10px;">(*) CANCELLED SHIPMENT FOR THE FOLLOWING ITEMS :</font></b>
		{else} <b><font style="font-family:arial; font-size:10px;">(*) FOUND SHORT SHIPMENT FOR THE FOLLOWING ITEMS :</font></b>
		{/if}
		<table style="font-family:arial; font-size:10px; margin-top: 10px;">
			<tr>
			  <th width="5.28%" valign="top">No.</th>
			  <th width="8%">
			  	<font style="font-family:arial; font-size:10px; ">HS Code</font><br/>
          		<font style="font-family:bell; font-size:8px;"><i>Kode HS</i></font></th>
			  <th width="30%">
			  	<font style="font-family:arial; font-size:10px;">Description of Goods</font><br/>
          		<font style="font-family:bell; font-size:8px;"><i>Uraian Barang</i></font>
			</th>
			  <th width="10%">
			  	<font style="font-family:arial; font-size:10px; ">Origin</font><br/>
          		<font style="font-family:bell; font-size:8px;"><i>Negara Asal</i></font>
			</th>
			  {if $commodity eq '09'}
			  <th width="8%">ICUMSA</th>
			  {/if}
			  {if $commodity eq '08' || $commodity eq '21'}
			  <th width="8%">CAS No.</th>
			  {/if}
			  {if $commodity eq '18' || $commodity eq '23' || $commodity eq '21'}
				<th width="10%">Net Weight/Unit</th>
			  {elseif $commodity eq '05' || $commodity eq '07' || $commodity eq '08' || $commodity eq '09' || $commodity eq '24' || $commodity eq '49'}
				<th width="10%">Quantity/NW</th>
			  {else}
				<th width="10%">
					<font style="font-family:arial; font-size:10px; ">Quantity / UoM</font><br/>
					<font style="font-family:bell; font-size:8px;"><i>Jumlah / Satuan</i></font>
				</th>				
			  {/if}
			</tr>
			{assign var=no_naa value=0} 
			{foreach from=$datalampiran item=rows_lampiran}
				<tr>
				  <td style="" align="center" width="5.28%" valign="top">{assign var=no_naa value=$no_naa+1}{$no_naa}.</td>
					<td style="" align="center" width="8%" valign="top">{$rows_lampiran.hscode1}.{$rows_lampiran.hscode2}.{$rows_lampiran.hscode3}.{$rows_lampiran.hscode4}</td>
					<td style="" align="justify" width="35%" valign="top">{$rows_lampiran.description|default:'-'}</td>
					<td style="" align="center" width="10%" valign="top">{$rows_lampiran.country_name|default:'-'}</td>
					{if $commodity eq '09'}
						<td style="" align="center" width="8%">{$rows_lampiran.icumsa_no|default:'-'}</td>
					{/if}
					{if $commodity eq '08' || $commodity eq '21'}
						<td style="" align="center" width="8%">{$rows_lampiran.cas_no|default:'-'}</td>
					{/if}
					{if $commodity eq '18' || $commodity eq '23' || $commodity eq '21'}
						<td style="" align="center" width="10%" valign="top">{$rows_lampiran.net_weight|number_format:4:".":","|default:'-'} {$rows_lampiran.unit_net|default:'-'}</td>
					{else}
						<td style="" align="center" width="10%" valign="top">{$rows_lampiran.qty|number_format:4:".":","|default:'-'} {$rows_lampiran.unit|default:'-'}</td>
					{/if}
				</tr>
			{/foreach}
		</table>
		</fieldset>
	{/if}	
	
</div>

</body>
</html>

<!DOCTYPE html>
<html>
<head>
    <title>VR Document</title>
    <style>
		table { width:100%; }
	</style>
</head>
<body>
<div id='content' style="">

	<div style="font-family:arial; font-size:10px;">
		<br/>
		<table width="100%" style="font-family:arial; font-size:10px;">			  
			<tr>
				<td colspan="3" style='color:navy;'><b>Importir / 
				<font style="font-size:8px;"><i>Importer</i></font></b></td>
				<td width="5%">&nbsp;</td>
				<td colspan="3" style='color:navy;'><b>Eksportir / 
				<font style="font-size:8px;"><i>Exporter</i></font></b></td>
			</tr>
			<tr>
				<td width="18%" valign="top" >Nama / <font style="font-size:8px;"><i>Name</i></font></td>
				<td width="3%" valign="top" >:</td>
				<td width="27%">{$dataCompany.COMPANY_NAME|default:'-'}</td>
				<td width="5%">&nbsp;</td>
				<td width="18%" valign="top" >Nama  / <font style="font-size:8px;"><i>Name</i></font></td>
				<td width="3%" valign="top" >:</td>
				<td width="27%" >{$dataDetail.exporter_name|default:'-'}</td>
			</tr>
			<tr>
				<td valign="top">Kontak / <font style="font-size:8px;"><i>Contact</i></font></td>
				<td valign="top">:</td>
				<td valign="top">{$dataCompany.contact_name|default:'-'}</td>
				<td >&nbsp;</td>
				<td valign="top">Kontak / <font style="font-size:8px;"><i>Contact</i></font></td>
				<td valign="top">:</td>
				<td valign="top">{$dataDetail.exporter_contact|default:'-'}</td>
			</tr>
			<tr>
				<td valign="top">Alamat / <font style="font-size:8px;"><i>Address</i></font></td>
				<td valign="top">:</td>
				<td valign="top" >{$dataCompany.address1|default:'-'}</td>
				<td >&nbsp;</td>
				<td valign="top">Alamat / <font style="font-size:8px;"><i>Address</i></font></td>
				<td valign="top">:</td>
				<td valign="top" >{$dataDetail.exporter_addr|replace:"^":"'"|default:'-'}</td>
			</tr>
			<tr>
				<td valign="top">Kota / <font style="font-size:8px;"><i>City</i></font></td>
				<td valign="top">:</td>
				<td >{$dataCompany.city|default:'-'}, {$dataCompany.propinsi|default:'-'} &nbsp;{$dataCompany.zip_code|default:''}</td>
				<td >&nbsp;</td>
				<td valign="top">Kota / <font style="font-size:8px;"><i>City</i></font></td>
				<td valign="top">:</td>
				<td valign="top">{$dataDetail.exporter_city|default:'-'}</td>
			</tr>
			<tr>
				<td >Telepon / <font style="font-size:8px;"><i>Phone</i></font></td>
				<td >:</td>
				<td >{$dataCompany.phone|default:'-'}</td>
				<td >&nbsp;</td>
				<td >Negara / <font style="font-size:8px;"><i>Country</i></font></td>
				<td >:</td>
				<td >{$dataDetail.exporter_country|default:'-'}</td>
			</tr>
			<tr>
				<td valign="top">Surel / <font style="font-size:8px;"><i>Email</i></font></td>
				<td valign="top">:</td>
				<td >{$dataCompany.email_address|default:'-'}</td>
				<td >&nbsp;</td>
				<td >Telepon / <font style="font-size:8px;"><i>Phone</i></font></td>
				<td >:</td>
				<td  >{$dataDetail.exporter_telp|default:'-'}</td>
			</tr>
			<tr>
				<td valign="top">NPWP</td>
				<td valign="top">:</td>
				<td valign="top" >{$dataCompany.npwp|default:'-'}</td>
				<td >&nbsp;</td>
				<td valign="top">Surel / <font style="font-size:8px;"><i>Email</i></font></td>
				<td valign="top">:</td>
				<td rowspan="3" valign="top" >{$dataDetail.exporter_email|default:'-'}</td>
			</tr>
			<tr>
				<td valign="top">Status</td>
				<td valign="top">:</td>
				<td valign="top" >{$dataCompany.jenis_api|default:'-'}</td>
				<td >&nbsp;</td>
				<td valign="top">&nbsp;</td>
				<td valign="top">&nbsp;</td>
			</tr>
			<tr>
				<td valign="top">NIB</td>
				<td valign="top">:</td>
				<td valign="top" >{if $dataCompany.api_no neq ''} 
					{$dataCompany.api_no}
				{else}
					{$dataCompany.nib|default:'-'}
				{/if}</td>
				<td >&nbsp;</td>
				<td >&nbsp;</td>
				<td >&nbsp;</td>
		    </tr>
			{if $dataCompany.commodity_code neq '11' && $dataCompany.commodity_code neq '14' && $dataCompany.commodity_code neq '15' && $dataCompany.commodity_code neq '16' && $dataCompany.commodity_code neq '17' && $dataCompany.commodity_code neq '19' && $dataCompany.commodity_code neq '20' && $dataCompany.commodity_code neq '26' && $dataCompany.commodity_code neq '33' && $dataCompany.commodity_code neq '50'}
			<tr>
				<td valign="top">PI/IP/IT</td>
				<td valign="top">:</td>
				<td colspan="5" valign="top" >{$dataDetail.import_license_no|default:'-'}</td>
			</tr>
			<tr>
				<td valign="top">Berakhir PI/IP/IT / <br><font style="font-size:8px;"><i>PI/IP/IT Expiry</i></font></td>
				<td valign="top">:</td>
				<td colspan="5" valign="top" >{$dataDetail.ip_expiry_date|date_format:"%d %b %Y"|default:'-'}</td>
			</tr>
			{/if}
		</table>
		<br/>
		<table width="100%" style="font-family:arial; font-size:10px;">			  
			<tr>
				<td colspan="3" style="color: navy;"><b>Pemasok (Tempat Verifikasi) / <font style="font-size:8px;"><i>Supplier (Place of Verification)</i></font></b></td>
			</tr>
			<tr>
				<td width="18%" valign="top" >Nama / <font style="font-size:8px;"><i>Name</i></font></td>
				<td width="3%" valign="top" >:</td>
				<td width="79%" >{$dataDetail.supplier_name|default:'-'}</td>
			</tr>
			<tr>
				<td >Kontak / <font style="font-size:8px;"><i>Contact</i></font></td>
				<td >:</td>
				<td >{$dataDetail.supplier_contact|default:'-'}</td>
			</tr>
			<tr>
				<td >Alamat / <font style="font-size:8px;"><i>Address</i></font></td>
				<td >:</td>
				<td >{$dataDetail.supplier_addr|default:'-'}</td>
			</tr>
			<!--tr>
			  <td >Kota / <font style="font-size:8px;"><i>City</i></font></td>
			  <td >:</td>
			  <td >&nbsp;</td>
		  </tr-->
			<tr>
				<td >Negara / <font style="font-size:8px;"><i>Country</i></font></td>
				<td >:</td>
				<td >{$dataDetail.supplier_country|default:'-'}</td>			
			</tr>
			<tr>
				<td >Telephone / <font style="font-size:8px;"><i>Phone</i></font></td>
				<td >:</td>
				<td >{$dataDetail.supplier_telp|default:'-'}</td>
			</tr>
			<tr>
				<td valign="top">Surel / <font style="font-size:8px;"><i>Email</i></font></td>
				<td valign="top">:</td>
				<td valign="top" >{$dataDetail.supplier_email|default:'-'}</td>			
			</tr>
		</table>
		<br/>
		<table width="100%" style="font-family:arial; font-size:10px;">			  			
			<tr>
			  <td colspan="7" style="color: navy;"><b>Data Pengapalan / <font style="font-size:8px;"><i>Shipment Data</i></font></b></td>
		  </tr>
			<tr>
				<td width="18%" valign="top">Moda Transportasi / <br><font style="font-size:8px;"><i>Mode of Transport</i></font></td>
				<td width="3%" valign="top">:</td>
				<td width="27%" valign="top">
							{if $dataDetail.transportation_mode eq 'SEA'}
								{$dataDetail.transportation_mode|default:'-'} 
							{else}
								{$dataDetail.transportation_mode|default:'-'}
							{/if}
							{if $dataDetail.transportation_mode neq 'AIR'}
								({$dataDetail.package_type|default:'-'})
				{/if}</td>
				<td width="4%">&nbsp;</td>
				<td width="18%"><i>Invoice</i></td>
				<td width="3%">:</td>
				<td width="27%">{$dataDetail.invoice_no|default:'-'} </td>
			</tr>
			<tr>
				<td >Pelabuhan Muat / <br><font style="font-size:8px;"><i>Port Of Loading</i></font></td>
				<td valign="top">:</td>
				<td valign="top">{$dataDetail.port|default:'-'}</td>
				<td >&nbsp;</td>
				<td valign="top">Total Nilai / <font style="font-size:8px;"><i>Total Value</i></font></td>
				<td valign="top">:</td>
				<td valign="top">{$dataDetail.curr_code|default:'-'}
					{if $dataDetail.curr_code eq 'IDR'}
						{$dataDetail.total_value|number_format:2:",":"."|default:0}
					{else}
						{$dataDetail.total_value|number_format:2:".":","|default:0}
					{/if}
				</td>
			</tr>
			<tr>
				<td >Pelabuhan Tujuan / <br><font style="font-size:8px;"><i>Port Of Destination</i></font></td>
				<td valign="top">:</td>
				<td valign="top">{$dataDetail.port_name|default:'-'}</td>
				<td >&nbsp;</td>
				<td valign="top"><i>Incoterm</i></td>
				<td valign="top">:</td>
				<td valign="top">{$dataDetail.incoterm|default:'-'}</td>
			</tr>
			<tr>
				<td >Perkiraan Keberangkatan / <font style="font-size:8px;"><i>ETD</i></font></td>
				<td valign="top">:</td>
				<td valign="top">{$dataDetail.shipment_date|date_format:"%d %b %Y"|default:'-'}</td>
				<td >&nbsp;</td>
				<td >Total Berat Bersih / <br><font style="font-size:8px;"><i>Total Net Weight</i></font></td>
				<td valign="top">:</td>
				<td valign="top">{$dataDetail.tot_net_weight|number_format:4:".":","|default:0}&nbsp;{$dataDetail.tot_net_weight_unit|default:'-'}</td>
			</tr>
			<tr>
				<td >Perkiraan Ketibaan / <font style="font-size:8px;"><i>ETA</i></font></td>
				<td >:</td>
				<td >{$dataDetail.discharge_date|date_format:"%d %b %Y"|default:'-'}</td>
				<td >&nbsp;</td>
				<td >Pengiriman Sebagian / <br><font style="font-size:8px;"><i>Partial Shipment</i></font></td>
				<td valign="top">:</td>
				<td valign="top">{$dataDetail.partial|default:'-'}</td>
			</tr>
		</table>
		  <br />
			<table style="font-family:arial; font-size:10px;width:100%">
				<tr>
				  <th colspan="10" style="color: navy;" align="left"><b>Detail Verifikasi Barang / <font style="font-size:8px;"><i>Details of Goods Verified</i></font></b></th>
			  </tr>
				<tr>
				  <th width="5%" valign="top">No.</th>
				  <th width="10%">Kode HS<br><font style="font-size:8px;"><i>HS Code</i></font></th>
				  <th width="35%" align="center">Uraian Barang<br><font style="font-size:8px;"><i>Goods Description</i></font></th>
				  <th width="15%" align="center">Jumlah / Satuan<br><font style="font-size:8px;"><i>Quantity / UOM</i></font></th>
				  <th width="15%" align="center">Berat Bersih <br><font style="font-size:8px;"><i>Net Weight</i></font></th>
				  <th width="20%" align="center">Negara Asal<br><font style="font-size:8px;"><i>Origin</i></font></th>
				</tr>
				{assign var=no_na value=0} 
				{foreach from=$detailSubmission item=rows}
				 <tr>
					<td style="padding:5px;" align="center" valign="top">{assign var=no_na value=$no_na+1}{$no_na}.</td>
					<td style="padding:5px;" align="center" valign="top">{$rows.hs_code}</td>
					<!--desciption-->
					{if $dataCompany.commodity_code eq '20' || $dataCompany.commodity_code eq '29'}
						<td style="padding:5px;">{$rows.hs_description|escape:'html'}<br>BPOM RI:&nbsp;{$rows.bpom_na_number|default:"-"}<br>Item Code:&nbsp;{$rows.item_code|default:"-"}<br><b style="color:red">{$rows.cekkmk|default:""}</b></td>
					{elseif $dataCompany.commodity_code eq '15'}
						<td style="padding:5px;">{$rows.hs_description|escape:'html'}<br>BPOM RI:&nbsp;{$rows.bpom_na_number|default:"-"}<br><b style="color:red">{$rows.cekkmk|default:""}</b></td>
					{elseif $dataCompany.commodity_code eq '08' || $dataCompany.commodity_code eq '11' || $dataCompany.commodity_code eq '14' || $dataCompany.commodity_code eq '16' || $dataCompany.commodity_code eq '17' || $dataCompany.commodity_code eq '19' || $dataCompany.commodity_code eq '26' || $dataCompany.commodity_code eq '30' || $dataCompany.commodity_code eq '33' || $dataCompany.commodity_code eq '35' || $dataCompany.commodity_code eq '36' || $dataCompany.commodity_code eq '45'}
						<td style="padding:5px;">{$rows.hs_description|escape:'html'}<br>SPPT SNI:&nbsp;{$rows.sppt_sni|default:"-"}<br><b style="color:red">{$rows.cekkmk|default:""}</b></td>
					{elseif $dataCompany.commodity_code eq '02' || $dataCompany.commodity_code eq '14' || $dataCompany.commodity_code eq '26' || $dataCompany.commodity_code eq '35' || $dataCompany.commodity_code eq '36' || $dataCompany.commodity_code eq '40'}
						<td style="padding:5px;">{$rows.hs_description|escape:'html'}<br>{$rows.sni_pertek}<br><b style="color:red">{$rows.cekkmk|default:""}</b></td>
					{elseif $dataCompany.commodity_code eq '30' || $dataCompany.commodity_code eq '31' }
						<td style="padding:5px;">{$rows.hs_description|escape:'html'}<br>SKPLBI:&nbsp;{$rows.skplbi|default:"-"}<br>SPPT SNI:&nbsp;{$rows.sppt_sni|default:"-"}<br><b style="color:red">{$rows.cekkmk|default:""}</b></td>
					{elseif $dataCompany.commodity_code eq '18'}
						<td style="padding:5px;">{$rows.hs_description|escape:'html'}{if $rows.quantity neq ""}, {$rows.quantity|number_format:4:".":","}{/if} &nbsp;{$rows.unit}<br><b style="color:red">{$rows.cekkmk|default:""}</b></td>
					{elseif $dataCompany.commodity_code eq '17'}
						<td style="padding:5px;">{$rows.hs_description|escape:'html'}<br>SPPT SNI:&nbsp;{$rows.sppt_sni|default:"-"}<br><b style="color:red">{$rows.cekkmk|default:""}</b></td>
					{else}
						<td style="padding:5px;">{$rows.hs_description|escape:'html'}<br><b style="color:red">{$rows.cekkmk|default:""}</b></td>
					{/if}
					<!--end of desciption-->
					<!--QTY and Net Weight-->
					<td style="padding:5px;" align="right" valign="top">{$rows.quantity|number_format:4:".":","|default:0}&nbsp;{$rows.unit}</td>
					<td style="padding:5px;" align="right" valign="top">{$rows.weight_kgs|number_format:4:".":","|default:0}&nbsp;{$rows.unit_net}</td>
					<!--end of QTY and Net Weight-->
					<td style="padding:5px;" align="center" valign="top">{$rows.country_name}</td>
				</tr>
				{/foreach}
			</table>
			
		<br />
		<table width="100%" style="font-family:arial; font-size:10px;">			  			
			<tr>
				<td width="18%" valign="top">Catatan / <font style="font-size:8px;"><i>Remarks</i></font></td>
				<td width="3%" valign="top">:</td>
				<td >{$dataDetail.remarks|default:'-'}</td>
			</tr>
		</table>
	  <br />
			<table width="100%" style="font-family:arial; font-size:10px;">
			 <tr>
			    <td align="right">Tanggal VR / <font style="font-size:8px;"><i>VR Date</i></font></td>
		      </tr>
			 <tr>
				<td align="right">{$dataDetail.vr_submit_date1|date_format:"%d %b %Y"|default:'-'}</td>
			 </tr>
			 <tr>
			  <td align="right">&nbsp;</td>
			 </tr>
			 <tr>
			  <td align="right">{$pic}</td>
			 </tr>	
			</table>
			<br />
		<!--div style="text-indent: 0px; margin-bottom: 10px; font-size:9px; color: navy; text-align:justify">
			Saya menyatakan dengan ini:<br>
			Saya setuju untuk membayar dimuka sebesar 50% jasa VPTI pada saat menyerahkan Verification Request (bukti pembayaran terlampir) dan akan
menyelesaikan pembayaran pada saat mengambil LS, atau melakukan pembayaran deposit.<br><br>
			<i>I herewith declare:<br />
			I agree to pay in advance 50% of VPTI fee when submitting Verification Request (transfer payment is attached) and will complete the payment when collecting LS, alternatively to pay deposit payment.</i><br><br>
			{$pic}
		</div-->
	</div>
</div>
</body>
</html>


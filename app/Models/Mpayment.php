<?php

namespace App\Models;

use CodeIgniter\Model;
use CodeIgniter\I18n\Time;
class Mpayment extends Model
{
    // ...
    protected $table='vpti_importer';
    protected function initialize()
    {
        $this->db = db_connect();
        $this->db_keu = db_connect('keu');
        $this->db_pay = db_connect('pay');
		$builder = $this->db->table('vpti_importer');
		$this->session = \Config\Services::session(); 
		$this->link_db = 'H2H';
    }
    function get_data($p1,$p2="",$p3="",$p4="")
    {
		$where =" WHERE 1=1 ";
        switch ($p1){
			case "get_bill_his":
				//$mva=$this->input->post('mva');
				if ($p2 == 'inv') {
					//$mva=base64_decode($p3);
					//$mva = $this->encrypt->decode($this->input->post('mva'));
					$mva = $p3;
					//echo $mva;exit;
					//$id_header=$this->db_pay->get_where('BillPay_Hd',array('no_nva'=>$mva))->row_array();
					/*
					$sql = "SELECT A.*,
					CONVERT(VARCHAR(11), A.Doc_Date, 106)+' '+CONVERT(VARCHAR(8), A.Doc_Date, 114) AS tgl
					FROM H2H.dbo.BillPay_Hd A
					WHERE A.MvaNoId = '" . $mva . "'";
					*/
					$sql = "SELECT A.*, CONVERT (VARCHAR(11), A.Doc_Date, 106) + ' ' + CONVERT (VARCHAR(8), A.Doc_Date, 114) AS tgl
							FROM(
								SELECT X.MvaNoId, X.Doc_Date, X.Valid_Date, X.Curr, X.Amount, '-' as Method, '-' as Link, '0' as TotalAsli, '0' as Fee
								FROM H2H.dbo.BillPay_Hd X
								UNION ALL
								SELECT Y.CheckoutNo, Y.Doc_Date, Y.Valid_Date, Y.Curr, Amount, Y.Method, Y.Link, Y.TotalAsli, Y.Fee
								FROM H2H.dbo.DokuCheckout_Hd Y
							)	A
							WHERE A.MvaNoId = '" . $mva . "'";
					//echo $sql;exit;
					$id_header = $this->db_pay->query($sql)->getRowArray();

					/*
					$sql = "SELECT * FROM H2H.dbo.BillPay_Dt WHERE MvaNoId='" . $id_header['MvaNoId'] . "'";
					*/
					$sql = "SELECT A.*
							FROM(
								SELECT X.MvaNoId, X.Tp, X.Vo_No, X.Ls_No, X.Inv_No, X.Curr, X.Amount, X.Descs
								FROM H2H.dbo.BillPay_Dt X
								UNION ALL
								SELECT Y.CheckoutNo, Y.Tp, Y.Vo_No, Y.Ls_No, Y.Inv_No, Y.Curr, Y.Amount, Y.Descs
								FROM H2H.dbo.DokuCheckout_Dt Y
							) A
							WHERE MvaNoId='" . $id_header['MvaNoId'] . "'";

					//echo $sql;exit;
					$data = array('data_header' => $id_header, 'data_detil' => $this->db_pay->query($sql)->getResultArray());
				} else {
					//$doc_no=base64_decode($p3);
					//$doc_no = $this->encrypt->decode($this->input->post('mva'));
					$doc_no = $p3;
					$sql = "SELECT A.*,
					--REPLACE(CONVERT(VARCHAR(11), A.Doc_Date, 106), ' ', '-') AS tgl
					CONVERT(VARCHAR(11), A.Doc_Date, 106)+' '+CONVERT(VARCHAR(8), A.Doc_Date, 114) AS tgl
					FROM H2H.dbo.PayConfirmHd A
					WHERE A.Doc_No = '" . $doc_no . "' AND A.VptiId = " . $this->session->get('COMPANY_ID') . " AND A.CommCd = 'X." . $this->session->get('commodity_code') . "'";
					$id_header = $this->db_pay->query($sql)->getRowArray();
					//$id_header=$this->db_pay->get_where('PayConfirmHd',array('Doc_No'=>$doc_no,'VptiId'=>$this->auth['COMPANY_ID'],'CommCd'=>'X.'.$this->auth['commodity_code']))->row_array();
					//echo $this->db_pay->last_query();exit;
					$sql = "SELECT * FROM H2H.dbo.PayConfirmDt WHERE Doc_No='" . $id_header['Doc_No'] . "'";
					$data = array('data_header' => $id_header, 'data_detil' => $this->db_pay->query($sql)->getResultArray());
				}


				return $data;
			break;
			case "his_bill":
				$this->db->query("SET ANSI_NULLS ON");
				$this->db->query("SET ANSI_WARNINGS ON");
				$sql = "SELECT ROW_NUMBER() OVER (ORDER BY A.MvaNoId DESC) as rowID,A.*,
						
						CONVERT(VARCHAR(11), A.Doc_Date, 106)+' '+CONVERT(VARCHAR(8), A.Doc_Date, 114) AS tgl
						
						FROM
						(
							SELECT VptiId,MvaNoId,Doc_Date,Valid_Date,CommCd,Curr,Amount,Descs,BillCd,Status,'-' as Link
							FROM [192.168.1.211]." . $this->link_db . ".dbo.BillPay_Hd
							UNION ALL
							SELECT AA.VptiId,--BB.Rekening,
							AA.CheckoutNo,AA.Doc_Date,AA.Valid_Date,AA.CommCd,AA.Curr,AA.Amount,AA.Descs,AA.BillCd,AA.Status,AA.Link
							FROM [192.168.1.211]." . $this->link_db . ".dbo.DokuCheckout_Hd AA
							LEFT JOIN [192.168.1.211]." . $this->link_db . ".dbo.DokuCheckout_Paid BB ON AA.CheckoutNo = BB.CheckoutNo
						) A
						WHERE A.VptiId=" . $this->session->get('COMPANY_ID') . " AND A.CommCd='X." . $this->session->get('commodity_code') . "'";
						//echo $sql;
				/*$sql="SELECT ROW_NUMBER() OVER (ORDER BY A.MvaNoId DESC) as rowID,
						A.*,REPLACE(CONVERT(VARCHAR(11), A.Valid_Date, 106), ' ', '-') AS tgl
						FROM payment.dbo.BillPay_Hd A
						WHERE A.VptiId=".$this->auth['COMPANY_ID']." AND A.CommCd='X.".$this->auth['commodity_code']."'";
				*/
				//return $this->json($sql, '', 'list_billing_submit');
			break;
			case "get_nva":
				//if($p2=='IDR')$field="VA_IDR"
				/*$sql="SELECT VA_".$p2." as nva,BnsCd FROM payment.dbo.Importer
						WHERE vpti_Id=".$this->auth['COMPANY_ID']."
						AND Item_Cd='X.".$this->auth['commodity_code']."'";
					//echo $sql;
				*/

				$sql = "SELECT VA_" . $p2 . " as nva,BnsCd FROM H2H.dbo.Importer
						WHERE vpti_Id=" . $this->session->get('COMPANY_ID') . "
						AND Item_Cd='X." . $this->session->get('commodity_code') . "'";

				$nva = $this->db_pay->query($sql)->getRowArray();
				return $nva;
				break;
			case "get_bill":
				$this->db->query("SET ANSI_NULLS ON");
				$this->db->query("SET ANSI_WARNINGS ON");
				$select = 'ROW_NUMBER() OVER (ORDER BY A.Vo_No DESC) as rowID, ';
				$where .= " AND (mamt1 + mamt2 + mamt3 + mamt4)>1 ";
				$where .= " AND Doc_No IN ('".join("','",$p2)."') ";
				$sql = "SELECT " . $select . "A.* FROM [192.168.1.211]." . $this->link_db . ".dbo.ViewTrnDtl A

						LEFT JOIN vptidatabase.dbo.vpti_importer C ON A.Vpti_Id=C.importer_id_new_table
						LEFT JOIN vptidatabase.dbo.vpti2_importer D ON C.importer_id_new_table=D.id
						 " . $where . "  AND A.Status=0 AND C.importer_id_new_table='" . $this->session->get('COMPANY_ID') . "'
						AND C.commodity_code='" . $this->session->get('commodity_code') . "'
						
						";
				return $this->db->query($sql)->getResultArray();
				//echo $sql;exit;
			break;
			case "get_list_pay":
				$select = 'ROW_NUMBER() OVER (ORDER BY A.Vo_No DESC) as rowID, ';
				//if ($p2 != 'IDR') {
				//	$where .= " AND (famt1 + famt2 + famt3 + famt4)>1 ";
				//} else {
					$where .= " AND (mamt1 + mamt2 + mamt3 + mamt4)>1 ";
				//}
				$sql = "SELECT " . $select . "A.* FROM [192.168.1.211]." . $this->link_db . ".dbo.ViewTrnDtl A

						LEFT JOIN vptidatabase.dbo.vpti_importer C ON A.Vpti_Id=C.importer_id_new_table
						LEFT JOIN vptidatabase.dbo.vpti2_importer D ON C.importer_id_new_table=D.id

						 " . $where . "  AND A.Status=0 AND C.importer_id_new_table='" . $this->session->get('COMPANY_ID') . "'
						AND C.commodity_code='" . $this->session->get('commodity_code') . "'
						--AND A.Vo_No NOT IN(
						--	SELECT A.Vo_No FROM [192.168.1.211]." . $this->link_db . ".dbo.BillPay_Dt A
						--	LEFT JOIN [192.168.1.211]." . $this->link_db . ".dbo.BillPay_Hd B ON A.MvaNoId=B.MvaNoId
						--	WHERE B.VptiId='" . $this->session->get('COMPANY_ID') . "' AND B.CommCd='X." . $this->session->get('commodity_code') . "'
						--)AND A.Vo_No NOT IN(
						--SELECT A.Vo_No FROM [192.168.1.211]." . $this->link_db . ".dbo.PayConfirmDt A
						--LEFT JOIN [192.168.1.211]." . $this->link_db . ".dbo.PayConfirmHd B ON A.Doc_No=B.Doc_No
						--WHERE B.VptiId='" . $this->session->get('COMPANY_ID') . "' AND B.CommCd='X." . $this->session->get('commodity_code') . "'
						--)
						";
				//echo $sql;exit;
				return $this->db->query($sql)->getResultArray();
			break;
			case "info_status":
				$io=$_POST["dt_vo"];
				$partial=$_POST["dt_partial"];
				$ls=$_POST["dt_ls"];
				$dt=array();
				
				$sql="SELECT CONVERT(VARCHAR(20), A.vr_submit_date, 13)AS vr_date,A.vr_submit_no,C.io_number,
				CONVERT(VARCHAR(20), B.registration_date, 13)as vo_date,
				CONVERT(VARCHAR(20), D.status_transf_date, 13)as transfer_date,
				CASE 
					WHEN D.sub_cont = 'SI' THEN E.kd_si 
					WHEN D.sub_cont = 'SCI' THEN E.kd_sci 
					ELSE D.sub_cont END AS sub_cont				
				FROM vpti2_vr_submission_header A
				LEFT JOIN vpti_register B ON A.registration_number=B.registration_number
				LEFT JOIN vpti_io_header C ON B.registration_number=C.registration_number
				LEFT JOIN vpti_io_transfer D ON D.io_number=C.io_number
				LEFT JOIN ref_plb E ON C.kd_plb=E.kd_plb
				WHERE C.io_number='".$io."'";
				$dt_io=$this->db->query($sql)->getRowArray();
				$dt["io"]=$dt_io;
				//GET INFO INSPECTION
				if($io!=""){
					$sql="SELECT 
						CONVERT(VARCHAR(20), C.registration_date, 13)AS vr_sent,
						CONVERT(VARCHAR(20), B.create_date, 13)AS vr_ord_sent,
						CONVERT(VARCHAR(20), A.rfi_sent_date, 13)AS rfi_sent, 
						CONVERT(VARCHAR(20), A.rfi_received_date, 13)AS rfi_recv, 
						CONVERT(VARCHAR(20), A.req_insp_date, 6)AS req_insp, 
						CONVERT(VARCHAR(20), A.planned_insp_date, 6)AS insp_plan, 
						CONVERT(VARCHAR(20), A.final_insp_date, 13)AS final_insp, 
						CONVERT(VARCHAR(20), A.fir_received_date, 13)AS fir_recv, 
						CONVERT(VARCHAR(20), E.ls_date, 13)AS ls_draft,
						E.ls_number AS ls_no,
						CONVERT(VARCHAR(20), D.checked_date, 13)AS ls_distr,					
						D.note AS dist_to,
						D.status_code,
						F.sub_cont, CONVERT(VARCHAR(20), F.status_transf_date, 13)AS io_trsf_sln,
						A.remarks,
						CONVERT(VARCHAR(20), G.cleared_date, 13) AS cleared_date
					FROM vpti_inspection_log AS A
						LEFT JOIN vpti_io_header AS B ON B.io_number = A.io_number
						LEFT JOIN vpti_register AS C ON C.registration_number = B.registration_number 
						LEFT JOIN ls_file_monitoring AS D ON D.io_number = A.io_number AND D.partial_number = A.partial_no 
						LEFT JOIN vpti_ls_header AS E ON E.io_number = A.io_number AND E.partial_number = A.partial_no AND E.status <> 'C'
						LEFT JOIN vpti_io_transfer AS F ON F.io_number = A.io_number
						LEFT JOIN vpti2_photo_inspection_limbah AS G ON G.ls_number = E.ls_number
					 WHERE B.io_number='".$io."' AND E.partial_number=".$partial." 
					 ORDER BY E.ls_number, D.status_code DESC";
					// echo $sql;exit;
					$tf=$this->db->query($sql)->getRowArray();
					$dt["tf"]=$tf;
				}
				if($ls!=""){
					// GET PROPORMA INVOICE
					$sql="SELECT ls_id from vpti_ls_header WHERE ls_number='".$ls."'";
					$ls_id=$this->db->query($sql)->getRowArray();
					$ls_id=$ls_id["ls_id"];
					
					
					$sql="select * from (
					select  ROW_NUMBER() OVER (ORDER BY LEN(A.ls_number) , A.ls_number) as rowID,
					A.ls_number,A.partial_number,A.ls_id,A.bl_awb_no,A.invoice_no,A.pro_invoice_no,
					
					case when len(A.ls_number) = 16 then substring(A.ls_number,len(A.ls_number)-1,2)
					when len(A.ls_number) = 15 then substring(A.ls_number,len(A.ls_number),1)
					else '-'
					end as split_nya
					from vpti_ls_header A 
					WHERE io_number='".$io."' AND partial_number='".$partial."' 
					AND A.ls_number='".$ls."' 
					AND status <> 'C' AND status <> 'A'
					) as A ";
					
					$proforma=$this->db->query($sql)->getRowArray();
					$dt["proforma"]=$proforma;
					// GET INFO STATUS CODE DARI TABLE ls_file_monitoring
					$sql="SELECT A.*,CONVERT(VARCHAR(20), A.checked_date, 13)AS tgl_na,B.ls_number,A.note,B.barcode,B.discharge_port, B.issuance_place, B.status, B.ls_type   
						FROM ls_file_monitoring A 
						LEFT JOIN vpti_ls_header B ON (A.io_number=B.io_number AND A.partial_number=B.partial_number AND A.ls_id=B.ls_id)
						WHERE A.io_number='".$io."' AND A.partial_number='".$partial."' AND A.ls_id='".$ls_id."' 
						AND A.flag_ls_reprint='F' AND A.flag_NNLS='F' AND A.flag_amend='F' ";
						
					$sts_code=$this->db->query($sql)->getResultArray();
					foreach($sts_code as $x=>$v){
						if($v['status_code']=='4A'){//DRAFT LS
							$dt["draft_ls"]=$v['tgl_na'];
						}
						if((int)$v['status_code']>=2){	// BUTTON FD
							$buttonhide = '';
							if($v['status']=='I'){
								//$buttonhide = 'visibility: hidden;';											
							}
							$dt["final_fd"]='<a href="#" onClick="get_doc(\''.$io.'\',\''.$partial.'\',\''.$ls.'\',\'add\')" style="text-decoration:none;'.$buttonhide.'" id="but_na" class="but_naa" >Upload FD</span></a>';
							
							
						}
						if($v['status_code']=='6' || $v['status_code']=='5'){//FINAL LS
							if($v['status']=='I'){
								$dt["final_ls"]=array('tgl_ls'=>$v['tgl_na'],'ls_number'=>'buton');
							}else{
								$dt["final_ls"]=array('tgl_ls'=>$v['tgl_na'],'ls_number'=>$ls);
								//$progress8 = array('ck' => 1, 'status' => 'Done','group'=>'Final', 'progress' => '13. Final LS', 'date' => $v['tgl_na'], 'remark' => $v['ls_number']); 
							}
						}
						if($v['status_code']=='7'){
							$dt["ls_dis"]=array('tgl'=>$v['tgl_na'],'note'=>'Distributed to '. $v['note']);
						}
					}
					// SEND TO INATRADE
					$sql="SELECT CONVERT(VARCHAR(20), max(A.tgl_kirim), 13)as tgl_na from ls_transfer_NSW A
						LEFT JOIN ls_file_monitoring B ON A.io_number=B.io_number AND A.ls_id=B.ls_id
						WHERE A.io_number='".$io."' and B.partial_number='".$partial."' AND A.ls_id='".$ls_id."'";
					$ina=$this->db->query($sql)->getRowArray();
					$dt["ina"]=$ina;
					
				}
				
				//echo "<pre>";print_r($dt);exit;
				
				return $dt;
			break;
			case "get_vo":
				$cari=(isset($_POST["search"]["value"])? $_POST["search"]["value"] : '' );
				$where .=" AND A.importer_id = '".$this->session->get('imp_id_lama')."' AND A.commodity_code = '".$this->session->get('commodity_code')."' AND A.cancel <>'Y' ";
				if($cari!=""){
					$where .=" AND (A.io_number like '%".$cari."%' OR B.ls_number like '%".$cari."%') ";
				}
				$sql="
					SELECT TOP 100 ROW_NUMBER() OVER (ORDER BY B.io_number DESC) as rowID,A.io_number,B.partial_number,B.ls_number,B.invoice_no 
					FROM vpti_io_header A
					LEFT JOIN vpti_ls_header B ON A.io_number=B.io_number
					$where
					
				
				";
						
			
				//echo $sql;exit;
                //$dt=$this->db->query($sql)->getResultArray();
            break;
			
        }
        
        return $this->dt_grid($sql);
    }
	function simpan_data($table,$data,$get_id=""){
		$this->db->transBegin();
		
		$id = (isset($data['id']) ? $data['id'] : null);
		$field_id = "id";
		$sts_crud = $_POST['sts'];
		unset($data['sts']);
		
		//echo $this->session->get('commodity_code');exit;
		
		switch($table){
			case "ip":
				$builder = $this->db->table('vpti_ip_header');
				$data["commodity_code"]=$this->session->get('commodity_code');
				$data["importer_id"]=$this->session->get('imp_id_lama');
				//upload_single
				if(isset($_FILES['file_ip']) && $_FILES['file_ip']['name']!="")$data["file_ip_it"]=$this->upload_single("file_ip","file_ip");
				if($sts_crud=='edit' || $sts_crud=='delete'){
					$builder->where(array('commodity_code'=>$data["commodity_code"],'importer_id'=>$data["importer_id"],'ip_number'=>$data["ip_number"]));
					unset ($data["commodity_code"]);
					unset ($data["importer_id"]);
					unset ($data["ip_number"]);
					//$data["commodity_code"]
				}
				//echo "<pre>";print_r($data);exit;
				
				
				
				
			break;
			case "hs":
				//echo "<pre>";print_r($data);exit;
				
				
				if($sts_crud=='delete'){
					$dhs_hdr_id = $data["dhs_id"];
					$builder = $this->db->table('vpti2_dhs_hdr');
					$builder->where(array('dhs_id'=>$dhs_hdr_id));
					$builder->delete();
					$builder = $this->db->table('vpti2_dhs_dtl');
					$builder->where(array('dhs_hdr_id'=>$dhs_hdr_id));
					$builder->delete();
				}else{
					$builder = $this->db->table('vpti2_dhs_hdr');
					
					$dt_h=array('dhs_title'=>$data["name"],
								'dhs_date'=>date('Y-m-d H:i:s'),
								'commodity_code'=>$this->session->get('commodity_code'),
								'importer_id'=>$this->session->get('imp_id_lama'),
								'created_by'=>$this->session->get('imp_id_lama'),
								'created_at'=>date('Y-m-d H:i:s'),
					);
					//echo "<pre>";print_r($dt_h);exit;
					
					if($sts_crud=='add'){
						$builder->insert($dt_h);
						$dhs_hdr_id = $this->db->insertID();
					}else{
						$dhs_hdr_id = $data["dhs_id"];
						$builder->where(array('dhs_id'=>$dhs_hdr_id));
						$builder->update($dt_h);
						
					}
					$hs_dt=$data["hs_code"];
					$dt_detil=array();
					foreach($hs_dt as $a=>$b){
						if($b!=""){
							$dt_detil[]=array('dhs_hdr_id'=>$dhs_hdr_id,
											'hs_code'=>$b,
											'hs_description'=>$data["hs_description"][$a],
											'country_code'=>$data["country_code"][$a],
											'quantity'=>$data["quantity"][$a],
											'unit'=>$data["unit"][$a],
											'weight'=>$data["weight"][$a],
											'unit_net'=>$data["unit_net"][$a],
											'pref_facility'=>$data["pref_facility"][$a],
											'license_no'=>$data["license_no"][$a],
											'sppt_sni'=>$data["sppt_sni"][$a],
											'created_by'=>$this->session->get('imp_id_lama'),
											'created_at'=>date('Y-m-d H:i:s'),
							);
						}
					}
					
					//echo "<pre>";print_r($dt_detil);exit;
					$builder = $this->db->table('vpti2_dhs_dtl');
					$builder->where(array('dhs_hdr_id'=>$dhs_hdr_id));
					$builder->delete();
					$builder->insertBatch($dt_detil);
				
				}
				
				if ($this->db->transStatus() === false) {
					$this->db->transRollback();
					return 0;
				} else {
					return $this->db->transCommit();
				}
				
				
				
			break;
			case "supplier":
				$builder = $this->db->table('ref_supplier');
				$data["commodity_code"]=$this->session->get('commodity_code');
				$data["importer_id"]=$this->session->get('imp_id_lama');
				//$data["item_no"]=$this->session->get('imp_id_lama');
				if($sts_crud=='add'){
					$sql="SELECT ISNULL(MAX (item_no) + 1,1)as idx FROM ref_supplier WHERE commodity_code='".$data["commodity_code"]."' AND importer_id=".$data["importer_id"]." ";
					$idx=$this->db->query($sql)->getRowArray()["idx"];
					$data["item_no"]=$idx;
					
				}
				if($sts_crud=='edit' || $sts_crud=='delete'){
					$builder->where(array('commodity_code'=>$data["commodity_code"],'importer_id'=>$data["importer_id"],'item_no'=>$data["item_no"]));
					unset ($data["commodity_code"]);
					unset ($data["importer_id"]);
					unset ($data["item_no"]);
					//$data["commodity_code"]
				}
				
				
			break;
			case "exporter":
				$builder = $this->db->table('ref_exporter');
				$data["commodity_code"]=$this->session->get('commodity_code');
				$data["importer_id"]=$this->session->get('imp_id_lama');
				//$data["item_no"]=$this->session->get('imp_id_lama');
				if($sts_crud=='add'){
					$sql="SELECT ISNULL(MAX (item_no) + 1,1)as idx FROM ref_exporter WHERE commodity_code='".$data["commodity_code"]."' AND importer_id=".$data["importer_id"]." ";
					$idx=$this->db->query($sql)->getRowArray()["idx"];
					$data["item_no"]=$idx;
					
				}
				if($sts_crud=='edit' || $sts_crud=='delete'){
					//print_r($data);exit;
					$builder->where(array('commodity_code'=>$data["commodity_code"],'importer_id'=>$data["importer_id"],'item_no'=>$data["item_no"]));
					unset ($data["commodity_code"]);
					unset ($data["importer_id"]);
					unset ($data["item_no"]);
					//$data["commodity_code"]
				}
				
				
			break;
		}
		
		if($sts_crud=='add'){
			
			$builder->insert($data);
			//$this->db->insert('ref_supplier', $data);
		}else if($sts_crud=='edit'){
			//print_r($data);exit;
			$builder->update($data);	
			//echo $this->db->getLastQuery(); die;				
		}else{
			
			$builder->delete();
		}
		
		if ($this->db->transStatus() === false) {
			$this->db->transRollback();
			return 0;
		} else {
			return $this->db->transCommit();
		}
		
		
	}

	function dt_grid($sql){
		$page = (integer) $_POST['draw'];
		$start = (integer) $_POST['start'];
		$end = (integer) $_POST['length'] + $start;
		$count = $this->db->query($sql)->getNumRows();
		


		if($start >= 10) $start = $start+1;

		//echo $start .'->'.$end;exit;
		$sql = "
			SELECT * FROM (
					".$sql."
			) AS X WHERE X.rowID BETWEEN $start AND $end
		";
		//echo $sql;exit;
		$totalRecords = $count;
		$totalRecordwithFilter = $count;
		$response = array(
		  "draw" => intval($page),
		  "recordsTotal" => $totalRecords,
		  "recordsFiltered" => $totalRecordwithFilter,
		  "data" => $this->db->query($sql)->getResultArray()
		);

		return json_encode($response);
	}
	
	function upload_single($mod,$object){
		$file=date('YmdHis');
		switch($mod){
			case "file_ip": $upload_path='__repo/';$file="IP_IT_".date('YmdHis'); break;
			
		}
		
		$upload=$this->upload_nya($upload_path, $object, $file);
		if($upload){return $upload;}
		else{echo 2;exit;}
	}
	function upload_nya($upload_path="", $object="", $file=""){
		//$upload_path = "./__repository/".$folder."/";
		
		//$ext = explode('.',$_FILES[$object]['name']);
		//$exttemp = sizeof($ext) - 1;
		//$extension = $ext[$exttemp];
		$extension = pathinfo($_FILES[$object]['name'], PATHINFO_EXTENSION); 
		$filename =  $file.'.'.$extension;
		
		$files = $_FILES[$object]['name'];
		$tmp  = $_FILES[$object]['tmp_name'];
		if(!file_exists($upload_path))mkdir($upload_path, 0777, true);
		if(file_exists($upload_path.$filename)){
			unlink($upload_path.$filename);
			$uploadfile = $upload_path.$filename;
		}else{
			$uploadfile = $upload_path.$filename;
		} 
		//echo $_SERVER["DOCUMENT_ROOT"].preg_replace('@/+$@','',dirname($_SERVER['SCRIPT_NAME'])).'/'.$uploadfile;exit;
		move_uploaded_file($tmp, $uploadfile);
		
		
		return $filename;
	}
	
	function set_payment($p1,$post){
		//print_r($post);exit;
		$this->db->transBegin();
		$jml = count($post);
		$sts = 0;
		$no_nva = "";
		$msg = array();
		$builder = $this->db_pay->table('BillPay_Hd');
		$builder_keu = $this->db_keu->table('Ar_H2H_Billpay');
		if ($jml > 0) {
			$get_no_mva = $this->get_data('get_nva', 'IDR');
			
			//echo "<pre>";print_r($get_no_mva);exit;
			if (isset($get_no_mva['nva'])) {
				
				$ex = $builder->getWhere(array(
					'VptiId' => $this->session->get('COMPANY_ID'),
					'CommCd' => 'X.' . $this->session->get('commodity_code')
				))->getResultArray();
				//echo "<pre>";print_r($ex);exit;
				//$ex = $this->db_pay->get_where('H2H.dbo.BillPay_Hd', )->getResultArray();
				if (count($ex) > 0) {
					$sql = "select max(right(MvaNoId,4))+1 as id_baru
							from H2H.dbo.BillPay_Hd
							WHERE VptiId=" . $this->session->get('COMPANY_ID') . "
							AND CommCd='X." . $this->session->get('commodity_code') . "'";

					$id = $this->db_pay->query($sql)->getRowArray()["id_baru"];
					//echo "xx-> ".$id;exit;
					//$id = $qry->id_baru;
				} else {
					$id = 1;
				}

				if ($id < 10) {
					$id_baru = '0000' . $id;
				}
				if ($id < 100 && $id >= 10) {
					$id_baru = '000' . $id;
				}
				if ($id < 1000 && $id >= 100) {
					$id_baru = '00' . $id;
				}
				if ($id < 10000 && $id >= 1000) {
					$id_baru = '0' . $id;
				}
				$no_nva = $get_no_mva['nva'] . '3' . $id_baru;
				
				/*$data_header=array(	'no_nva'=>$no_nva,
									'bns_code'=>$get_no_mva['BnsCd'],
									'importer_id'=>$this->auth['COMPANY_ID'],
									'commodity_code'=>$this->auth['commodity_code'],
									'curr'=>$post['cur'],
									'total'=>$post['total'],
									'create_date'=>date('Y-m-d H:i:s'),
									'status'=>0,
									'flag'=>'I'
				);
				$this->db->insert('vpti2_billing_header',$data_header);
				$id_header=$this->db->insert_id();
				*/
				
				$tgl = date('Y-m-d H:i:s');
				//echo $tgl;exit;
				$tgl_besok = new Time('+1 day','Asia/Jakarta');
				//$datetime = new DateTime('now');
				
				//$datetime->modify('+1 day');
				//$tgl_besok = $datetime->format('Y-m-d H:i:s');
				//echo $tgl_besok;exit;
				$data_h2h_header = array(
					'VptiId' => $this->session->get('COMPANY_ID'),
					'BnsCd' => $get_no_mva['BnsCd'],
					'MvaNoId' => $no_nva,
					'CommCd' => 'X.' . $this->session->get('commodity_code'),
					// 'Curr'=>$post['cur'],
					'Curr' => 'IDR',
					'Doc_Date' => $tgl,
					'Valid_Date' => $tgl_besok,
					//'Amount'=>$post['total']
					'Amount' => $post['grand_total']
				);
				
				$builder->insert($data_h2h_header);
				$builder_keu->insert($data_h2h_header);


				//for($i=0;$i<$jml;$i++){
				$Doc_No=$post["Doc_No"];
				//echo "<pre>";print_r($Doc_No);exit;
				foreach ($Doc_No as $x=>$v) {
					//echo $post['vo_number'][$i]."<br>";
					/*$data_detil=array(
						'vpti2_billing_header_id'=>$id_header,
						'vo_number'=>$post['vo_number'][$i],
						'ls_number'=>$post['ls_number'][$i],
						'type_tagihan'=>$post['jenis_tagihan'][$i],
						'curr'=>$post['cur'],
						'total'=>$post['nilai'][$i],
						'description'=>$post['desc'][$i],
						'inv_no'=>$post['inv_no'][$i]

					);
					*/
					//echo 'x->'.$post['IdxChoice'][$x];exit;
					$data_h2h_detil = array(
						'MvaNoId' => $no_nva,
						//'Tp'=>($post['jenis_tagihan'][$i]==0 ? 'J' : 'I'),
						'Tp' => $post['TP'][$x],
						'Vo_No' => $post['Vo_No'][$x],
						'Ls_No' => (isset($post['LS'][$x])? $post['LS'][$x] : null),
						'Inv_No' =>$v,
						//'Curr'=>$post['cur'],
						'Curr' => 'IDR',
						'Amount' => $post['NILAI'][$x],
						'IdexChoise' => $post['IdxChoice'][$x],
						'Descs' => $post['DESC'][$x]
					);
					//echo "<pre>";print_r($data_h2h_detil);exit;
					$builder = $this->db_pay->table('BillPay_Dt');
					
					if ($builder->insert($data_h2h_detil)) {
						$sts = 1;
						//$this->db_pay->insert('BillPay_Dt',$data_h2h_detil);
					}
				}
			} else {
				$sts = 2;
			}
		}
		if ($this->db->transStatus() === false) {
			$this->db->transRollback();
			return 0;
		} else {
			return $this->db->transCommit();
		}
		
		
		
	}
	
}
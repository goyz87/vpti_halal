<?php

namespace App\Models;

use CodeIgniter\Model;

class Mvreq extends Model
{
    // ...
    protected $table='vpti_importer';
    protected function initialize()
    {
        $this->db = db_connect();
		$builder = $this->db->table('vpti_importer');
		$this->session = \Config\Services::session(); 
    }
    function get_data($p1,$p2="",$p3="")
    {
		$where =" WHERE 1=1 ";
        switch ($p1){
			case "dt-hs":
				return $this->db->table('ref_hsdetail')->getWhere(['commcode' => $this->session->get('commodity_code')])->getResultArray();
			break;
			case "dt-country":
				return $this->db->table('ref_country')->get()->getResultArray();
			break;
			case "hs-module":
				$sql="SELECT * FROM vpti2_vr_submission_detail WHERE vr_submit_no='".$p2."' ";
				return $this->db->query($sql)->getResultArray();
			break;
			case "vpti2_vr_doc":
				$sql="SELECT * FROM vpti2_vr_document WHERE vr_submit_no='".$p2."' 
						AND com_code='".$this->session->get('commodity_code')."' AND cl_vr_kategori_file_id=".$p3;
				//echo $sql;exit;
				return $this->db->query($sql)->getRowArray();
			break;
			case "cl_vr_kategori_file":
				$sql="SELECT A.*,CONVERT(TEXT,B.file_name)as file_name,CONVERT(VARCHAR(50), upload_date, 13) as aplod_date, B.upload_date,B.ceklist FROM cl_vr_kategori_file A
						LEFT JOIN 
							(SELECT A.*,B.ceklist,B.remark FROM vpti2_vr_document A 
							LEFT JOIN vpti2_vr_ceklist B ON B.vpti2_vr_document_id=A.id  WHERE A.vr_submit_no='".$p3."' AND com_code='".$p2."') 
						AS B ON B.cl_vr_kategori_file_id=A.id WHERE A.com_code='".$p2."'";
						//echo $sql;exit;
				return $this->db->query($sql)->getResultArray();
			break;
			case "vr_doc":
				$sql="SELECT * FROM vpti2_vr_document WHERE vr_submit_no='".$p2."'";
				//echo $sql;
				$data["res"]=$this->db->query($sql)->getResultArray();
				$data["rec"]=$this->db->query($sql)->getRowArray();
				return $data;
			break;
			case "currency":
				$sql="SELECT curr_code, upper(description) as Description FROM ref_currency ORDER by curr_code ASC";
				return $this->db->query($sql)->getResultArray();
			break;
			case "exporter":
				$cari=(isset($_POST["search"]["value"])? $_POST["search"]["value"] : '' );
				if($cari!=""){
					$where .=" AND A.name like '%".$cari."%' ";
				}
				$where .=" AND A.importer_id='".$this->session->get('imp_id_lama')."' AND A.commodity_code='".$this->session->get('commodity_code')."' ";
				$sql="SELECT ROW_NUMBER() OVER (ORDER BY A.name ASC) as rowID,A.*
						FROM ref_exporter AS A ".$where;
			break;
			case "ip":
				$cari=(isset($_POST["search"]["value"])? $_POST["search"]["value"] : '' );
				if($cari!=""){
					$where .=" AND A.ip_number like '%".$cari."%' ";
				}
				$where .=" AND A.importer_id='".$this->session->get('imp_id_lama')."' AND A.commodity_code='".$this->session->get('commodity_code')."' ";
				$sql="SELECT ROW_NUMBER() OVER (ORDER BY A.ip_number DESC) as rowID,A.*,
					CONVERT (VARCHAR, A.ip_date, 106) AS ip_date2
					FROM vpti_ip_header AS A ".$where;
			break;
            case "list_vr":
				$where .=" AND A.importer_id='".$this->session->get('imp_id_lama')."' AND A.commodity_code='".$this->session->get('commodity_code')."' ";
				$sql="SELECT  ROW_NUMBER() OVER (ORDER BY A.vr_submit_no DESC) as rowID, A.vr_submit_no, CONVERT(VARCHAR(11), A.vr_submit_date, 105)+' '+CONVERT(VARCHAR(8), 
						A.vr_submit_date, 114) AS vr_submit_date, 
						B.description AS commodity, A.exporter_name, A.exporter_contact, A.exporter_addr, A.exporter_telp,
						A.exporter_fax, A.exporter_email, A.transportation_mode,						
						REPLACE(CONVERT(VARCHAR(11), A.shipment_date, 103), '/', '-') AS shipment_date, A.invoice_no, 
						A.import_license_no, A.total_value, A.incoterm, A.flag_compare,A.flag_cek_doc,
						A.tot_net_weight, A.tot_net_weight_unit, A.[partial], A.template_file, A.supplier_name, A.supplier_contact, A.supplier_addr, 
						A.supplier_telp, A.supplier_cell, A.supplier_fax, A.supplier_email, A.supplier_country, A.io_number, A.registration_number,
						
						A.exporter_city, A.package_type, A.remarks, A.exporter_country_code, A.status, A.flag_plb, A.flag_conformity, RIGHT(A.template_file,21)as file_name, A.flag_dropbox, A.flag_kbgb
						
						FROM vpti2_vr_submission_header AS A
							LEFT JOIN vpti_importer Z ON (A.importer_id=Z.importer_id_new_table AND A.commodity_code=Z.commodity_code)
							LEFT JOIN ref_commodity_code AS B ON B.commodity_code = A.commodity_code 
							 ".$where;
				//echo $sql;exit;
               // $dt=$this->db->query($sql)->getResultArray();
			   
					if($p2=="get"){
						$sql .=" AND A.vr_submit_no='".$p3."' ";
						//echo $sql;exit;
						return $this->db->query($sql)->getRowArray(); 
					}
            break;
            case "rampok":
                $dt=$this->db->table('rampok')->get()->getRowArray();
            break;
			case "hs-module-ref":
				$cari=(isset($_POST["search"]["value"])? $_POST["search"]["value"] : '' );
				if($cari!=""){
					$where .=" AND A.dhs_title like '%".$cari."%' ";
				}
				$where .=" AND A.importer_id='".$this->session->get('imp_id_lama')."' AND A.commodity_code='".$this->session->get('commodity_code')."' ";
				$sql="SELECT ROW_NUMBER() OVER (ORDER BY A.dhs_id DESC) as rowID,A.*,
						CONVERT (VARCHAR(11), A.dhs_date, 106) + ' ' + CONVERT (VARCHAR, A.dhs_date, 8) AS dhs_date2
						FROM vpti2_dhs_hdr AS A ".$where
						;

               // $this->dt_grid($sql);
            break;
			case "supplier":
				$cari=(isset($_POST["search"]["value"])? $_POST["search"]["value"] : '' );
				if($cari!=""){
					$where .=" AND A.name like '%".$cari."%' ";
				}
				$where .=" AND A.importer_id='".$this->session->get('imp_id_lama')."' AND A.commodity_code='".$this->session->get('commodity_code')."' ";
				$sql="SELECT ROW_NUMBER() OVER (ORDER BY A.name DESC) as rowID,A.*
						FROM ref_supplier AS A ".$where;
						
				

                //$dt=$this->db->query($sql)->getResultArray();
            break;
			case "port_loading":
				$cari=(isset($_POST["search"]["value"])? $_POST["search"]["value"] : '' );
				if($cari!=""){
					$where .=" AND (A.port like '%".$cari."%' OR A.country like '%".$cari."%') ";
				}
				$sql="SELECT ROW_NUMBER() OVER (ORDER BY A.country ASC) as rowID,A.*
						FROM ref_port_code_loading AS A ".$where;
						
				

                //$dt=$this->db->query($sql)->getResultArray();
            break;
			case "port_discharge":
				$cari=(isset($_POST["search"]["value"])? $_POST["search"]["value"] : '' );
				if($cari!=""){
					$where .=" AND (A.port_name like '%".$cari."%') ";
				}
				$sql="SELECT ROW_NUMBER() OVER (ORDER BY A.port_name ASC) as rowID,A.*
						FROM ref_port_code_ind AS A ".$where;
						
				

                //$dt=$this->db->query($sql)->getResultArray();
            break;
        }
        
        return $this->dt_grid($sql,$p1);
    }
	
	function dt_grid($sql,$mod=""){
		$page = (integer) $_POST['draw'];
		$start = (integer) $_POST['start'];
		$end = (integer) $_POST['length'] + $start;
		$count = $this->db->query($sql)->getNumRows();
		


		if($start >= 10) $start = $start+1;

		//echo $start .'->'.$end;exit;
		$sql = "
			SELECT * FROM (
					".$sql."
			) AS X WHERE X.rowID BETWEEN $start AND $end
		";
		
		$totalRecords = $count;
		$totalRecordwithFilter = $count;
		$response = array(
		  "draw" => intval($page),
		  "recordsTotal" => $totalRecords,
		  "recordsFiltered" => $totalRecordwithFilter,
		);
		if($mod=="list_vr"){
			//echo "x";exit;
			$dt=$this->db->query($sql)->getResultArray();
			$sql_kat="SELECT * FROM cl_vr_kategori_file WHERE com_code='".$this->session->get('commodity_code')."' ";
			$kat=$this->db->query($sql_kat)->getResultArray();
			$icon="";
			foreach($dt as $x=>$v){
				$icon="";
				foreach($kat as $i=>$p){
					$sql_up="SELECT * FROM vpti2_vr_document WHERE vr_submit_no='".$v["vr_submit_no"]."' AND cl_vr_kategori_file_id=".$p["id"];
					$upl=$this->db->query($sql_up)->getRowArray();
					if(isset($upl["id"])){
						$icon .='<i class="ri-checkbox-circle-line align-middle text-success"></i> '.$p["kategori_file"].' <br>';
					}else{
						$icon .='<i class="ri-close-circle-line align-middle text-danger"></i></i> '.$p["kategori_file"].' <br>';
					}
					
				}
				
				$dt[$x]["upl_doc"]=$icon;
				
			}
			$response["data"]= $dt;
			
		}else{
			$response["data"]= $this->db->query($sql)->getResultArray();
		}
		

		return json_encode($response);
	}
	
	function submit_vr($data){
		$this->db->transBegin();
		$builder = $this->db->table('vpti2_vr_submission_header');
		//echo "<pre>";print_r($data);exit;
		$tgl_submit_na=date('Y-m-d');
		$sql=" EXEC COUNTER_VR '".$tgl_submit_na."'"; 
		$this->db->query($sql);
		$cntr=$this->db->table('ref_counter_vr')->getWhere(['tgl'=>$tgl_submit_na])->getRowArray();
		$nol=0;
		$tmp = date("ymd");
			if ((int)$cntr['counter'] < 10){$nol='000'.(int)$cntr['counter'];}
			else if((int)$cntr['counter'] < 100){$nol='00'.(int)$cntr['counter'];}
			else if((int)$cntr['counter'] < 1000){$nol='0'.(int)$cntr['counter'];}
			else {$nol=$cntr['counter'];}
		$vr_submit_no = $tmp.$nol;
		//ETA
		$sailingdays=0;
		$discharge_date=NULL;
		$country = substr($data['port_code'],0,2);
		$sql="SELECT * FROM sailing_days WHERE NEGARA='".$country."'";
		$data_import=$this->db->query($sql)->getRowArray();
		//echo $country;exit;
		//echo "<pre>";print_r($data_import);exit;
		//$data_import = $this->db->get_where('sailing_days',array('NEGARA'=>$country))->row();
		$shipment_date2 = date('d-m-Y',strtotime($data['shipment_date']));
		if(isset($data_import["AIR"]) && isset($data_import["SEA"])){
			if($data["transportation_mode"]=='AIR'){
				$sailingdays=$data_import["AIR"];
			}elseif($data["transportation_mode"]=='SEA'){
				$sailingdays=$data_import["SEA"];
			}else{
				$sailingdays=1;
			}
		}else{
			$sailingdays=1;
		}
		
		$discharge_date = date('Y-m-d', strtotime($shipment_date2. ' + '.$sailingdays.' days'));		
		//$discharge_date = $this->db->escape(date('Y-m-d',strtotime($discharge_date)));
		//ETA
		//echo $discharge_date;exit;
		
		$dt=array('vr_submit_no'=>$vr_submit_no,
				  'vr_submit_date'=>$tgl_submit_na,
				  'commodity_code'=>$this->session->get('commodity_code'),
				  'importer_id'=>$this->session->get('imp_id_lama'),
				  'exporter_name'=>$data["name_exporter"],
				  'exporter_contact'=>$data["contact_exporter"],
				  'exporter_addr'=>$data["address_exporter"],
				  'exporter_telp'=>$data["tlp_exporter"],
				 // 'exporter_fax'=>$data["fax_exporter"],
				  'exporter_email'=>$data["email_exporter"],
				  'transportation_mode'=>$data["transportation_mode"],
				  'port_code_load'=>$data["port_code"],
				  'port_code_discharge'=>$data["port_code_discharge"],
				  'shipment_date'=>$data["shipment_date"],
				  'discharge_date'=>$discharge_date,
				  'invoice_no'=>$data["invoice_no"],
				  'import_license_no'=>$data["import_license_no"],
				  'total_value'=>$data["total_value"],
				  'curr_code'=>$data["curr_code"],
				  'incoterm'=>$data["incoterm"],
				  'tot_net_weight'=>$data["tot_net_weight"],
				  'partial'=>$data["partial"],
				  'supplier_name'=>$data["name_supplier"],
				  'supplier_contact'=>$data["contact_supplier"],
				  'supplier_addr'=>$data["address_supplier"],
				  'supplier_telp'=>$data["phone_supplier"],
				  'supplier_cell'=>$data["mobile_phone_supplier"],
				  'supplier_email'=>$data["email_supplier"],
				//  'supplier_fax'=>$data["fax_supplier"],
				  'supplier_country'=>$data["country_supplier"],
				  'tot_net_weight_unit'=>$data["tot_net_weight_unit"],
				  'exporter_city'=>$data["exporter_city"],
				  'exporter_country_code'=>$data["country_exporter"],
				  'package_type'=>$data["package_type"],
				  'remarks'=>(isset($data["remark"]) ? $data["remark"] : null),
				  
		);
		//echo '<pre>';print_r($dt);exit;
		$builder->insert($dt);
		$sql="INSERT INTO vpti2_vr_submission_detail 
			(
			vr_submit_no,hs_code,hs_description,
			country_code,quantity,unit,weight_kgs,
			unit_net,pref_facility,license_no,
			bpom_na_number,sppt_sni
			) 
			SELECT '".$vr_submit_no."',hs_code,hs_description,
			country_code,quantity,unit,weight,
			unit_net,pref_facility,
			license_no,
			bpom_num,
			sppt_sni 
			FROM vpti2_dhs_dtl 
			WHERE dhs_hdr_id=".$data["dhs_id"];
		$this->db->query($sql);
		
		/*$sql="SELECT count(hs_code)as total
			FROM vpti2_dhs_dtl 
			WHERE dhs_hdr_id=".$this->input->post("dhs_id");
		//echo $this->db->last_query();exit;
		*/
		
		if ($this->db->transStatus() === false) {
			$this->db->transRollback();
			return 0;
		} else {
			return $this->db->transCommit();
		}
	}
	
	function getDataDetail($vr_submit_no){
        $sql = "SELECT A.*, convert(varchar, A.vr_submit_date, 23) AS vr_submit_date1, (B.port + ' ('+ B.port_code + ')' ) AS port, 
		(C.port_name + ' (' + A.port_code_discharge + ')' ) AS port_name, D.Description AS currency, 
		E.country_name AS exporter_country, CONVERT (VARCHAR, G.ip_expiry_date, 23) AS ip_expiry_date,
		CASE WHEN H.nama_lokasi IS NULL THEN J.port ELSE H.nama_lokasi END AS lok_load 
		,I.nama_lokasi as lok_disch, K.description, K.description2,
		CONVERT(VARCHAR(11), A.vr_submit_date, 103)as vr_date
                FROM vpti2_vr_submission_header A
                    LEFT JOIN ref_port_code_loading AS B ON B.port_code=A.port_code_load
                    LEFT JOIN ref_port_code_ind AS C ON C.port_code = A.port_code_discharge
                    LEFT JOIN ref_currency AS D ON D.curr_code=A.curr_code                      
					
					LEFT JOIN v_ip_imp_idnewtable G ON A.import_license_no = G.ip_number AND A.commodity_code=G.commodity_code 
					LEFT JOIN ref_lokasi AS H ON convert(varchar(10),H.kode_daerah)= A.port_code_load
					LEFT JOIN ref_lokasi AS I ON convert(varchar(10),I.kode_daerah)  = A.port_code_discharge
					LEFT JOIN ref_port_code_loading AS J ON A.port_code_load=J.port_code
					AND A.importer_id = G.importer_id_new_table
					INNER JOIN ref_commodity_code AS K ON A.commodity_code = K.commodity_code
					LEFT JOIN ref_country AS E ON E.country_code = A.exporter_country_code
                
                WHERE vr_submit_no = '$vr_submit_no'";
		//echo $sql;exit;
        return $this->db->query($sql)->getRowArray(); 	                
    }
	function getDetailSubmission($vr_submit_no){
        $sql = "SELECT A.*,B.country_name, 
				CASE 
					WHEN UPPER(A.sppt_sni) like '%PERTEK%' THEN 'PERTEK: '+ A.sppt_sni
					ELSE 'SPPT SNI:'+ A.sppt_sni 
				END AS sni_pertek,
				C.commodity_code, 
				D.unit as unitkmk,
				CASE
				WHEN A.unit <> D.unit THEN 'Cek. Satuan VR: '+A.unit+', KMK: '+D.unit
				ELSE ''
				END AS cekkmk 
                FROM vpti2_vr_submission_detail A
                    LEFT JOIN ref_country AS B ON B.country_code=A.country_code
					INNER JOIN vpti2_vr_submission_header AS C ON A.vr_submit_no = C.vr_submit_no 
					LEFT JOIN ref_hs_kmk D ON D.hs_code = A.hs_code AND D.commodity_code = C.commodity_code
                WHERE A.vr_submit_no = '".$vr_submit_no."'
				ORDER BY A.vr_submit_detail_id ASC";
				//echo $sql;exit;
        return $this->db->query($sql)->getResultArray();        
    }
	
	function hapus_doc($id_doc){
		$builder = $this->db->table('vpti2_vr_ceklist');
		$builder->where(array('vpti2_vr_document_id'=>$id_doc));
		$builder->delete();
	}
	
	function simpan_filedoc($table,$data){
		$this->db->transBegin();
		$builder = $this->db->table($table);
		$sts=$data['sts'];
		unset($data['sts']);
		$sql="SELECT * FROM vpti2_vr_submission_header WHERE vr_submit_no='".$data["vr_submit_no"]."'";
		$vr=$this->db->query($sql)->getRowArray();
		
		if($sts=='add'){
			//print_r($data);exit;
			//$sql="update  vpti2_vr_submission_header set flag_cek_doc='P' where vr_submit_no='".$data["vr_submit_no"]."'";
			//$this->db->query($sql);
			
			//add by tuanmudaheri @Sept, 09 2015
			//$vr=$this->db->get_where('vpti2_vr_submission_header',array('vr_submit_no'=>$data["vr_submit_no"]))->row_array();
			if($vr['flag_cek_doc']!='F'){
					$sql="update  vpti2_vr_submission_header set flag_cek_doc='P' where vr_submit_no='".$data["vr_submit_no"]."'";
					$this->db->query($sql);
			}
			//end
			
			$builder->insert($data);
		}
		
		else{
			
			//$vr=$this->db->get_where('vpti2_vr_submission_header',array('vr_submit_no'=>$data["vr_submit_no"]))->row_array();
			//if(isset($vr['flag_cek_doc'])){
				if($vr['flag_cek_doc']!='F'){
					$sql="update  vpti2_vr_submission_header set flag_cek_doc='P' where vr_submit_no='".$data["vr_submit_no"]."'";
					$this->db->query($sql);
				}
			//}
			
			
			/*$this->db->where(array('vr_submit_no'=>$data["vr_submit_no"],"com_code"=>$data["com_code"],
									"cl_vr_kategori_file_id"=>$data["cl_vr_kategori_file_id"]));
									
			
			
			$this->db->delete($table);
			*/
			$id=$data["id"];
			unset($data["id"]);
			$builder->where(array('id'=>$id));
			$builder->update($data);
			//$this->db->where(array('id'=>$id));
			//$this->db->update($table,$data);
			
			//$this->db->insert($table,$data);
			
		}
		
		if ($this->db->transStatus() === false) {
			$this->db->transRollback();
			return 0;
		} else {
			return $this->db->transCommit();
		}
		 
	}
	
	function edit_detil($data){
		$this->db->transBegin();
		$builder = $this->db->table('vpti2_vr_submission_detail');
		$dt_detil=array();
		$hs_dt=$data["vr_submit_detail_id"];
		foreach($hs_dt as $a=>$b){
			if($b!=""){
				$dt_detil=array('hs_code'=>$data["hs_code"][$a],
								'hs_description'=>$data["hs_description"][$a],
								'country_code'=>$data["country_code"][$a],
								'quantity'=>$data["quantity"][$a],
								'unit'=>$data["unit"][$a],
								'weight_kgs'=>$data["weight"][$a],
								'unit_net'=>$data["unit_net"][$a],
								'pref_facility'=>$data["pref_facility"][$a],
								'license_no'=>$data["license_no"][$a],
								'sppt_sni'=>$data["sppt_sni"][$a],
								//'created_by'=>$this->session->get('imp_id_lama'),
								//'created_at'=>date('Y-m-d H:i:s'),
				);
				
				//echo "<pre>";print_r($dt_detil);exit;
				$builder->where(array('vr_submit_detail_id'=>$b));
				$builder->update($dt_detil);
			}
		}
		
		
		
		if ($this->db->transStatus() === false) {
			$this->db->transRollback();
			return 0;
		} else {
			return $this->db->transCommit();
		}
	}
	
}
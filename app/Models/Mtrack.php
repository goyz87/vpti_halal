<?php

namespace App\Models;

use CodeIgniter\Model;

class Mtrack extends Model
{
    // ...
    protected $table='vpti_importer';
    protected function initialize()
    {
        $this->db = db_connect();
		$builder = $this->db->table('vpti_importer');
		$this->session = \Config\Services::session(); 
    }
    function get_data($p1,$p2="",$p3="",$p4="")
    {
		$where =" WHERE 1=1 ";
        switch ($p1){
			case "list_upload":
				$sql="SELECT * FROM vpti2_final_document WHERE id=".$p2;
				//echo $sql;exit;
				$res=$this->db->query($sql)->getRowArray();
				return $res;
			break;
			case "cl_category_file":
				$sql="SELECT * FROM cl_category_file ";
				$res=$this->db->query($sql)->getResultArray();
				return $res;
			break;
			case "get_seq_no":
				$sql="SELECT MAX(seq_no)+1 as seq_baru,partial_number from ls_file_monitoring 
						WHERE io_number='".$p2."' AND ls_id='".$p3."' GROUP BY partial_number";
				return $this->db->query($sql)->getRowArray();
			break;
			case "get_partial":
				$sql = "SELECT partial_number, CONVERT(VARCHAR, ls_date, 103) as ls_date, CASE WHEN ls_date>='2021-11-15 00:00:00' THEN 1 ELSE 0 END AS permentbaru  
						FROM vpti_ls_header
						WHERE ls_id = '".$p2."' AND ls_number='".$p3."' AND io_number='".$p4."'";
				return $this->db->query($sql)->getRowArray();
			break;
			case 'comm_desc':
				$sql = "
					SELECT UPPER(description) as deskripsi, UPPER(description2) as deskripsi2
					FROM ref_commodity_code
					WHERE commodity_code = '".$p2."'
				";
				return $this->db->query($sql)->getRowArray();
			break;
			case 'npik_io_license':
				$sql = "
					SELECT npik, CONVERT(VARCHAR(10), npik_expiry_date, 103) AS npik_expiry_date,ip_no,ip_expiry_date 
					FROM vpti_io_license
					WHERE io_number = '".$p2."'
				";
				return $this->db->query($sql)->getRowArray();
			break;
			case 'ip_header':
				$sql = "
					SELECT ip_number, CONVERT(VARCHAR(10), ip_expiry_date, 103) AS expiry_date
					FROM vpti_ip_header
					WHERE importer_id = '".$p2."' AND commodity_code = '".$p3."'
				";
				return $this->db->query($sql)->getRowArray();
			break;
			case 'importer':
				$sql = "
					SELECT A.*, B.jenis_api, B.nib, C.propinsi 
					FROM vpti_importer as A, vpti2_importer as B, ref_lokasi as C
					WHERE A.importer_id_new_table = B.id AND 
					A.region_code = C.kode_propinsi AND
					A.importer_id = '".$p2."' AND A.commodity_code = '".$p3."'
				";
				return $this->db->query($sql)->getRowArray();
			break;
			case 'exporter':
				$sql = "
					SELECT A.*, B.country_name
					FROM vpti_exporter A, ref_country B
					WHERE A.country_code = B.country_code AND 
					A.exporter_id = '".$p2."' AND A.commodity_code = '".$p3."'
				";
				return $this->db->query($sql)->getRowArray();
			break;
			case 'vr_no':
				$sql = "
					SELECT vr_submit_no
					FROM vpti2_vr_submission_header 
					WHERE io_number = '".$p2."'
				";
				return $this->db->query($sql)->getRowArray();
			break;
			case 'io_remarks':
				$sql = "
					SELECT * 
					FROM vpti_io_remarks 
					WHERE io_number = '".$p2."'
					ORDER BY seq_no ASC
				";
				return $this->db->query($sql)->getResultArray();
			break;
			case 'io_detail':
				$sql = "
					SELECT A.*, B.country_name
					FROM vpti_io_detail A 
					LEFT JOIN ref_country B ON
					A.origin=B.country_code
					WHERE A.io_number = '".$p2."'
					ORDER BY seq_no ASC
				";
				return $this->db->query($sql)->getResultArray();
			break;
			case 'io_notify_parties':
				$sql = "
					SELECT A.name as name_notifyparties, A.npwp as npwp_notifyparties, A.address as address_notifyparties
					FROM vpti_io_supplier A 
					WHERE A.io_number = '".$p2."'
				";
				return $this->db->query($sql)->getResultArray();
			break;
			case 'io_header':
				$sql = "
					SELECT A.* , CONVERT(VARCHAR(10), A.create_date, 23) AS vo_date,
						CONVERT(VARCHAR(10), A.loading_date, 23) AS load_date, 
						CONVERT(VARCHAR(10), A.discharge_date, 23) AS disch_date, 
						CONVERT(VARCHAR(10), A.pro_inv_date, 23) AS inv_date,
						CONVERT(VARCHAR(10), A.invoice_date, 23) AS invoice_date_plb,
						CONVERT(VARCHAR(10), A.bl_awb_date, 23) AS bl_date,
						A.net_weight, B.nama_plb, B.alamat_site, B.city as city_plb, 
						B.contact as contact_plb, B.phone as phone_plb, B.fax as fax_plb, B.email as email_plb, C.vr_submit_no 
					FROM vpti_io_header A 
					LEFT JOIN ref_plb B ON A.kd_plb = B.kd_plb
					INNER JOIN vpti_register C On A.registration_number = C.registration_number
					WHERE A.io_number = '".$p2."'
				";
				return $this->db->query($sql)->getRowArray();
			break;
			case "info_status":
				$io=$_POST["dt_vo"];
				$partial=$_POST["dt_partial"];
				$ls=$_POST["dt_ls"];
				$dt=array();
				
				$sql="SELECT CONVERT(VARCHAR(20), A.vr_submit_date, 13)AS vr_date,A.vr_submit_no,C.io_number,
				CONVERT(VARCHAR(20), B.registration_date, 13)as vo_date,
				CONVERT(VARCHAR(20), D.status_transf_date, 13)as transfer_date,
				CASE 
					WHEN D.sub_cont = 'SI' THEN E.kd_si 
					WHEN D.sub_cont = 'SCI' THEN E.kd_sci 
					ELSE D.sub_cont END AS sub_cont				
				FROM vpti2_vr_submission_header A
				LEFT JOIN vpti_register B ON A.registration_number=B.registration_number
				LEFT JOIN vpti_io_header C ON B.registration_number=C.registration_number
				LEFT JOIN vpti_io_transfer D ON D.io_number=C.io_number
				LEFT JOIN ref_plb E ON C.kd_plb=E.kd_plb
				WHERE C.io_number='".$io."'";
				$dt_io=$this->db->query($sql)->getRowArray();
				$dt["io"]=$dt_io;
				//GET INFO INSPECTION
				if($io!=""){
					$sql="SELECT 
						CONVERT(VARCHAR(20), C.registration_date, 13)AS vr_sent,
						CONVERT(VARCHAR(20), B.create_date, 13)AS vr_ord_sent,
						CONVERT(VARCHAR(20), A.rfi_sent_date, 13)AS rfi_sent, 
						CONVERT(VARCHAR(20), A.rfi_received_date, 13)AS rfi_recv, 
						CONVERT(VARCHAR(20), A.req_insp_date, 6)AS req_insp, 
						CONVERT(VARCHAR(20), A.planned_insp_date, 6)AS insp_plan, 
						CONVERT(VARCHAR(20), A.final_insp_date, 13)AS final_insp, 
						CONVERT(VARCHAR(20), A.fir_received_date, 13)AS fir_recv, 
						CONVERT(VARCHAR(20), E.ls_date, 13)AS ls_draft,
						E.ls_number AS ls_no,
						CONVERT(VARCHAR(20), D.checked_date, 13)AS ls_distr,					
						D.note AS dist_to,
						D.status_code,
						F.sub_cont, CONVERT(VARCHAR(20), F.status_transf_date, 13)AS io_trsf_sln,
						A.remarks,
						CONVERT(VARCHAR(20), G.cleared_date, 13) AS cleared_date
					FROM vpti_inspection_log AS A
						LEFT JOIN vpti_io_header AS B ON B.io_number = A.io_number
						LEFT JOIN vpti_register AS C ON C.registration_number = B.registration_number 
						LEFT JOIN ls_file_monitoring AS D ON D.io_number = A.io_number AND D.partial_number = A.partial_no 
						LEFT JOIN vpti_ls_header AS E ON E.io_number = A.io_number AND E.partial_number = A.partial_no AND E.status <> 'C'
						LEFT JOIN vpti_io_transfer AS F ON F.io_number = A.io_number
						LEFT JOIN vpti2_photo_inspection_limbah AS G ON G.ls_number = E.ls_number
					 WHERE B.io_number='".$io."' AND E.partial_number=".$partial." 
					 ORDER BY E.ls_number, D.status_code DESC";
					// echo $sql;exit;
					$tf=$this->db->query($sql)->getRowArray();
					$dt["tf"]=$tf;
				}
				if($ls!=""){
					// GET PROPORMA INVOICE
					$sql="SELECT ls_id from vpti_ls_header WHERE ls_number='".$ls."'";
					$ls_id=$this->db->query($sql)->getRowArray();
					$ls_id=$ls_id["ls_id"];
					$dt["ls_id"]=$ls_id;
					
					$sql="select * from (
					select  ROW_NUMBER() OVER (ORDER BY LEN(A.ls_number) , A.ls_number) as rowID,
					A.ls_number,A.partial_number,A.ls_id,A.bl_awb_no,A.invoice_no,A.pro_invoice_no,
					
					case when len(A.ls_number) = 16 then substring(A.ls_number,len(A.ls_number)-1,2)
					when len(A.ls_number) = 15 then substring(A.ls_number,len(A.ls_number),1)
					else '-'
					end as split_nya
					from vpti_ls_header A 
					WHERE io_number='".$io."' AND partial_number='".$partial."' 
					AND A.ls_number='".$ls."' 
					AND status <> 'C' AND status <> 'A'
					) as A ";
					
					$proforma=$this->db->query($sql)->getRowArray();
					$dt["proforma"]=$proforma;
					// GET INFO STATUS CODE DARI TABLE ls_file_monitoring
					$sql="SELECT A.*,CONVERT(VARCHAR(20), A.checked_date, 13)AS tgl_na,B.ls_number,A.note,B.barcode,B.discharge_port, B.issuance_place, B.status, B.ls_type   
						FROM ls_file_monitoring A 
						LEFT JOIN vpti_ls_header B ON (A.io_number=B.io_number AND A.partial_number=B.partial_number AND A.ls_id=B.ls_id)
						WHERE A.io_number='".$io."' AND A.partial_number='".$partial."' AND A.ls_id='".$ls_id."' 
						AND A.flag_ls_reprint='F' AND A.flag_NNLS='F' AND A.flag_amend='F' ";
						
					$sts_code=$this->db->query($sql)->getResultArray();
					foreach($sts_code as $x=>$v){
						if($v['status_code']=='4A'){//DRAFT LS
							$dt["draft_ls"]=$v['tgl_na'];
						}
						if((int)$v['status_code']>=2){	// BUTTON FD
							$buttonhide = '';
							if($v['status']=='I'){
								//$buttonhide = 'visibility: hidden;';											
							}
							$dt["final_fd"]='<a href="#" onClick="get_doc(\''.$io.'\',\''.$partial.'\',\''.$ls.'\',\'add\')" style="text-decoration:none;'.$buttonhide.'" id="but_na" class="btn btn-sm btn-danger" >Upload FD</span></a>';
							
							
						}
						if($v['status_code']=='6' || $v['status_code']=='5'){//FINAL LS
							if($v['status']=='I'){
								$dt["final_ls"]=array('tgl_ls'=>$v['tgl_na'],'ls_number'=>'buton');
							}else{
								$dt["final_ls"]=array('tgl_ls'=>$v['tgl_na'],'ls_number'=>$ls);
								//$progress8 = array('ck' => 1, 'status' => 'Done','group'=>'Final', 'progress' => '13. Final LS', 'date' => $v['tgl_na'], 'remark' => $v['ls_number']); 
							}
						}
						if($v['status_code']=='7'){
							$dt["ls_dis"]=array('tgl'=>$v['tgl_na'],'note'=>'Distributed to '. $v['note']);
						}
					}
					// SEND TO INATRADE
					$sql="SELECT CONVERT(VARCHAR(20), max(A.tgl_kirim), 13)as tgl_na from ls_transfer_NSW A
						LEFT JOIN ls_file_monitoring B ON A.io_number=B.io_number AND A.ls_id=B.ls_id
						WHERE A.io_number='".$io."' and B.partial_number='".$partial."' AND A.ls_id='".$ls_id."'";
					$ina=$this->db->query($sql)->getRowArray();
					$dt["ina"]=$ina;
					
					
					$sql="SELECT * FROM cl_category_file ";
					$doc=$ina=$this->db->query($sql)->getResultArray();
					$icon='';
					foreach($doc as $x=>$v){
						$sql="SELECT * FROM vpti2_final_document WHERE cl_category_file_id=".$v["id"]." AND io_number='".$io."' 
						AND partial_no='".$partial."' AND ls_number='".$ls."'";
						//echo $sql;exit;
						$upl=$this->db->query($sql)->getRowArray();
						$txt='ri-close-circle-line align-middle text-danger';
						if(isset($upl["id"])){
							$txt='ri-checkbox-circle-line align-middle text-success';
						}
						if ($v["category_file"]!= 'Nomor Pendaftaran Barang'){
							if ($v["category_file"]=='Phytosanitary'){
								if ($this->session->get('commodity_code')=='24') {
									$icon .='<i class="'.$txt.'"></i></i> '.$v["category_file"].'&nbsp;&nbsp;';
								}
							}else{
								$icon .='<i class="'.$txt.'"></i></i> '.$v["category_file"].'&nbsp;&nbsp;';
							}
						}
						
					}
					$dt["sts_fd"]=$icon;
				}
				
			//print_r($dt);exit;
				
				return $dt;
			break;
			case "get_vo":
				$io_number=$_POST["io_number"];
				if($io_number!='-'){
					$where .=" AND A.io_number='".$io_number."' ";
				}
				$cari=(isset($_POST["search"]["value"])? $_POST["search"]["value"] : '' );
				$where .=" AND A.importer_id = '".$this->session->get('imp_id_lama')."' 
							AND A.commodity_code = '".$this->session->get('commodity_code')."' 
							AND A.cancel <>'Y' 
							AND B.status <> 'C' ";
				if($cari!=""){
					$where .=" AND (A.io_number like '%".$cari."%' OR B.ls_number like '%".$cari."%') ";
				}
				$sql="
					SELECT TOP 100 ROW_NUMBER() OVER (ORDER BY B.io_number DESC) as rowID, A.io_number, B.partial_number,B.ls_id,
					B.ls_number,B.invoice_no, C.vr_submit_no 
					FROM vpti_io_header A
					LEFT JOIN vpti_ls_header B ON A.io_number=B.io_number
					LEFT JOIN vpti_register C ON A.registration_number = C.registration_number
					$where
				";
				
				
				return $this->dt_grid($sql,$p1);
            break;
			case "get_icon":
				$sql="SELECT * FROM cl_category_file ";
					$doc=$ina=$this->db->query($sql)->getResultArray();
					$icon='';
					foreach($doc as $x=>$v){
						$sql="SELECT * FROM vpti2_final_document WHERE cl_category_file_id=".$v["id"]." AND io_number='".$p2."' 
						AND partial_no='".$p3."' AND ls_number='".$p4."'";
						//echo $sql;exit;
						$upl=$this->db->query($sql)->getRowArray();
						$txt='ri-close-circle-line align-middle text-danger';
						$evn="";
						if(isset($upl["id"])){
							$txt='ri-checkbox-circle-line align-middle text-success';
							$evn='onclick="get_list('.$upl["id"].',\''.$upl["io_number"].'\',\''.$upl["cl_category_file_id"].'\',\''.$upl["partial_no"].'\',\''.$upl["ls_number"].'\')"';
						}
						if ($v["category_file"]!= 'Nomor Pendaftaran Barang'){
							if ($v["category_file"]=='Phytosanitary'){
								if ($this->session->get('commodity_code')=='24') {
									$icon .='<i class="'.$txt.'"></i></i> '.$v["category_file"].'&nbsp;&nbsp;';
								}
							}else{
								$icon .='<i class="'.$txt.'"></i></i><a href="javascript:void(0)" '.$evn.'> '.$v["category_file"].'&nbsp;&nbsp;</a>';
							}
						}
						
					}
					return $icon;
			break;
        }
        
        return $this->dt_grid($sql);
    }
	
	function simpan_data($table,$data,$get_id=""){
		$this->db->transBegin();
		
		$id = (isset($data['id']) ? $data['id'] : null);
		$field_id = "id";
		$sts_crud = $_POST['sts'];
		unset($data['sts']);
		
		//echo $this->session->get('commodity_code');exit;
		
		switch($table){
			case "ip":
				$builder = $this->db->table('vpti_ip_header');
				$data["commodity_code"]=$this->session->get('commodity_code');
				$data["importer_id"]=$this->session->get('imp_id_lama');
				//upload_single
				if(isset($_FILES['file_ip']) && $_FILES['file_ip']['name']!="")$data["file_ip_it"]=$this->upload_single("file_ip","file_ip");
				if($sts_crud=='edit' || $sts_crud=='delete'){
					$builder->where(array('commodity_code'=>$data["commodity_code"],'importer_id'=>$data["importer_id"],'ip_number'=>$data["ip_number"]));
					unset ($data["commodity_code"]);
					unset ($data["importer_id"]);
					unset ($data["ip_number"]);
					//$data["commodity_code"]
				}
				//echo "<pre>";print_r($data);exit;
			break;
			case "hs":
				//echo "<pre>";print_r($data);exit;
				
				if($sts_crud=='delete'){
					$dhs_hdr_id = $data["dhs_id"];
					$builder = $this->db->table('vpti2_dhs_hdr');
					$builder->where(array('dhs_id'=>$dhs_hdr_id));
					$builder->delete();
					$builder = $this->db->table('vpti2_dhs_dtl');
					$builder->where(array('dhs_hdr_id'=>$dhs_hdr_id));
					$builder->delete();
				}else{
					$builder = $this->db->table('vpti2_dhs_hdr');
					
					$dt_h=array('dhs_title'=>$data["name"],
								'dhs_date'=>date('Y-m-d H:i:s'),
								'commodity_code'=>$this->session->get('commodity_code'),
								'importer_id'=>$this->session->get('imp_id_lama'),
								'created_by'=>$this->session->get('imp_id_lama'),
								'created_at'=>date('Y-m-d H:i:s'),
					);
					//echo "<pre>";print_r($dt_h);exit;
					
					if($sts_crud=='add'){
						$builder->insert($dt_h);
						$dhs_hdr_id = $this->db->insertID();
					}else{
						$dhs_hdr_id = $data["dhs_id"];
						$builder->where(array('dhs_id'=>$dhs_hdr_id));
						$builder->update($dt_h);
						
					}
					$hs_dt=$data["hs_code"];
					$dt_detil=array();
					foreach($hs_dt as $a=>$b){
						if($b!=""){
							$dt_detil[]=array('dhs_hdr_id'=>$dhs_hdr_id,
											'hs_code'=>$b,
											'hs_description'=>$data["hs_description"][$a],
											'country_code'=>$data["country_code"][$a],
											'quantity'=>$data["quantity"][$a],
											'unit'=>$data["unit"][$a],
											'weight'=>$data["weight"][$a],
											'unit_net'=>$data["unit_net"][$a],
											'pref_facility'=>$data["pref_facility"][$a],
											'license_no'=>$data["license_no"][$a],
											'sppt_sni'=>$data["sppt_sni"][$a],
											'created_by'=>$this->session->get('imp_id_lama'),
											'created_at'=>date('Y-m-d H:i:s'),
							);
						}
					}
					
					//echo "<pre>";print_r($dt_detil);exit;
					$builder = $this->db->table('vpti2_dhs_dtl');
					$builder->where(array('dhs_hdr_id'=>$dhs_hdr_id));
					$builder->delete();
					$builder->insertBatch($dt_detil);
				}
				
				if ($this->db->transStatus() === false) {
					$this->db->transRollback();
					return 0;
				} else {
					return $this->db->transCommit();
				}
			break;
			case "supplier":
				$builder = $this->db->table('ref_supplier');
				$data["commodity_code"]=$this->session->get('commodity_code');
				$data["importer_id"]=$this->session->get('imp_id_lama');
				//$data["item_no"]=$this->session->get('imp_id_lama');
				if($sts_crud=='add'){
					$sql="SELECT ISNULL(MAX (item_no) + 1,1)as idx FROM ref_supplier WHERE commodity_code='".$data["commodity_code"]."' AND importer_id=".$data["importer_id"]." ";
					$idx=$this->db->query($sql)->getRowArray()["idx"];
					$data["item_no"]=$idx;
					
				}
				if($sts_crud=='edit' || $sts_crud=='delete'){
					$builder->where(array('commodity_code'=>$data["commodity_code"],'importer_id'=>$data["importer_id"],'item_no'=>$data["item_no"]));
					unset ($data["commodity_code"]);
					unset ($data["importer_id"]);
					unset ($data["item_no"]);
					//$data["commodity_code"]
				}
			break;
			case "exporter":
				$builder = $this->db->table('ref_exporter');
				$data["commodity_code"]=$this->session->get('commodity_code');
				$data["importer_id"]=$this->session->get('imp_id_lama');
				//$data["item_no"]=$this->session->get('imp_id_lama');
				if($sts_crud=='add'){
					$sql="SELECT ISNULL(MAX (item_no) + 1,1)as idx FROM ref_exporter WHERE commodity_code='".$data["commodity_code"]."' AND importer_id=".$data["importer_id"]." ";
					$idx=$this->db->query($sql)->getRowArray()["idx"];
					$data["item_no"]=$idx;
					
				}
				if($sts_crud=='edit' || $sts_crud=='delete'){
					//print_r($data);exit;
					$builder->where(array('commodity_code'=>$data["commodity_code"],'importer_id'=>$data["importer_id"],'item_no'=>$data["item_no"]));
					unset ($data["commodity_code"]);
					unset ($data["importer_id"]);
					unset ($data["item_no"]);
					//$data["commodity_code"]
				}
			break;
		}
		
		if($sts_crud=='add'){			
			$builder->insert($data);
			//$this->db->insert('ref_supplier', $data);
		}else if($sts_crud=='edit'){
			//print_r($data);exit;
			$builder->update($data);	
			//echo $this->db->getLastQuery(); die;				
		}else{			
			$builder->delete();
		}
		
		if ($this->db->transStatus() === false) {
			$this->db->transRollback();
			return 0;
		} else {
			return $this->db->transCommit();
		}
	}

	function dt_grid($sql,$mod=""){
		$page = (integer) $_POST['draw'];
		$start = (integer) $_POST['start'];
		$end = (integer) $_POST['length'] + $start;
		$count = $this->db->query($sql)->getNumRows();

		if($start >= 10) $start = $start+1;

		//echo $start .'->'.$end;exit;
		$sql = "
			SELECT * FROM (
					".$sql."
			) AS X WHERE X.rowID BETWEEN $start AND $end
		";
		//echo $sql;exit;
		$totalRecords = $count;
		$totalRecordwithFilter = $count;
		$data=$this->db->query($sql)->getResultArray();
		if($mod=='get_vo'){
			foreach($data as $x=>$v){
				$sql="SELECT MAX(seq_no) as sts
							FROM ls_file_monitoring A 
							LEFT JOIN vpti_ls_header B ON (A.io_number=B.io_number AND A.partial_number=B.partial_number AND A.ls_id=B.ls_id)
							WHERE A.io_number='".$v["io_number"]."' AND A.partial_number='".$v["partial_number"]."' AND A.ls_id='".$v["ls_id"]."' 
							AND A.flag_ls_reprint='F' AND A.flag_NNLS='F' AND A.flag_amend='F' ";
							
				$sts=$this->db->query($sql)->getRowArray()["sts"];
				//echo $sts;exit;
				$btn="";
				if((int)$sts>=2){	// BUTTON FD
					$buttonhide = '';
					$btn='<a href="#" onClick="get_doc(\''.$v["io_number"].'\',\''.$v["partial_number"].'\',\''.$v["ls_number"].'\',\'add\')" style="text-decoration:none;'.$buttonhide.'" id="but_na" class="btn btn-sm btn-danger" >Upload FD</span></a>';
					
					
				}
				$data[$x]['final_fd']=$btn;
			}
		}
		$response = array(
		  "draw" => intval($page),
		  "recordsTotal" => $totalRecords,
		  "recordsFiltered" => $totalRecordwithFilter,
		  "data" => $data
		);

		return json_encode($response);
	}
	
	function upload_single($mod,$object){
		$file=date('YmdHis');
		switch($mod){
			case "file_ip": $upload_path='__repo/';$file="IP_IT_".date('YmdHis'); break;
		}
		
		$upload=$this->upload_nya($upload_path, $object, $file);
		if($upload){return $upload;}
		else{echo 2;exit;}
	}
	function upload_nya($upload_path="", $object="", $file=""){
		//$upload_path = "./__repository/".$folder."/";
		
		//$ext = explode('.',$_FILES[$object]['name']);
		//$exttemp = sizeof($ext) - 1;
		//$extension = $ext[$exttemp];
		$extension = pathinfo($_FILES[$object]['name'], PATHINFO_EXTENSION); 
		$filename =  $file.'.'.$extension;
		
		$files = $_FILES[$object]['name'];
		$tmp  = $_FILES[$object]['tmp_name'];
		if(!file_exists($upload_path))mkdir($upload_path, 0777, true);
		if(file_exists($upload_path.$filename)){
			unlink($upload_path.$filename);
			$uploadfile = $upload_path.$filename;
		}else{
			$uploadfile = $upload_path.$filename;
		} 
		//echo $_SERVER["DOCUMENT_ROOT"].preg_replace('@/+$@','',dirname($_SERVER['SCRIPT_NAME'])).'/'.$uploadfile;exit;
		move_uploaded_file($tmp, $uploadfile);		
		
		return $filename;
	}
	
	function getrecord($type, $param1="", $param2="", $param3=""){
		switch($type){
			case "get_total_ls":
				$sql="SELECT count(A.hs_code) as total 
					FROM vpti_ls_detail A
					WHERE A.ls_id = '".$param1."' AND A.io_number = '".$param2."'
						AND ( A.flag = 0 OR A.flag IS NULL ) ";
				return $this->db->query($sql)->row("total");//$data[0];
			break;
			case 'ls_detail_dev':
				$sql = "
					SELECT * FROM(
						SELECT ROW_NUMBER() OVER (ORDER BY A.seq_no ASC) as rowID,A.*, 
											SUBSTRING(A.hs_code, 1, 4) as hscode1,
											SUBSTRING(A.hs_code, 5, 2) as hscode2,
											SUBSTRING(A.hs_code, 7, 2) as hscode3,
											SUBSTRING(A.hs_code, 9, 2) as hscode4,					
											B.country_name
										FROM vpti_ls_detail A
										LEFT JOIN ref_country B ON A.origin = B.country_code
										WHERE A.ls_id = '".$param1."' AND A.io_number = '".$param2."'
											AND ( A.flag = 0 OR A.flag IS NULL )
					)X WHERE X.rowID BETWEEN ".$this->input->post('awal')." AND ".$this->input->post('akhir')."
				";
				//echo $sql;exit; 
				return $this->db->query($sql)->getResultArray();//$data[0];
			break;
			case 'ls_header' :
				$sql = "
					SELECT  DISTINCT   vpti_ls_header.io_number, vpti_ls_header.ls_id, vpti_ls_header.ls_number, vpti_ls_header.ls_date, vpti_ls_header.npwp, vpti_ls_header.importer_type,
								vpti_ls_header.importer_name, vpti_ls_header.importer_address, vpti_ls_header.importer_address1, vpti_ls_header.importer_city, vpti_ls_header.importer_zipcode,
								vpti_ls_header.importer_api, vpti_ls_header.ip_no, 
								vpti_ls_header.ip_expiry_date , vpti_ls_header.bmtb_object, vpti_ls_header.bmtb_poi, vpti_ls_header.bmtb_result, 
								FORMAT(vpti_ls_header.ls_date, 'dd-MMM-yyyy') AS tgl_ls, 
								YEAR(vpti_ls_header.ip_expiry_date) ip_expiry_date_year ,
								vpti_ls_header.npik, vpti_ls_header.npik_expiry_date,
								vpti_ls_header.exporter_name, vpti_ls_header.exporter_address, vpti_ls_header.exporter_city, vpti_ls_header.transportation_method,
								vpti_ls_header.transporter_name, vpti_ls_header.flight_no, vpti_ls_header.loading_port, vpti_ls_header.transit_port, vpti_ls_header.discharge_port,
								vpti_ls_header.invoice_no, vpti_ls_header.invoice_date, vpti_ls_header.lc_no, vpti_ls_header.lc_date, vpti_ls_header.bl_awb_no, vpti_ls_header.bl_awb_date,
								vpti_ls_header.gross_weight, vpti_ls_header.net_weight, vpti_ls_header.unit, vpti_ls_header.djbc_code, vpti_ls_header.inspection_date,
								vpti_ls_header.partial_shipment, vpti_ls_header.partial_number, vpti_io_header.registration_number, ref_country.country_name, ref_city_abroad.city_name,
								vpti_ls_sec_code.sc_code, vpti_ls_header.flag_pdf, ref_plb.nama_plb, vpti_ls_header.kd_plb, vpti_io_header.flag_plb,
								ref_lokasi.propinsi, vpti_ls_header.status, vpti_io_header.flag_kbgb, vpti_io_header.pdkb_no, ref_plb.alamat_site, ref_plb.city as city_plb,
								vpti_io_header.pdkb_kota, vpti_io_header.pdkb_alamat, CASE WHEN vpti_ls_header.ls_date>='2021-11-15 00:00:00' THEN 1 ELSE 0 END AS permentbaru,
								vpti_ls_header.currency, vpti_ls_header.fob_per_invoice, vpti2_importer.nib, FORMAT(vpti2_importer.nib_date, 'dd-MMM-yyyy') AS nib_date,
								ref_country2.country_name as loading_country_name, FORMAT(vpti_ip_header.ip_date, 'dd-MMM-yyyy') AS ip_date, pro_invoice_no, pro_invoice_date  
					FROM         vpti_ls_header AS vpti_ls_header 
						LEFT JOIN ref_country AS ref_country ON vpti_ls_header.exporter_country = ref_country.country_code 
						LEFT JOIN ref_city_abroad AS ref_city_abroad ON vpti_ls_header.inspection_place = ref_city_abroad.code 
						INNER JOIN vpti_io_header AS vpti_io_header ON vpti_ls_header.io_number = vpti_io_header.io_number 
						LEFT OUTER JOIN vpti_ls_sec_code AS vpti_ls_sec_code ON vpti_ls_header.io_number = vpti_ls_sec_code.io_number AND vpti_ls_header.ls_number = vpti_ls_sec_code.ls_number AND vpti_ls_header.ls_id = vpti_ls_sec_code.ls_id
						LEFT JOIN ref_plb AS ref_plb ON vpti_ls_header.kd_plb = ref_plb.kd_plb
						LEFT JOIN vpti_importer as vpti_importer ON vpti_io_header.importer_id=vpti_importer.importer_id And vpti_io_header.commodity_code=vpti_importer.commodity_code
						LEFT JOIN ref_lokasi AS ref_lokasi ON vpti_importer.region_code=ref_lokasi.kode_propinsi
						LEFT JOIN vpti2_importer AS vpti2_importer ON vpti_importer.importer_id_new_table=vpti2_importer.id
						LEFT JOIN ref_country AS ref_country2 ON SUBSTRING(vpti_ls_header.loading_country, 1, 2) = ref_country2.country_code
						LEFT JOIN vpti_ip_header AS vpti_ip_header ON vpti_ip_header.ip_number = vpti_ls_header.ip_no						
					WHERE  (vpti_ls_header.ls_number = '".$param1."')
					ORDER BY vpti_ls_header.ls_id				
				";
								
				//$data = $this->db->query($sql)->result_array(); 
				return $this->db->query($sql)->getResultArray();//$data[0];
			break;
			case 'ls_header_histori' :
				$sql = "
					SELECT  DISTINCT   vpti_ls_header_amendhistory.io_number, vpti_ls_header_amendhistory.ls_id, vpti_ls_header_amendhistory.ls_number, vpti_ls_header_amendhistory.ls_date, vpti_ls_header_amendhistory.npwp, vpti_ls_header_amendhistory.importer_type,
								vpti_ls_header_amendhistory.importer_name, vpti_ls_header_amendhistory.importer_address, vpti_ls_header_amendhistory.importer_address1, vpti_ls_header_amendhistory.importer_city, vpti_ls_header_amendhistory.importer_zipcode,
								vpti_ls_header_amendhistory.importer_api, vpti_ls_header_amendhistory.ip_no, 
								vpti_ls_header_amendhistory.ip_expiry_date ,
								FORMAT(vpti_ls_header_amendhistory.ls_date, 'dd-MMM-yyyy') AS tgl_ls,
								YEAR(vpti_ls_header_amendhistory.ip_expiry_date) ip_expiry_date_year ,
								vpti_ls_header_amendhistory.npik, vpti_ls_header_amendhistory.npik_expiry_date,
								vpti_ls_header_amendhistory.exporter_name, vpti_ls_header_amendhistory.exporter_address, vpti_ls_header_amendhistory.exporter_city, vpti_ls_header_amendhistory.transportation_method,
								vpti_ls_header_amendhistory.transporter_name, vpti_ls_header_amendhistory.flight_no, vpti_ls_header_amendhistory.loading_port, vpti_ls_header_amendhistory.transit_port, vpti_ls_header_amendhistory.discharge_port,
								vpti_ls_header_amendhistory.invoice_no, vpti_ls_header_amendhistory.invoice_date, vpti_ls_header_amendhistory.lc_no, vpti_ls_header_amendhistory.lc_date, vpti_ls_header_amendhistory.bl_awb_no, vpti_ls_header_amendhistory.bl_awb_date,
								vpti_ls_header_amendhistory.gross_weight, vpti_ls_header_amendhistory.net_weight, vpti_ls_header_amendhistory.unit, vpti_ls_header_amendhistory.djbc_code, vpti_ls_header_amendhistory.inspection_date,
								vpti_ls_header_amendhistory.partial_shipment, vpti_ls_header_amendhistory.partial_number, vpti_io_header.registration_number, ref_country.country_name, ref_city_abroad.city_name,
								vpti_ls_sec_code.sc_code, vpti_ls_header_amendhistory.flag_pdf, ref_plb.nama_plb, vpti_ls_header_amendhistory.kd_plb, vpti_io_header.flag_plb,
								ref_lokasi.propinsi, vpti_ls_header_amendhistory.status, vpti_io_header.flag_kbgb, vpti_io_header.pdkb_no, ref_plb.alamat_site, ref_plb.city as city_plb, vpti_io_header.pdkb_kota, vpti_io_header.pdkb_alamat  	
					FROM         vpti_ls_header_amendhistory AS vpti_ls_header_amendhistory 
						INNER JOIN ref_country AS ref_country ON vpti_ls_header_amendhistory.exporter_country = ref_country.country_code 
						LEFT JOIN ref_city_abroad AS ref_city_abroad ON vpti_ls_header_amendhistory.inspection_place = ref_city_abroad.code 
						INNER JOIN vpti_io_header AS vpti_io_header ON vpti_ls_header_amendhistory.io_number = vpti_io_header.io_number 
						LEFT OUTER JOIN vpti_ls_sec_code AS vpti_ls_sec_code ON vpti_ls_header_amendhistory.io_number = vpti_ls_sec_code.io_number AND vpti_ls_header_amendhistory.ls_number = vpti_ls_sec_code.ls_number AND vpti_ls_header_amendhistory.ls_id = vpti_ls_sec_code.ls_id
						LEFT JOIN ref_plb AS ref_plb ON vpti_ls_header_amendhistory.kd_plb = ref_plb.kd_plb
						LEFT JOIN vpti_importer as vpti_importer ON vpti_io_header.importer_id=vpti_importer.importer_id And vpti_io_header.commodity_code=vpti_importer.commodity_code
						LEFT JOIN ref_lokasi AS ref_lokasi ON vpti_importer.region_code=ref_lokasi.kode_propinsi
					WHERE  (vpti_ls_header_amendhistory.ls_number = '".$param1."')
					AND vpti_ls_header_amendhistory.id_amend= (SELECT MIN(id_amend) FROM vpti_ls_header_amendhistory WHERE ls_number = '".$param1."')
					ORDER BY vpti_ls_header_amendhistory.ls_id					
				";
								
				//$data = $this->db->query($sql)->result_array(); 
				return $this->db->query($sql)->getResultArray();//$data[0];
			break;
			case 'ls_package':
				$sql = "
					SELECT A.*
					FROM vpti_ls_package A
					WHERE A.ls_id = '".$param1."' AND A.io_number = '".$param2."'
				";
				return $this->db->query($sql)->getResultArray();//$data[0];
			break;
			case 'ls_package_histori':
				$sql = "
					SELECT A.*
					FROM vpti_ls_package_amendhistory A
					WHERE A.ls_id = '".$param1."' AND A.io_number = '".$param2."'
					AND A.id_amend= (SELECT MIN(id_amend) FROM vpti_ls_package_amendhistory WHERE ls_id = '".$param1."' AND io_number = '".$param2."')
				";
				return $this->db->query($sql)->getResultArray();//$data[0];
			break;
			case 'ls_remarks':
				$sql = "
					SELECT A.*
					FROM vpti_ls_remarks A
					WHERE A.ls_id = '".$param1."' AND A.io_number = '".$param2."'
					ORDER BY A.seq_no ASC
				";
				return $this->db->query($sql)->getResultArray();//$data[0];
			break;
			case 'ls_remarks_histori':
				$sql = "
					SELECT A.*
					FROM vpti_ls_remarks_amendhistory A
					WHERE A.ls_id = '".$param1."' AND A.io_number = '".$param2."'
					AND A.id_amend= (SELECT MIN(id_amend) FROM vpti_ls_remarks_amendhistory WHERE ls_id = '".$param1."' AND io_number = '".$param2."')
					ORDER BY A.seq_no ASC
				";
				return $this->db->query($sql)->getResultArray();//$data[0];
			break;
			case 'ls_remarks_detail':
				$sql = "
					SELECT A.*
					FROM vpti_ls_remarks_detail A
					WHERE A.ls_id = '".$param1."' AND A.io_number = '".$param2."'
					ORDER BY A.container_seq_no ASC
				";
				return $this->db->query($sql)->getResultArray();//$data[0];
			break;
			case 'ls_remarks_detail_histori':
				$sql = "
					SELECT A.*
					FROM vpti_ls_remarks_detail_amendhistory A
					WHERE A.ls_id = '".$param1."' AND A.io_number = '".$param2."'
					AND A.id_amend= (SELECT MIN(id_amend) FROM vpti_ls_remarks_detail_amendhistory WHERE ls_id = '".$param1."' AND io_number = '".$param2."')
					ORDER BY A.container_seq_no ASC
				";
				return $this->db->query($sql)->getResultArray();//$data[0];
			break;
			case "get_total_ls":
				$sql="SELECT count(A.hs_code) as total 
					FROM vpti_ls_detail A
					WHERE A.ls_id = '".$param1."' AND A.io_number = '".$param2."'
						AND ( A.flag = 0 OR A.flag IS NULL ) ";
				return $this->db->query($sql)->row("total");//$data[0];
			break;
			case 'ls_detail_dev':
				$sql = "
					SELECT * FROM(
						SELECT ROW_NUMBER() OVER (ORDER BY A.seq_no ASC) as rowID,A.*, 
											SUBSTRING(A.hs_code, 1, 4) as hscode1,
											SUBSTRING(A.hs_code, 5, 2) as hscode2,
											SUBSTRING(A.hs_code, 7, 2) as hscode3,
											SUBSTRING(A.hs_code, 9, 2) as hscode4,					
											B.country_name
										FROM vpti_ls_detail A
										LEFT JOIN ref_country B ON A.origin = B.country_code
										WHERE A.ls_id = '".$param1."' AND A.io_number = '".$param2."'
											AND ( A.flag = 0 OR A.flag IS NULL )
					)X WHERE X.rowID BETWEEN ".$this->input->post('awal')." AND ".$this->input->post('akhir')."
				";
				//echo $sql;exit; 
				return $this->db->query($sql)->getResultArray();//$data[0];
			break;
			case 'ls_detail':
				$sql = "
					SELECT A.*, 
						SUBSTRING(A.hs_code, 1, 4) as hscode1,
						SUBSTRING(A.hs_code, 5, 2) as hscode2,
						SUBSTRING(A.hs_code, 7, 2) as hscode3,
						SUBSTRING(A.hs_code, 9, 2) as hscode4,					
						B.country_name
					FROM vpti_ls_detail A
					LEFT JOIN ref_country B ON A.origin = B.country_code
					WHERE A.ls_id = '".$param1."' AND A.io_number = '".$param2."'
						AND ( A.flag = 0 OR A.flag IS NULL )
					ORDER BY A.seq_no ASC
				";
				//echo $sql;exit; 
				return $this->db->query($sql)->getResultArray();//$data[0];
			break;
			case 'ls_detail_histori':
				$sql = "
					SELECT A.*, 
						SUBSTRING(A.hs_code, 1, 4) as hscode1,
						SUBSTRING(A.hs_code, 5, 2) as hscode2,
						SUBSTRING(A.hs_code, 7, 2) as hscode3,
						SUBSTRING(A.hs_code, 9, 2) as hscode4,					
						B.country_name
					FROM vpti_ls_detail_amendhistory A
					LEFT JOIN ref_country B ON A.origin = B.country_code
					WHERE A.ls_id = '".$param1."' AND A.io_number = '".$param2."'
						AND ( A.flag = 0 OR A.flag IS NULL )
						AND A.id_amend= (SELECT MIN(id_amend) FROM vpti_ls_detail_amendhistory WHERE ls_id = '".$param1."' AND io_number = '".$param2."')
					ORDER BY A.seq_no ASC
				";
				//echo $sql;exit; 
				return $this->db->query($sql)->getResultArray();//$data[0];
			break;
			case 'ls_detail_lampiran':
				$sql = "
					SELECT A.*, 
						SUBSTRING(A.hs_code, 1, 4) as hscode1,
						SUBSTRING(A.hs_code, 5, 2) as hscode2,
						SUBSTRING(A.hs_code, 7, 2) as hscode3,
						SUBSTRING(A.hs_code, 9, 2) as hscode4,					
						B.country_name
					FROM vpti_ls_detail A
					LEFT JOIN ref_country B ON A.origin = B.country_code
					WHERE A.ls_id = '".$param1."' AND A.io_number = '".$param2."' 
								AND A.flag = 1 
					ORDER BY A.seq_no ASC
				";
				return $this->db->query($sql)->getResultArray();//$data[0];
			break;
			case 'ls_verificator':
				/*$sql = "
					SELECT A.checked_by_user
					FROM ls_file_monitoring A
					WHERE A.ls_id = '".$param1."' AND A.io_number = '".$param2."' AND A.status_code = '6'
				";*/
				$sql = "
					SELECT top 1 B.inisial
					FROM ls_file_monitoring A, ref_users_inisial B
					WHERE A.checked_by_user = B.user_name AND 
					A.flag_ls_reprint ='F' AND A.flag_NNLS='F' AND A.flag_amend='F' AND 
					A.ls_id = '".$param1."' AND A.io_number = '".$param2."' AND A.status_code = '4B'
				";
				//echo $sql;exit;
				return $this->db->query($sql)->getRowArray();				
			break;
			case 'commodity_desc':
				$sql = "
					SELECT A.description, description2 
					FROM ref_commodity_code A
					WHERE A.commodity_code = '".$param1."'
				";
				return $this->db->query($sql)->getRowArray();
			break;
			case "ksocode":
				$sql = "
					SELECT sc_code
					FROM vpti_ls_sec_code
					WHERE ls_id = '".$param1."' AND io_number = '".$param2."'
				";
				return $this->db->query($sql)->getRowArray();
			break;
		}
	}
	function logdownload_els($comm_code, $imp_id_newtable, $ls){
		$sql = "SELECT commodity_code
				FROM vpti2_download_els
				WHERE commodity_code = '".$comm_code."' AND importer_id_new_table='".$imp_id_newtable."' 
				AND ls_number='".$ls."'";

		return $this->db->query($sql)->getRowArray();
	}
	function simpan_log_els($table,$data){
		$this->db->transBegin();
		$builder = $this->db->table($table);
		$data["download_date"]=date('Y-m-d H:i:s');
		$builder->insert($data);
		
		if ($this->db->transStatus() === false) {
			$this->db->transRollback();
			return 0;
		} else {
			return $this->db->transCommit();
		}
	}
	function simpan_monitoring($table,$data){
		$this->db->transBegin();
		$builder = $this->db->table($table);
		$builder->insert($data);
		
		if ($this->db->transStatus() === false) {
			$this->db->transRollback();
			return 0;
		} else {
			return $this->db->transCommit();
		}
	}
	
	function simpan_filedoc($table,$data){		
		$this->db->transBegin();
		$sts=$data["sts"];
		
		unset($data["sts"]);
		$builder = $this->db->table($table);
		
		if($sts=='add'){
			//echo "<pre>";print_r($data);exit;
			$builder->insert($data);			
		}else{
			$id=$data["id"];
			unset($data["id"]);
			$data["update_date"]=date('Y-m-d H:i:s');
			$data["flag_cek"]='P';
			$builder->where(array('id'=>$id));
			$builder->update($data);
		}
		
		if ($this->db->transStatus() === false) {
			$this->db->transRollback();
			return 0;
		} else {
			return $this->db->transCommit();
		}
	}
}
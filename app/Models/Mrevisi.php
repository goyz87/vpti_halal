<?php

namespace App\Models;

use CodeIgniter\Model;

class Mrevisi extends Model
{
    // ...
    protected $table='vpti_importer';
    protected function initialize()
    {
        $this->db = db_connect();
		$builder = $this->db->table('vpti_importer');
		$this->session = \Config\Services::session(); 
    }
    function get_data($p1,$p2="")
    {
		$where =" WHERE 1=1 ";
        switch ($p1){
			case "list_revisi_vo":
				$cari=(isset($_POST["search"]["value"])? $_POST["search"]["value"] : '' );
				if($cari!=""){
					$where .=" AND (A.io_number like '%".$cari."%' OR A.vr_submit_no like '%".$cari."%') ";
				}
				$sql="SELECT ROW_NUMBER() OVER (ORDER BY A.create_date DESC) as rowID ,A.* 
						FROM vpti2_revisi_VR_VO A 
						".$where." AND A.importer_id=".$this->session->get('COMPANY_ID')."
						AND A.commodity_code='".$this->session->get('commodity_code')."' ";
						
				if($p2=='edit'){
					$sql .= " AND A.id=".$_POST['id'];
					return $this->db->query($sql)->getRowArray();
				}
				//$sql .=" ORDER BY A.create_date DESC ";

                //$dt=$this->db->query($sql)->getResultArray();
            break;
			case "list_revisi_ls":
				$cari=(isset($_POST["search"]["value"])? $_POST["search"]["value"] : '' );
				if($cari!=""){
					$where .=" AND (A.io_number like '%".$cari."%' OR A.vr_submit_no like '%".$cari."%') ";
				}
				$sql="SELECT ROW_NUMBER() OVER (ORDER BY A.create_date DESC) as rowID ,A.* 
						FROM vpti2_revisi_LS A 
						".$where." AND A.importer_id=".$this->session->get('COMPANY_ID')."
						AND A.commodity_code='".$this->session->get('commodity_code')."' ";
						
				if($p2=='edit'){
					$sql .= " AND A.id=".$_POST['id'];
					return $this->db->query($sql)->getRowArray();
				}
				

                //$dt=$this->db->query($sql)->getResultArray();
            break;
			case "dt_vo":
				$sql="SELECT io_number
					FROM vpti_io_header 
					WHERE importer_id=".$this->session->get('imp_id_lama')." AND commodity_code='".$this->session->get('commodity_code')."' ";
					
				$dt=$this->db->query($sql)->getResultArray();
				return $dt;
				/*$dt_js=array();
				$js=array();
				foreach($dt as $x=>$v){
					$js[]=array('id'=>$v["io_number"],'text'=>$v["io_number"]);
				}
				$dt_js=array('results'=>$js,'pagination'=>false);
				return json_encode($dt_js);
				*/
			break;
			case "dt_ls":
				$sql="SELECT A.ls_number
					FROM vpti_ls_header A 
					LEFT JOIN vpti_io_header B ON A.io_number=B.io_number
					WHERE B.importer_id=".$this->session->get('imp_id_lama')." 
					AND B.commodity_code='".$this->session->get('commodity_code')."' AND A.io_number='".$_POST["io_number"]."'";
					
				$dt=$this->db->query($sql)->getResultArray();
				//return $dt;
				$dt_js=array();
				$js=array();
				foreach($dt as $x=>$v){
					$select=false;
					if(isset($_POST["ls_number"])){
						if($_POST["ls_number"]==$v["ls_number"]){
							$select=true;
						}
					}
					
					$js[]=array('id'=>$v["ls_number"],'text'=>$v["ls_number"],"selected"=>$select);
				}
				//$dt_js=array('results'=>$js,'pagination'=>false);
				$dt_js=$js;
				return json_encode($dt_js);
				
			break;
			case "dt_partial":
				$sql="SELECT A.partial_number
					FROM vpti_ls_header A 
					LEFT JOIN vpti_io_header B ON A.io_number=B.io_number
					WHERE B.importer_id=".$this->session->get('imp_id_lama')." 
					AND B.commodity_code='".$this->session->get('commodity_code')."' AND A.ls_number='".$_POST["ls_number"]."'";
					
				$dt=$this->db->query($sql)->getResultArray();
				//return $dt;
				$dt_js=array();
				$js=array();
				foreach($dt as $x=>$v){
					$select=false;
					if(isset($_POST["partial"])){
						if($_POST["partial"]==$v["partial_number"]){
							$select=true;
						}
					}
					$js[]=array('id'=>$v["partial_number"],'text'=>$v["partial_number"],"selected"=>$select);
				}
				//$dt_js=array('results'=>$js,'pagination'=>false);
				$dt_js=$js;
				return json_encode($dt_js);
				
			break;
        }
        
        return $this->dt_grid($sql);
    }
	function simpan_data($table,$data,$get_id=""){
		$this->db->transBegin();
		
		$id = (isset($data['id']) ? $data['id'] : null);
		$field_id = "id";
		$sts_crud = $_POST['sts'];
		unset($data['sts']);
		
		//echo $this->session->get('commodity_code');exit;
		
		switch($table){
			case "ip":
				$builder = $this->db->table('vpti_ip_header');
				$data["commodity_code"]=$this->session->get('commodity_code');
				$data["importer_id"]=$this->session->get('imp_id_lama');
				//upload_single
				if(isset($_FILES['file_ip']) && $_FILES['file_ip']['name']!="")$data["file_ip_it"]=$this->upload_single("file_ip","file_ip");
				if($sts_crud=='edit' || $sts_crud=='delete'){
					$builder->where(array('commodity_code'=>$data["commodity_code"],'importer_id'=>$data["importer_id"],'ip_number'=>$data["ip_number"]));
					unset ($data["commodity_code"]);
					unset ($data["importer_id"]);
					unset ($data["ip_number"]);
					//$data["commodity_code"]
				}
				//echo "<pre>";print_r($data);exit;
				
				
				
				
			break;
			case "vo":
				//echo "<pre>";print_r($_FILES);exit;
				//echo "<pre>";print_r($data);exit;
				$builder = $this->db->table('vpti2_revisi_VR_VO');
				$data["commodity_code"]=$this->session->get('commodity_code');
				$data["importer_id"]=$this->session->get('COMPANY_ID');
				if(isset($_FILES['file_revvo']) && $_FILES['file_revvo']['name']!="")$data["dok_attach"]=$this->upload_single("file_revvo","file_revvo");
				if($sts_crud=='edit' || $sts_crud=='delete'){
					$builder->where(array('id'=>$id));
					unset($data['id']);
					$data["update_date"]=date('Y-m-d H:i:s');
					
				}
				if($sts_crud=='add'){
					$data["create_date"]=date('Y-m-d H:i:s');
				}
			break;
			case "ls":
				//echo "<pre>";print_r($_FILES);exit;
				//echo "<pre>";print_r($data);exit;
				$builder = $this->db->table('vpti2_revisi_LS');
				$data["commodity_code"]=$this->session->get('commodity_code');
				$data["importer_id"]=$this->session->get('COMPANY_ID');
				if(isset($_FILES['file_revls']) && $_FILES['file_revls']['name']!="")$data["dok_attach"]=$this->upload_single("file_revls","file_revls");
				if($sts_crud=='edit' || $sts_crud=='delete'){
					$builder->where(array('id'=>$id));
					unset($data['id']);
					$data["update_date"]=date('Y-m-d H:i:s');
					
				}
				if($sts_crud=='add'){
					$data["create_date"]=date('Y-m-d H:i:s');
				}
			break;
			
		}
		
		if($sts_crud=='add'){
			
			$builder->insert($data);
			//$this->db->insert('ref_supplier', $data);
		}else if($sts_crud=='edit'){
			//print_r($data);exit;
			$builder->update($data);	
			//echo $this->db->getLastQuery(); die;				
		}else{
			
			$builder->delete();
		}
		
		if ($this->db->transStatus() === false) {
			$this->db->transRollback();
			return 0;
		} else {
			return $this->db->transCommit();
		}
		
		
	}

	function dt_grid($sql){
		$page = (integer) $_POST['draw'];
		$start = (integer) $_POST['start'];
		$end = (integer) $_POST['length'] + $start;
		$count = $this->db->query($sql)->getNumRows();
		


		if($start >= 10) $start = $start+1;

		//echo $start .'->'.$end;exit;
		$sql = "
			SELECT * FROM (
					".$sql."
			) AS X WHERE X.rowID BETWEEN $start AND $end
		";
		
		$totalRecords = $count;
		$totalRecordwithFilter = $count;
		$response = array(
		  "draw" => intval($page),
		  "recordsTotal" => $totalRecords,
		  "recordsFiltered" => $totalRecordwithFilter,
		  "data" => $this->db->query($sql)->getResultArray()
		);

		return json_encode($response);
	}
	
	function upload_single($mod,$object){ 
		$file=date('YmdHis');
		switch($mod){
			case "file_revls": $upload_path='__repo/Revisi_ls/';$file=date('YmdHis'); break;
			case "file_revvo": $upload_path='__repo/Revisi_vo/';$file=date('YmdHis'); break;
			
		}
		
		$upload=$this->upload_nya($upload_path, $object, $file);
		if($upload){return $upload;}
		else{echo 2;exit;}
	}
	function upload_nya($upload_path="", $object="", $file=""){
		//$upload_path = "./__repository/".$folder."/";
		
		//$ext = explode('.',$_FILES[$object]['name']);
		//$exttemp = sizeof($ext) - 1;
		//$extension = $ext[$exttemp];
		$extension = pathinfo($_FILES[$object]['name'], PATHINFO_EXTENSION); 
		$filename =  $file.'.'.$extension;
		
		$files = $_FILES[$object]['name'];
		$tmp  = $_FILES[$object]['tmp_name'];
		if(!file_exists($upload_path))mkdir($upload_path, 0777, true);
		if(file_exists($upload_path.$filename)){
			unlink($upload_path.$filename);
			$uploadfile = $upload_path.$filename;
		}else{
			$uploadfile = $upload_path.$filename;
		} 
		//echo $_SERVER["DOCUMENT_ROOT"].preg_replace('@/+$@','',dirname($_SERVER['SCRIPT_NAME'])).'/'.$uploadfile;exit;
		move_uploaded_file($tmp, $uploadfile);
		
		
		return $filename;
	}
	
}
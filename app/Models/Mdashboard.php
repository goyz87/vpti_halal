<?php

namespace App\Models;

use CodeIgniter\Model;

class Mdashboard extends Model
{
    // ...
    protected $table='tbl_user';
    protected function initialize()
    {
        $this->db = db_connect();
		$this->session = \Config\Services::session(); 
    }
    function get_data($p1)
    {
        switch ($p1){
           case 'linechart':
				$thn1=(int)date('Y');
				$thn2=(int)date('Y')-1;
				//echo $thn1.'->'.$thn2;
				$dt=array();
				$idx1=0;
				$idx2=0;
				$tr="";
				for ($i=$thn2;$i<=$thn1; $i++ ){
					$dt[$idx1]["name"]=$i;
					$dtil=array();
					for($a=1;$a<=12;$a++){
						$sql ="SELECT  
								CAST(SUM(b.fob_value_curr * e.KURS) as numeric)  AS value_usd
						FROM    vpti_ls_header a
								INNER JOIN vpti_ls_detail b ON a.io_number = b.io_number
															AND a.ls_id = b.ls_id
								INNER JOIN vpti_io_header c ON a.io_number = c.io_number
								INNER JOIN vpti_importer d ON c.commodity_code = d.commodity_code
															AND c.importer_id = d.importer_id
								INNER JOIN kurs e ON b.currency = e.MTU
						WHERE   a.status = 'I'
								AND ( b.flag IS NULL
									OR b.flag = 0
									)
								AND d.importer_id_new_table = ".$this->session->get('COMPANY_ID')."
								AND YEAR(a.ls_date) =".$i." AND MONTH(a.ls_date)=".$a."
						";
						$data = $this->db->query($sql)->getRowArray();
						$dtil[]=(float)$data["value_usd"];
						if($i==$thn1){
							$tr .="<tr>";
							$tr .="<td>";
							$tr .=$this->kon_bulan($a);
							$tr .="</td>";
							$tr .="<td>";
							$tr .=(float)$data["value_usd"];
							$tr .="</td>";
							$tr .="</tr>";
						}
						
					}
					$dt[$idx1]["data"]=$dtil;
					
					$idx1++;
				}
				$data["dt"]=$dt;
				$data["tbl"]=$tr;
				return json_encode($data);
			break;
			case 'stack_dash':

				$sql = "SELECT TOP 5
								e.country_name AS country_origin ,
								b.unit ,
								CAST(SUM(b.qty) as numeric) AS tot_qty
								--FORMAT(CAST(SUM(b.qty) as numeric), '###,###,###.##', 'en-us') AS tot_qty
						FROM    vpti_ls_header a
								INNER JOIN vpti_ls_detail b ON a.io_number = b.io_number
															AND a.ls_id = b.ls_id
								INNER JOIN vpti_io_header c ON a.io_number = c.io_number
								INNER JOIN vpti_importer d ON c.commodity_code = d.commodity_code
															AND c.importer_id = d.importer_id
								INNER JOIN ref_country e ON b.origin = e.country_code
						WHERE   a.status = 'I'
								AND ( b.flag IS NULL
									OR b.flag = 0
									)
								AND d.importer_id_new_table =".$this->session->get('COMPANY_ID')."
								--AND YEAR(a.ls_date) = YEAR(GETDATE())
						GROUP BY e.country_name ,
								b.unit
						ORDER BY SUM(b.qty) DESC";
				$datas = $this->db->query($sql)->getResultArray();

				foreach($datas as $x=>$v){
					$dt["data"][]=$v["tot_qty"];
					$dt["contry"][]=$v["country_origin"];
					
				}
				return json_encode($dt);

				/*
				$sql="SELECT TOP 5
								e.country_name AS country_origin ,
								CAST(SUM(b.fob_value_curr * f.KURS) as numeric) AS value_usd
						FROM    vpti_ls_header a
								INNER JOIN vpti_ls_detail b ON a.io_number = b.io_number
															AND a.ls_id = b.ls_id
								INNER JOIN vpti_io_header c ON a.io_number = c.io_number
								INNER JOIN vpti_importer d ON c.commodity_code = d.commodity_code
															AND c.importer_id = d.importer_id
								INNER JOIN ref_country e ON b.origin = e.country_code
								INNER JOIN kurs f ON b.currency = f.MTU
						WHERE   a.status = 'I'
								AND ( b.flag IS NULL
									OR b.flag = 0
									)
								AND d.importer_id_new_table =".$this->session->get('COMPANY_ID')."
								--AND YEAR(a.ls_date) = YEAR(GETDATE())
						GROUP BY e.country_name
						ORDER BY SUM(b.fob_value_curr * f.KURS) DESC";
						
				//$data = $this->dba->query($sql)->result_array();
				$data = $this->db->query($sql)->getResultArray();
				$dt=array();
				
				foreach($data as $x=>$v){
					$dt["data"][]=$v["value_usd"];
					$dt["contry"][]=$v["country_origin"];
					
				}
				return json_encode($dt);
			*/
			break;
			case 'donat_dash':
				$sql = "SELECT TOP 5
								a.discharge_port as country_origin,
								CAST(SUM(b.fob_value_curr * e.KURS) as numeric) AS value_usd
						FROM    vpti_ls_header a
								INNER JOIN vpti_ls_detail b ON a.io_number = b.io_number
															AND a.ls_id = b.ls_id
								INNER JOIN vpti_io_header c ON a.io_number = c.io_number
								INNER JOIN vpti_importer d ON c.commodity_code = d.commodity_code
															AND c.importer_id = d.importer_id
								INNER JOIN kurs e ON b.currency = e.MTU
						WHERE   a.status = 'I'
								AND ( b.flag IS NULL
									OR b.flag = 0
									)
								AND d.importer_id_new_table = ".$this->session->get('COMPANY_ID')."
								--AND YEAR(a.ls_date) = YEAR(GETDATE())
						GROUP BY a.discharge_port
						ORDER BY SUM(b.fob_value_curr * e.KURS) DESC";
				$data = $this->db->query($sql)->getResultArray();
				foreach($data as $x=>$v){
					$dt["data"][]=(float)$v["value_usd"];
					$dt["contry"][]=$v["country_origin"];
					
				}
				return json_encode($dt);
			break;
        }
        
        return $dt;
    }

	function kon_bulan($p1){
		$bln="";
		switch($p1){
			case 1:$bln="Jan";break;
			case 2:$bln="Feb";break;
			case 3:$bln="Mar";break;
			case 4:$bln="Apr";break;
			case 5:$bln="Mei";break;
			case 6:$bln="Jun";break;
			case 7:$bln="Jul";break;
			case 8:$bln="Agu";break;
			case 9:$bln="Sep";break;
			case 10:$bln="Okt";break;
			case 11:$bln="Nov";break;
			case 12:$bln="Des";break;
		}
		return $bln;
	}
}
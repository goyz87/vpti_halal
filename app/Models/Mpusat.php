<?php

namespace App\Models;

use CodeIgniter\Model;

class Mpusat extends Model
{
    // ...
    protected $table='vpti_importer';
    protected function initialize()
    {
        $this->db = db_connect();
		$builder = $this->db->table('vpti_importer');
		$this->session = \Config\Services::session(); 
    }
    function get_data($p1,$p2="")
    {
		$where =" WHERE 1=1 ";
        switch ($p1){
			case "total_hs_usd":
				$sql = "SELECT TOP 10
							b.hs_code,
							(SELECT TOP 1 pos_ind_desc FROM dbo.ref_hsdetail WHERE hs_code=b.hs_code) AS desc_hs,
							CAST(SUM(b.fob_value_curr * e.KURS) AS NUMERIC) AS value_usd
						FROM vpti_ls_header a
							INNER JOIN vpti_ls_detail b
								ON a.io_number = b.io_number
								   AND a.ls_id = b.ls_id
							INNER JOIN vpti_io_header c
								ON a.io_number = c.io_number
							INNER JOIN vpti_importer d
								ON c.commodity_code = d.commodity_code
								   AND c.importer_id = d.importer_id
							INNER JOIN kurs e
								ON b.currency = e.MTU
						WHERE a.status = 'I'
							  AND
							  (
								  b.flag IS NULL
								  OR b.flag = 0
							  )
							  AND a.io_number LIKE 'X.24%'
							  AND YEAR(a.ls_date) = YEAR(GETDATE())-1
						GROUP BY b.hs_code
						ORDER BY SUM(b.fob_value_curr * e.KURS) DESC ";
				$datas = $this->db->query($sql)->getResultArray();

				foreach($datas as $x=>$v){
					$dt["data"][]=$v["value_usd"];
					$dt["contry"][]=$v["hs_code"];
					
				}
				return json_encode($dt);
			break;
			case "total_hs":
				$sql = "SELECT TOP 10
							b.hs_code,
							(SELECT TOP 1 pos_ind_desc FROM dbo.ref_hsdetail WHERE hs_code=b.hs_code) AS desc_hs,
							COUNT(a.ls_number) AS total
						FROM vpti_ls_header a
							INNER JOIN vpti_ls_detail b
								ON a.io_number = b.io_number
								   AND a.ls_id = b.ls_id
							INNER JOIN vpti_io_header c
								ON a.io_number = c.io_number
							INNER JOIN vpti_importer d
								ON c.commodity_code = d.commodity_code
								   AND c.importer_id = d.importer_id
							INNER JOIN kurs e
								ON b.currency = e.MTU
						WHERE a.status = 'I'
							  AND
							  (
								  b.flag IS NULL
								  OR b.flag = 0
							  )
							  AND a.io_number LIKE 'X.24%'
							  AND YEAR(a.ls_date) = YEAR(GETDATE())-1
						GROUP BY b.hs_code
						ORDER BY COUNT(a.ls_number) DESC ";
				$datas = $this->db->query($sql)->getResultArray();

				foreach($datas as $x=>$v){
					$dt["data"][]=$v["total"];
					$dt["contry"][]=$v["hs_code"];
					
				}
				return json_encode($dt);
			break;
			case "total_port":
				$sql = "SELECT TOP 10
							a.discharge_port,
							COUNT(a.ls_number) AS total
						FROM vpti_ls_header a
							INNER JOIN vpti_ls_detail b
								ON a.io_number = b.io_number
								   AND a.ls_id = b.ls_id
							INNER JOIN vpti_io_header c
								ON a.io_number = c.io_number
							INNER JOIN vpti_importer d
								ON c.commodity_code = d.commodity_code
								   AND c.importer_id = d.importer_id
							INNER JOIN kurs e
								ON b.currency = e.MTU
						WHERE a.status = 'I'
							  AND
							  (
								  b.flag IS NULL
								  OR b.flag = 0
							  )
							  AND a.io_number LIKE 'X.25%'
							  AND YEAR(a.ls_date) = YEAR(GETDATE())-1
						GROUP BY a.discharge_port
						ORDER BY COUNT(a.ls_number) DESC ";
				$datas = $this->db->query($sql)->getResultArray();

				foreach($datas as $x=>$v){
					$dt["data"][]=$v["total"];
					$dt["contry"][]=$v["discharge_port"];
					
				}
				return json_encode($dt);
			break;
			case "total_country_usd":
				$sql = "SELECT TOP 10
							e.country_name AS country_origin,
							CAST(SUM(b.fob_value_curr * f.KURS) AS NUMERIC) AS value_usd
						FROM vpti_ls_header a
							INNER JOIN vpti_ls_detail b
								ON a.io_number = b.io_number
								   AND a.ls_id = b.ls_id
							INNER JOIN vpti_io_header c
								ON a.io_number = c.io_number
							INNER JOIN vpti_importer d
								ON c.commodity_code = d.commodity_code
								   AND c.importer_id = d.importer_id
							INNER JOIN ref_country e
								ON b.origin = e.country_code
							INNER JOIN kurs f
								ON b.currency = f.MTU
						WHERE a.status = 'I'
							  AND
							  (
								  b.flag IS NULL
								  OR b.flag = 0
							  )
							  AND a.io_number LIKE 'X.25%'
							  AND YEAR(a.ls_date) = YEAR(GETDATE())-1
						GROUP BY e.country_name
						ORDER BY SUM(b.fob_value_curr * f.KURS) DESC;;
";
				$datas = $this->db->query($sql)->getResultArray();

				foreach($datas as $x=>$v){
					$dt["data"][]=$v["value_usd"];
					$dt["contry"][]=$v["country_origin"];
					
				}
				return json_encode($dt);
			break;
			case "total_country":
				$sql = "SELECT TOP 10
							e.country_name AS country_origin,
							COUNT(a.ls_number) AS total
						FROM vpti_ls_header a
							INNER JOIN vpti_ls_detail b
								ON a.io_number = b.io_number
								   AND a.ls_id = b.ls_id
							INNER JOIN vpti_io_header c
								ON a.io_number = c.io_number
							INNER JOIN vpti_importer d
								ON c.commodity_code = d.commodity_code
								   AND c.importer_id = d.importer_id
							INNER JOIN ref_country e
								ON b.origin = e.country_code
							INNER JOIN kurs f
								ON b.currency = f.MTU
						WHERE a.status = 'I'
							  AND
							  (
								  b.flag IS NULL
								  OR b.flag = 0
							  )
							  AND a.io_number LIKE 'X.25%'
							  AND YEAR(a.ls_date) = YEAR(GETDATE())-1
						GROUP BY e.country_name
						ORDER BY COUNT(a.ls_number) DESC;
";
				$datas = $this->db->query($sql)->getResultArray();

				foreach($datas as $x=>$v){
					$dt["data"][]=$v["total"];
					$dt["contry"][]=$v["country_origin"];
					
				}
				return json_encode($dt);
			break;
			case "total_usd_ls":
				$thn1=(int)date('Y');
				$thn2=(int)date('Y')-2;
				//echo $thn1.'->'.$thn2;
				$dt=array();
				$idx1=0;
				$idx2=0;
				//$tr="";
				for ($i=$thn2;$i<=$thn1; $i++ ){
					$dt[$idx1]["name"]=$i;
					$dtil=array();
					for($a=1;$a<=12;$a++){
						$sql ="SELECT MONTH(a.ls_date) AS bulan,
									   YEAR(a.ls_date) AS Tahun,
									   CAST(SUM(b.fob_value_curr * e.KURS) AS NUMERIC) AS value_usd
								FROM vpti_ls_header a
									INNER JOIN vpti_ls_detail b
										ON a.io_number = b.io_number
										   AND a.ls_id = b.ls_id
									INNER JOIN vpti_io_header c
										ON a.io_number = c.io_number
									INNER JOIN vpti_importer d
										ON c.commodity_code = d.commodity_code
										   AND c.importer_id = d.importer_id
									INNER JOIN kurs e
										ON b.currency = e.MTU
								WHERE YEAR(a.ls_date) = ".$i." AND MONTH(a.ls_date)=".$a." 
									  AND a.status = 'I'
									  AND
									  (
										  b.flag IS NULL
										  OR b.flag = 0
									  )
									  
									  AND a.io_number LIKE 'X.24%'
								GROUP BY MONTH(a.ls_date),
										 YEAR(a.ls_date)
								ORDER BY YEAR(a.ls_date),
										 MONTH(a.ls_date)";
						$data = $this->db->query($sql)->getRowArray();
						if(isset($data["value_usd"]))$dtil[]=(float)$data["value_usd"];
						else $dtil[]=0;
						
						
					}
					$dt[$idx1]["data"]=$dtil;
					
					$idx1++;
				}
				$data["dt"]=$dt;
				//$data["tbl"]=$tr;
				return json_encode($data);
			break;
			case "total_ls":
				$thn1=(int)date('Y');
				$thn2=(int)date('Y')-2;
				//echo $thn1.'->'.$thn2;
				$dt=array();
				$idx1=0;
				$idx2=0;
				//$tr="";
				for ($i=$thn2;$i<=$thn1; $i++ ){
					$dt[$idx1]["name"]=$i;
					$dtil=array();
					for($a=1;$a<=12;$a++){
						$sql ="SELECT MONTH(ls_date) AS bulan,YEAR(ls_date) tahun,
									   ISNULL(COUNT(ls_number),0) AS total_ls
								FROM dbo.vpti_ls_header
								WHERE YEAR(ls_date) = ".$i." AND MONTH(ls_date)=".$a." 
									  AND status = 'I'
									  AND io_number LIKE 'X.24%'
								GROUP BY MONTH(ls_date),
										 YEAR(ls_date)
								ORDER BY YEAR(ls_date),
										 MONTH(ls_date) ";
						$data = $this->db->query($sql)->getRowArray();
						if(isset($data["total_ls"]))$dtil[]=(float)$data["total_ls"];
						else $dtil[]=0;
						
						
					}
					$dt[$idx1]["data"]=$dtil;
					
					$idx1++;
				}
				$data["dt"]=$dt;
				//$data["tbl"]=$tr;
				return json_encode($data);
			break;
			case "ip":
				$cari=(isset($_POST["search"]["value"])? $_POST["search"]["value"] : '' );
				if($cari!=""){
					$where .=" AND A.ip_number like '%".$cari."%' ";
				}
				$sql="SELECT ROW_NUMBER() OVER (ORDER BY A.ip_number DESC) as rowID,A.*,
						CONVERT (VARCHAR(11), A.ip_date, 106) AS ip_date2,
						CONVERT (VARCHAR(11), A.ip_expiry_date, 106) AS ip_expiry_date2
						FROM vpti_ip_header AS A ".$where."  AND importer_id='".$this->session->get('imp_id_lama')."' 
						AND A.commodity_code='".$this->session->get('commodity_code')."' ";
						
				if($p2=='edit'){
					$sql .= " AND commodity_code='".$this->session->get('commodity_code')."' 
						  AND importer_id='".$this->session->get('imp_id_lama')."' 
						  AND ip_number='".$_POST['ip_number']."' ";
					//echo $sql;exit;
					return $this->db->query($sql)->getRowArray();
				}
				

                //$dt=$this->db->query($sql)->getResultArray();
            break;
			case "hs-module":
				$dt=array();
				$dt['header']=$this->db->table('vpti2_dhs_hdr')->getWhere(['dhs_id' => $_POST["dhs_id"]])->getRowArray();
				$dt['detil']=$this->db->table('vpti2_dhs_dtl')->getWhere(['dhs_hdr_id' => $_POST["dhs_id"]])->getResultArray();
				//$dt['header']
				return $dt;
			break;
			case "dt-hs":
				return $this->db->table('ref_hsdetail')->getWhere(['commcode' => $this->session->get('commodity_code')])->getResultArray();
			break;
			case "dt-country":
				$sql="SELECT * FROM ref_country ORDER BY country_name ASC ";
				//$dt=$this->db->table('ref_country')->get()->getResultArray();
				$dt=$this->db->query($sql)->getResultArray();
				foreach($dt as $x=>$v){
					$dt[$x]['country_name']=str_replace("'"," ",$v["country_name"]);
				}
				return $dt;
			break;
            case "supplier":
				$cari=(isset($_POST["search"]["value"])? $_POST["search"]["value"] : '' );
				if($cari!=""){
					$where .=" AND A.name like '%".$cari."%' ";
				}
				$where .=" AND A.importer_id='".$this->session->get('imp_id_lama')."' AND A.commodity_code='".$this->session->get('commodity_code')."' ";
				$sql="SELECT ROW_NUMBER() OVER (ORDER BY A.name DESC) as rowID,A.*
						FROM ref_supplier AS A ".$where;
						
				if($p2=='edit'){
					$sql .= " AND commodity_code='".$this->session->get('commodity_code')."' 
						  AND importer_id='".$this->session->get('imp_id_lama')."' 
						  AND item_no=".$_POST['dt_item_no'];
					return $this->db->query($sql)->getRowArray();
				}
				

                //$dt=$this->db->query($sql)->getResultArray();
            break;
			case "exporter":
				$cari=(isset($_POST["search"]["value"])? $_POST["search"]["value"] : '' );
				if($cari!=""){
					$where .=" AND A.name like '%".$cari."%' ";
				}
				$where .=" AND A.importer_id='".$this->session->get('imp_id_lama')."' AND A.commodity_code='".$this->session->get('commodity_code')."' ";
				$sql="SELECT ROW_NUMBER() OVER (ORDER BY A.name DESC) as rowID,A.*
						FROM ref_exporter AS A ".$where;
						
				if($p2=='edit'){
					$sql .= " AND commodity_code='".$this->session->get('commodity_code')."' 
						  AND importer_id='".$this->session->get('imp_id_lama')."' 
						  AND item_no=".$_POST['dt_item_no'];
					return $this->db->query($sql)->getRowArray();
				}
				

                //$dt=$this->db->query($sql)->getResultArray();
            break;
			case "hs":
				$cari=(isset($_POST["search"]["value"])? $_POST["search"]["value"] : '' );
				if($cari!=""){
					$where .=" AND A.dhs_title like '%".$cari."%' ";
				}
				$sql="SELECT ROW_NUMBER() OVER (ORDER BY A.dhs_id DESC) as rowID,A.*,
						CONVERT (VARCHAR(11), A.dhs_date, 106) + ' ' + CONVERT (VARCHAR(8), A.dhs_date, 114) AS dhs_date2
						FROM vpti2_dhs_hdr AS A ".$where." AND importer_id='".$this->session->get('imp_id_lama')."' 
						AND A.commodity_code='".$this->session->get('commodity_code')."' ";
						
				if($p2=='edit'){
					$sql .= " AND dhs_id=".$_POST['dhs_id'];
					return $this->db->query($sql)->getRowArray();
				}
				

                //$dt=$this->db->query($sql)->getResultArray();
            break;
            case "rampok":
                $dt=$this->db->table('rampok')->get()->getRowArray();
            break;
        }
        
        return $this->dt_grid($sql);
    }
	function simpan_data($table,$data,$get_id=""){
		$this->db->transBegin();
		
		$id = (isset($data['id']) ? $data['id'] : null);
		$field_id = "id";
		$sts_crud = $_POST['sts'];
		unset($data['sts']);
		
		//echo $this->session->get('commodity_code');exit;
		
		switch($table){
			case "ip":
				$builder = $this->db->table('vpti_ip_header');
				$data["commodity_code"]=$this->session->get('commodity_code');
				$data["importer_id"]=$this->session->get('imp_id_lama');
				//upload_single
				if(isset($_FILES['file_ip']) && $_FILES['file_ip']['name']!="")$data["file_ip_it"]=$this->upload_single("file_ip","file_ip");
				if($sts_crud=='edit' || $sts_crud=='delete'){
					$builder->where(array('commodity_code'=>$data["commodity_code"],'importer_id'=>$data["importer_id"],'ip_number'=>$data["ip_number"]));
					unset ($data["commodity_code"]);
					unset ($data["importer_id"]);
					unset ($data["ip_number"]);
					//$data["commodity_code"]
				}
				//echo "<pre>";print_r($data);exit;
				
				
				
				
			break;
			case "hs":
				//echo "<pre>";print_r($data);exit;
				
				
				if($sts_crud=='delete'){
					$dhs_hdr_id = $data["dhs_id"];
					$builder = $this->db->table('vpti2_dhs_hdr');
					$builder->where(array('dhs_id'=>$dhs_hdr_id));
					$builder->delete();
					$builder = $this->db->table('vpti2_dhs_dtl');
					$builder->where(array('dhs_hdr_id'=>$dhs_hdr_id));
					$builder->delete();
				}else{
					$builder = $this->db->table('vpti2_dhs_hdr');
					
					$dt_h=array('dhs_title'=>$data["name"],
								'dhs_date'=>date('Y-m-d H:i:s'),
								'commodity_code'=>$this->session->get('commodity_code'),
								'importer_id'=>$this->session->get('imp_id_lama'),
								'created_by'=>$this->session->get('imp_id_lama'),
								'created_at'=>date('Y-m-d H:i:s'),
					);
					//echo "<pre>";print_r($dt_h);exit;
					
					if($sts_crud=='add'){
						$builder->insert($dt_h);
						$dhs_hdr_id = $this->db->insertID();
					}else{
						$dhs_hdr_id = $data["dhs_id"];
						$builder->where(array('dhs_id'=>$dhs_hdr_id));
						$builder->update($dt_h);
						
					}
					$hs_dt=$data["hs_code"];
					$dt_detil=array();
					foreach($hs_dt as $a=>$b){
						if($b!=""){
							$dt_detil[]=array('dhs_hdr_id'=>$dhs_hdr_id,
											'hs_code'=>$b,
											'hs_description'=>$data["hs_description"][$a],
											'country_code'=>$data["country_code"][$a],
											'quantity'=>$data["quantity"][$a],
											'unit'=>$data["unit"][$a],
											'weight'=>$data["weight"][$a],
											'unit_net'=>$data["unit_net"][$a],
											'pref_facility'=>$data["pref_facility"][$a],
											'license_no'=>$data["license_no"][$a],
											'sppt_sni'=>$data["sppt_sni"][$a],
											'created_by'=>$this->session->get('imp_id_lama'),
											'created_at'=>date('Y-m-d H:i:s'),
							);
						}
					}
					
					//echo "<pre>";print_r($dt_detil);exit;
					$builder = $this->db->table('vpti2_dhs_dtl');
					$builder->where(array('dhs_hdr_id'=>$dhs_hdr_id));
					$builder->delete();
					$builder->insertBatch($dt_detil);
				
				}
				
				if ($this->db->transStatus() === false) {
					$this->db->transRollback();
					return 0;
				} else {
					return $this->db->transCommit();
				}
				
				
				
			break;
			case "supplier":
				$builder = $this->db->table('ref_supplier');
				$data["commodity_code"]=$this->session->get('commodity_code');
				$data["importer_id"]=$this->session->get('imp_id_lama');
				//$data["item_no"]=$this->session->get('imp_id_lama');
				if($sts_crud=='add'){
					$sql="SELECT ISNULL(MAX (item_no) + 1,1)as idx FROM ref_supplier WHERE commodity_code='".$data["commodity_code"]."' AND importer_id=".$data["importer_id"]." ";
					$idx=$this->db->query($sql)->getRowArray()["idx"];
					$data["item_no"]=$idx;
					
				}
				if($sts_crud=='edit' || $sts_crud=='delete'){
					$builder->where(array('commodity_code'=>$data["commodity_code"],'importer_id'=>$data["importer_id"],'item_no'=>$data["item_no"]));
					unset ($data["commodity_code"]);
					unset ($data["importer_id"]);
					unset ($data["item_no"]);
					//$data["commodity_code"]
				}
				
				
			break;
			case "exporter":
				$builder = $this->db->table('ref_exporter');
				$data["commodity_code"]=$this->session->get('commodity_code');
				$data["importer_id"]=$this->session->get('imp_id_lama');
				//$data["item_no"]=$this->session->get('imp_id_lama');
				if($sts_crud=='add'){
					$sql="SELECT ISNULL(MAX (item_no) + 1,1)as idx FROM ref_exporter WHERE commodity_code='".$data["commodity_code"]."' AND importer_id=".$data["importer_id"]." ";
					$idx=$this->db->query($sql)->getRowArray()["idx"];
					$data["item_no"]=$idx;
					
				}
				if($sts_crud=='edit' || $sts_crud=='delete'){
					//print_r($data);exit;
					$builder->where(array('commodity_code'=>$data["commodity_code"],'importer_id'=>$data["importer_id"],'item_no'=>$data["item_no"]));
					unset ($data["commodity_code"]);
					unset ($data["importer_id"]);
					unset ($data["item_no"]);
					//$data["commodity_code"]
				}
				
				
			break;
		}
		
		if($sts_crud=='add'){
			
			$builder->insert($data);
			//$this->db->insert('ref_supplier', $data);
		}else if($sts_crud=='edit'){
			//print_r($data);exit;
			$builder->update($data);	
			//echo $this->db->getLastQuery(); die;				
		}else{
			
			$builder->delete();
		}
		
		if ($this->db->transStatus() === false) {
			$this->db->transRollback();
			return 0;
		} else {
			return $this->db->transCommit();
		}
		
		
	}

	function dt_grid($sql){
		$page = (integer) $_POST['draw'];
		$start = (integer) $_POST['start'];
		$end = (integer) $_POST['length'] + $start;
		$count = $this->db->query($sql)->getNumRows();
		


		if($start >= 10) $start = $start+1;

		//echo $start .'->'.$end;exit;
		$sql = "
			SELECT * FROM (
					".$sql."
			) AS X WHERE X.rowID BETWEEN $start AND $end
		";
		
		$totalRecords = $count;
		$totalRecordwithFilter = $count;
		$response = array(
		  "draw" => intval($page),
		  "recordsTotal" => $totalRecords,
		  "recordsFiltered" => $totalRecordwithFilter,
		  "data" => $this->db->query($sql)->getResultArray()
		);

		return json_encode($response);
	}
	
	function upload_single($mod,$object){
		$file=date('YmdHis');
		switch($mod){
			case "file_ip": $upload_path='__repo/';$file="IP_IT_".date('YmdHis'); break;
			
		}
		
		$upload=$this->upload_nya($upload_path, $object, $file);
		if($upload){return $upload;}
		else{echo 2;exit;}
	}
	function upload_nya($upload_path="", $object="", $file=""){
		//$upload_path = "./__repository/".$folder."/";
		
		//$ext = explode('.',$_FILES[$object]['name']);
		//$exttemp = sizeof($ext) - 1;
		//$extension = $ext[$exttemp];
		$extension = pathinfo($_FILES[$object]['name'], PATHINFO_EXTENSION); 
		$filename =  $file.'.'.$extension;
		
		$files = $_FILES[$object]['name'];
		$tmp  = $_FILES[$object]['tmp_name'];
		if(!file_exists($upload_path))mkdir($upload_path, 0777, true);
		if(file_exists($upload_path.$filename)){
			unlink($upload_path.$filename);
			$uploadfile = $upload_path.$filename;
		}else{
			$uploadfile = $upload_path.$filename;
		} 
		//echo $_SERVER["DOCUMENT_ROOT"].preg_replace('@/+$@','',dirname($_SERVER['SCRIPT_NAME'])).'/'.$uploadfile;exit;
		move_uploaded_file($tmp, $uploadfile);
		
		
		return $filename;
	}
	
}